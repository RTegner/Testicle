#!/bin/bash
#-----------------------------------------------------------------------------
#	Title: mk-requires.sh
#	Date: 2021-07-08
#	Version: 1.1
#	Author: scott-andrews@columbus.rr.com
#	Options: none
#-----------------------------------------------------------------------------
#	Copyright 2021 Scott Andrews all rights reserved
#-----------------------------------------------------------------------------
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------
#set -o errexit	# exit if error...insurance ;)
set -o nounset	# exit if variable not initalized
#-----------------------------------------------------------------------------
#	Common support functions
_end_run() {
	printf "%s\n" "${0##*/}: Run Complete"
	return
}
unique() {
	local status=0
	string="${1}"
	declare -A words
	IFS=", "
	for w in ${string}; do words+=( [$w]="" );done
	echo "${!words[@]}"
	return $(( status ))
}
for pkg in RPMBUILD/*;do
#	pkg=gettext
	line='';list='';depends=''
	pkg=${pkg##*/}
	if [[ -r RPMBUILD/"${pkg}"/requires ]];then
		printf "   Package: [%s]: " "${pkg}"
		while IFS= read -r line; do
			#	printf "  Requires: [%s]\n" "${line}"
			line=${line%%)>=*}
			line=${line%%>=*}
			line=${line%%3*}
			depends="$(grep -l -R "${line}" RPMBUILD/*/provides)"
			depends=${depends#*/}
			depends=${depends%%/*}
			if [ -z "${depends}" ];then depends="${line}:NOT_FOUND";fi
			if [ "${depends}" == "${pkg}" ];then continue;fi
			list+=" ${depends}"
			#list="${list/${pkg}/}"
			list=$(unique "${list}")
		done < RPMBUILD/"${pkg}"/requires
		list=$(echo "${list}" | tr " " "\n" | sort --version-sort | tr "\n" " ")
		printf "%s\n" "${list}"
		printf "%s\n" "${list}" > RPMBUILD/"${pkg}"/depends
	fi
done
_end_run;exit 0
