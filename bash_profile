set +h
#	umask 022
#	Toolchain Build
#	LFS=/mnt
#	LFS_TGT="$(uname -m)-lfs-linux-gnu"
#	PATH=${LFS}/tools/bin:/bin:/usr/bin
#	CONFIG_SITE=${LFS}/usr/share/config.site
#	DBPATH="--dbpath ${LFS}/var/lib/rpm"
#	export LFS LFS_TGT PATH CONFIG_SITE DBPATH
#	Standard Builds
#export PATH=/usr/bin:/usr/sbin
#	CONFIG_SITE=/usr/share/config.site
#	export PATH CONFIG_SITE
#	Generic
LC_ALL=POSIX
HOST=rpi.example.org
HOSTNAME=rpi.example.org
export LC_ALL HOST HOSTNAME
NORMAL="\[\e[0m\]"
RED="\[\e[1;31m\]"
GREEN="\[\e[1;32m\]"
CYAN="\[\e[1;36m\]"
if [ ${EUID} == 0 ] ; then
	PS1="${RED}\u${NORMAL}@${HOST}${RED} [ ${NORMAL}\w${RED} ]# ${NORMAL}"
else
	PS1="\u${GREEN}@${CYAN}${HOST}${GREEN} [ ${NORMAL}\w${GREEN} ]$ ${NORMAL}"
fi

unset gawklibpath_append
unset gawklibpath_default
unset gawklibpath_prepend
unset gawkpath_append
unset gawkpath_default
unset gawkpath_prepend
