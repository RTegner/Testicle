set +h
umask 022
#	HOST=rpi.example.org
#	HOSTNAME=rpi.example.org
#export PATH=/usr/bin
export LC_ALL=POSIX
#	unset BASH_LOADABLES_PATH
NORMAL="\[\e[0m\]"
RED="\[\e[1;31m\]"
GREEN="\[\e[1;32m\]"
CYAN="\[\e[1;36m\]"
export NORMAL RED GREEN CYAN
PS1="\u${GREEN}@${CYAN}${HOSTNAME}${GREEN} [ ${NORMAL}\w${GREEN} ]$ ${NORMAL}"
