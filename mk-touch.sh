#!/bin/dash
#-----------------------------------------------------------------------------
#	Title: mk-touch.sh
#	Date: 2022-08-03
#	Version: 1.0
#	Author: scott-andrews@columbus.rr.com
#	Options: none
#-----------------------------------------------------------------------------
#	Copyright 2019 Scott Andrews
#-----------------------------------------------------------------------------
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------
#	Dedicated to Elizabeth my cat of 20 years, Euthanasia on 2019-05-16
#-----------------------------------------------------------------------------
set -o errexit	# exit if error...insurance ;)
set -o nounset	# exit if variable not initalized
#-----------------------------------------------------------------------------
#	Master variables
PRGNAME=${0##*/}	#	script name minus the path
RELEASE="2022-08-03"
USAGE="Usage: ${0##*/} [ -h | -v | -f ]"
VERSTR="${0##*/}, version ${RELEASE}"
OPTIND=1
FILE=""
STATUS=0
COUNT=0
_red="\\033[1;31m"
_normal="\\033[0;39m"
_green="\\033[1;32m"
#-----------------------------------------------------------------------------
_success() {
	#	printf "${_green}%s${_normal}\n" "SUCCESS"
	printf "%s\n" "SUCCESS"
	return
}
_end_run() {
	#	printf "${_green}%s${_normal}\n" "${PRGNAME}: Run Complete"
	printf "%s\n" "${PRGNAME}: Run Complete"
	return
}
# Summary: strip whitespace from string
#   Usage: strip_whitespace "string"
#  Return: clean string
strip_whitespace(){
	local string="${1}"
	local status=0
	# remove leading whitespace characters
	string="${string#"${string%%[![:space:]]*}"}"
	# remove trailing whitespace characters
	string="${string%"${string##*[![:space:]]}"}"
	printf "%s" "${string}"
	return $(( status ))
}
#-----------------------------------------------------------------------------
# Process arguments
while getopts vhnd:r:f: opt; do
	case "${opt}" in
		h)	printf "%s\n" "${VERSTR}"
			printf "%s\n" "${USAGE}"
			printf "%s\n" "${0##*/} installs rpm packages to a directory"
			printf "\t%s\n" "-v version"
			printf "\t%s\n" "-h help"
			printf "\t%s\n" "-f <filespec> File that holds list to touch"
			exit 1; ;;
		v)	printf "\t%s\n" "${VERSTR}";exit 1; ;;
		f)	FILE=${OPTARG} ;;
		*)	: ;;
	esac
done
shift $((OPTIND - 1))
printf "%s\n" "${PRGNAME}:"
printf "%s" "   parm -f: [${FILE}]: "
if [ -z "${FILE}" ] || [ ! -r "${FILE}" ];then
	printf "%s\n" "FAILURE"
	exit 2
else
	printf "%s\n" "SUCCESS"
fi
if [ ${STATUS} -eq 0 ];then
	while IFS= read -r i; do
		i=${i%:*}
		case ${i} in
			\#*) ;;
			*)
				printf "%s" "     Touch: [${i}]: "
				touch "SPECS/${i}.spec"
				printf "%s\n" "SUCCESS"
				COUNT=$((COUNT+1))
			;;
		esac
	done < "${FILE}"
	printf "%s\n" "     Count: [${COUNT}]: "
fi
_end_run;
