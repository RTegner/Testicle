#!/usr/bin/bash
set -o errexit	# exit if error...insurance ;)
set -o nounset	# exit if variable not initalized
#	set -euo pipefail
IFS=$(printf ' \n\t')
Date="$(date +%s)"
LIST=""
LIST=(	packages-core.text
	packages-argonone.text
	packages-bind.text
	packages-mail.text
	packages-nfs-utils.text
	packages-nginx.text
	packages-openssh.text
	packages-pciutils.text
#	packages-wireless.text
)
#	packages-docbook.text
#	part of core or special
#	LIST="packages-gnu.toolchain.text"
#	LIST="packages-lzip.text"
#	LIST="packages-perl.text"
#	LIST="packages-python.text"
#	LIST="packages-rpm.text"

_count() {
	local begin
	local end
	local total
	begin="${1}"
	end="$(rpm -qa | wc -l)"
	total="$((begin - end))"
	printf "%s\n" "Begin Count: [${begin}]"
	printf "%s\n" "  End Count: [${end}]"
	printf "%s\n" "Error Count: [${total}]"
	return
}
if [ -d RPMS/aarch64 ];then rm -r RPMS/aarch64;fi
Count="$(rpm -qa | wc -l)"
for i in "${LIST[@]}";do
	printf "%s\n" "${i}"
	if [ -d  RPMS/"${i%.text}"-"${Date}" ];then rm -r RPMS/"${i%.text}"-"${Date}";fi
	case "${i}" in
		packages-core.text)
			./mk-pkg.sh -i -f "${i}"
		;;
		packages-python.text)
			./mk-pkg.sh -i -f "${i}"
		;;
		*)
			./mk-pkg.sh -i -f "${i}"
			./mk-remove.sh -f "${i}"
		;;
	esac
	./mk-depends.sh -f ${i}
	if [ -d RPMS/aarch64 ];then mv RPMS/aarch64 RPMS/"${i%.text}"-"${Date}";fi
	printf "%s\n" "${i}"
done
_count "${Count}"

