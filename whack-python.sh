#!/usr/bin/bash -ee
farts=(
#	python
	python-pip
	python-pyparsing
	python-packaging
	python-tomli
	python-toml
	python-pep517
	python-setuptools
	python-setuptools-scm
	python-wheel
	python-build
	python-requests
	python-urllib3
	python-chardet
	python-idna
	python-docutils
	python-flit
	python-installer
	python-jinja
	python-markupsafe
	python-typing_extensions
	python-more-itertools
	python-jaraco.functools
	python-jaraco.context
	python-jaraco.text
	python-appdirs
	python-ordered-set
	python-tomli
	python-tomli-w
	python-inflect
	python-calver
	python-validate-pyproject
	python-autocommand
	python-pydantic
	python-trove-classifiers
	python-cython
	python-six
	python-gyp-20220404.9ecf45e3
	python-future
	python-pyproject-hooks)
for i in "${farts[@]}"; do
	echo ${i}
	sudo rpm -ve --nodeps ${i} || true
done
sudo rm -rf /usr/lib/python3.11
sudo rm -rf /usr/include/python3.11
#	sudo rpm -Uvh --nodeps RPMS/armv7hnl/python-*.rpm
#	sudo python -m ensurepip
#	sudo rpm -Uvh --nodeps RPMS/armv7hnl/python-*.rpm
#	echo "rebuild meson"
