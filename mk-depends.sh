#!/bin/dash
#-----------------------------------------------------------------------------
#	Title: mk-depends.sh
#	Date: 2022-11-23
#	Version: 1.0
#	Author: scott-andrews@columbus.rr.com
#	Options: none
#-----------------------------------------------------------------------------
#	Copyright 2022 Scott Andrews All rights reserved
#-----------------------------------------------------------------------------
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------
#	Dedicated to Elizabeth my cat of 20 years, Euthanasia on 2019-05-16
#-----------------------------------------------------------------------------
#set -o errexit	# exit if error...insurance ;)
#set -o nounset	# exit if variable not initalized
set -eu
IFS=$(printf ' \n\t')
#-----------------------------------------------------------------------------
#	Master variables
PRGNAME=${0##*/}	#	script name minus the path
RELEASE="2022-11-23"
USAGE="Usage: ${0##*/} [ -h | -v | -f ]"
VERSTR="${0##*/}, version ${RELEASE}"
OPTIND=1
FILE=""
#-----------------------------------------------------------------------------
#	Common support functions
# Summary:  fetch rpm NAME VERSION RELEASE
#   Usage: _get_names "SPECS/<pkg>.spec"
#  Return: formated strings: NAME, VERSION and RELEASE
_get_names(){
	local file
	local name
	local version
	local release
	file="${1}"
	name="INVALID"
	version="INVALID"
	release="INVALID"
	NAME="INVALID"
	VERSION="INVALID"
	RELEASE="INVALID"
	name="$(grep '^Name:' "${file}")"
	version="$(grep '^Version:' "${file}")"
	release="$(grep '^Release:' "${file}")"
	NAME=${name#*: }
	VERSION=${version#*: }
	RELEASE=${release#*: }
	return
}
#	Summary: Validate filespec exists and readable
#	  Usage: _file_exists "<path>/<filespec>"
#	 Return: status: 0=success, 1=failure
_file_readable(){
	local status
	status=1
	printf "%s" "      FILE: [${1}]: "
	if [ -r "${1}" ];then
		printf "%s\n" "READABLE"
		status=0
	else
		printf "%s\n" " INVALID"
	fi
	return $(( status ))
}

_requires() {
	local w
	local p
	#	create requires string
	requires=""
	requires=$(/usr/bin/grep '^Requires:' "${log}") || true
	requires=$(printf '%s' "${requires}" | /usr/bin/sed "s/^Requires: //")
	#	requires=$(printf '%s' "${requires}" | /usr/bin/sed "s/ = /=/g")
	requires=$(printf '%s' "${requires}" | /usr/bin/sed "s/ >= />=/g")
	#	remove requirement>=
	w=""
	for p in ${requires};do
		p=${p%%'>='*}
		p=${p%%3*}
		w="${w} ${p}"
	done
	requires="${w}"
	#	requires="$(printf "%s" "${w}" | tr " " "\n" | sort -u --version-sort | tr "\n" " ")"
	return
}
_depends() {
	local s
	local g
#	create depends
	depends=""
	for s in ${2};do
		g="$(grep -l -R "${s}" LOGS/*.provides || true)"
		#	remove LOGS/
		#	g=$(printf '%s' "${g}" | /usr/bin/sed "s/*LOGS\/ //")
		g="${g##*/}"
		#	remove revision and .provides
		g=${g%-*}
		#	remove version
		g=${g%-*}
		if [ -z "${g}" ];then g="${s}:NOT_FOUND";fi
		if [ "${g}" = "${1}" ];then continue;fi
		depends="${depends} ${g}"
	done
	#	#	sort and remove duplicate
	depends="$(printf "%s" "${depends}" | tr " " "\n" | sort -u --version-sort | tr "\n" " ")"
	depends="$(printf "%s" "${depends}" | sed 's/^[ \t]*//;s/[ \t]*$//')"
	return
}
#-----------------------------------------------------------------------------
PLATFORM="$(uname -m)"
if [ "${PLATFORM}" = "armv7l" ];then PLATFORM="armv7hnl";fi
#	Process arguments
while getopts vhnd:r:f: opt; do
	case "${opt}" in
		h)	printf "%s\n" "${VERSTR}"
			printf "%s\n" "${USAGE}"
			printf "%s\n" "${0##*/} update dependency list for package list"
			printf "\t%s\n" "-v version"
			printf "\t%s\n" "-h help"
			printf "\t%s\n" "-f <filespec> File that holds package list"
			exit 1; ;;
		v)	printf "\t%s\n" "${VERSTR}";exit 1; ;;
		f)	FILE=${OPTARG} ;;
		*)	: ;;
	esac
done
shift $((OPTIND - 1))
printf "%s\n" "${PRGNAME}:"
#	printf "%s" "   parm -f: [${FILE}]: "
_file_readable "${FILE}";printf "\n"
#IFS=$(printf '\n')
#scratch="$(mktemp -t tmp.XXXXXXXXXX)"
scratch="${FILE}.$(date +%F)"
if [ -r "${scratch}" ];then unlink "${scratch}";fi
while read -r line; do
	pkg="${line%:*}"
	if [ "${line%%' '*}" = "#" ];then
		printf "%s\n" "      Line: [${pkg}]"
		#	comment so just copy line to file
		printf "%s\n" "${line}" >> "${scratch}"
	else
		spec="SPECS/${pkg}".spec
		_file_readable "${spec}"
		_get_names "${spec}"
		name="${NAME}"-"${VERSION}"-"${RELEASE}"
		log="LOGS/${name}".log
		_file_readable "${log}"
		_requires "${log}"
		#	printf "[%s]\n" "${requires}"
		_depends "${pkg}" "${requires}"
		#	printf "[%s]\n" "${depends}"
		printf "\t%s: %s\n" "${pkg}" "${depends}"
		line=$(printf "%-32s%s\n" "${pkg}:" "${depends}")
		line="$(printf "%s" "${line}" | sed 's/^[ \t]*//;s/[ \t]*$//')"
		printf "%s\n" "${line}" >> "${scratch}"
	fi
	printf "\n"
done < "${FILE}"
#if [ -r "${scratch}" ];then unlink "${FILE}";mv "${scratch}" "${FILE}";fi
printf "%s\n" "${0##*/}: Run Complete"
