#!/usr/bin/bash
#-----------------------------------------------------------------------------
#	Title: mk-pkg.sh
#	Date: 2022-09-02
#	Version: 1.0
#	Author: scott-andrews@columbus.rr.com
#-----------------------------------------------------------------------------
#	Copyright 2020, 2021 Scott Andrews all rights reserved
#-----------------------------------------------------------------------------
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------
set -o errexit	# exit if error...insurance ;)
set -o nounset	# exit if variable not initalized
set +h		# disable hashall
#-----------------------------------------------------------------------------
#	Functions
#
#	Summary: Validate filespec exists and readable
#	  Usage: _file_readable "<path>/<filespec>"
#	 Return: status: 0=success, 1=failure
_file_readable(){
	local file="${1}"
	local status=1
	printf "%s" "      FILE: [${file}]: "
	if [ -r "${file}" ];then
		printf "%s\n" "READABLE"
		status=0
	else
		printf "%s\n" " INVALID"
	fi
	return $(( status ))
}
#	Summary: Validate directory exists
#	  Usage: _directory_readable "<path>"
#	 Return: status: 0=success, 1=failure
_directory_readable(){
	local file="${1}"
	local status=1
	printf "%s" " DIRECTORY: [${file}]: "
	if [ -d "${file}" ];then
		printf "%s\n" "AVAILABLE"
		status=0
	else
		printf "%s\n" " INVALID"
	fi
	return $(( status ))
}
# Summary:  fetch rpm NAME VERSION RELEASE
#   Usage: _get_names "SPECS/<pkg>.spec"
#  Return: formated strings: NAME, VERSION and RELEASE
_get_names(){
	local file="${1}"
	local name="INVALID"
	local version="INVALID"
	local release="INVALID"
	NAME="INVALID"
	VERSION="INVALID"
	RELEASE="INVALID"
	name="$(/usr/bin/grep '^Name:' "${file}")"
	version="$(/usr/bin/grep '^Version:' "${file}")"
	release="$(/usr/bin/grep '^Release:' "${file}")"
	NAME=${name#*: }
	VERSION=${version#*: }
	RELEASE=${release#*: }
	return
}
# Summary: fetch source archives, read spec file for source/patch files
#   Usage: fetch "control_filespec"
#  Return: status=0 success, status=1 failure
#  _fetch "${SPEC}"
_fetch() {
	local source=""
	local pkg=""
	local i=""
	source="$(grep '^Source' "${SPEC}")" || true
	for i in ${source}; do pkg+="${i##Source*:} ";done
	for i in ${pkg};do
		pushd SOURCES > /dev/null 2>&1;curl -L -O "${i}" > /dev/null 2>&1;popd > /dev/null 2>&1
		_file_readable "SOURCES/${i##*/}"
	done
	return
}
#-----------------------------------------------------------------------------
#	Mainline
#
#	Globals
PRGNAME=${0##*/}
RELEASE="2022-09-02"
USAGE="Usage: ${0##*/} [ -h | -v | -f filespec ]"
VERSTR="${0##*/}: version ${RELEASE}"
OPTIND=1
FILESPEC=INVALID
INSTALLFLG=1
PLATFORM="$(/usr/bin/rpm -E "%{_target_cpu}")"
PKG=""
ARRAY=()
COUNT=0
# Process arguments
while getopts vhif: opt; do
	case "${opt}" in
		h)	printf "%s\n" "${VERSTR}"
			printf "%s\n" "${USAGE}"
			printf "%s\n" "${0##*/} Builds rpm including dependencies"
			printf "\t%s\n" "-v version"
			printf "\t%s\n" "-h help"
			printf "\t%s\n" "-f <path>/<filespec> to file which contains a list of packages to build"
			printf "\t%s\n" "-i install finished rpm"
			exit 1; ;;
		v)	printf "%\n" "${VERSTR}";exit 1; ;;
		f)	FILESPEC=${OPTARG}; ;;
		i)	INSTALLFLG=0; ;;
		*)	;;
	esac
done
shift $((OPTIND - 1))
printf "%s\n" "${VERSTR}"
#
#	read list of packages to build into an array,
#	so if it is modified during run nothing bad happens
_file_readable "${FILESPEC}"
while read -r i; do
	if [ "#" == "${i:0:1}" ];then
		continue
	fi
	PKG=${i%%:*}
	ARRAY+=("${PKG}")
done < "${FILESPEC}"
#	make sure environment exists
for d in EMPTY LOGS SPECS TMP SOURCES;do
	if [ -d "${d}" ];then
		continue
	else
		/usr/bin/mkdir "${d}"
	fi
done
#
#	Process list of packages to build
for i in "${ARRAY[@]}";do
	NAME=INVALID
	VERSION=INVALID
	RELEASE=INVALID
	SPEC=INVALID
	SRPM=INVALID
	RPM=INVALID
	LOG=INVALID
	COUNT=$((COUNT+1))
	printf "%s\n" ""
	printf "%s\n" "       PKG: [${i}]"
	SPEC="SPECS/${i}".spec
	_directory_readable "SPECS"
	_file_readable "${SPEC}"
	#	Get names: look at spec file
	_get_names "${SPEC}"
	SRPM="SRPMS/${NAME}-${VERSION}-${RELEASE}.src.rpm"
	RPM="RPMS/${PLATFORM}/${NAME}-${VERSION}-${RELEASE}.${PLATFORM}.rpm"
	#	Set log file
	LOG="LOGS/${i}-${VERSION}-${RELEASE}"
	printf "%s\n" "      NAME: [${NAME}]"
	printf "%s\n" "   VERSION: [${VERSION}]"
	printf "%s\n" "   RELEASE: [${RELEASE}]"
	printf "%s\n" "      SPEC: [${SPEC}] "
	printf "%s\n" "      SRPM: [${SRPM}] "
	printf "%s\n" "       RPM: [${RPM}] "
	printf "%s\n" "       LOG: [${LOG}.log] "
#	#	spec newer than srpm || spec newer rpm
	if [ "${SPEC}" -nt "${SRPM}" ];then
		# build source rpm
		if [ -e "SOURCES" ];then /usr/bin/rm -rf "SOURCES";/usr/bin/mkdir "SOURCES";fi
		if [ -e "TMP" ];then /usr/bin/rm -rf "TMP";/usr/bin/mkdir "TMP";fi
		if [ -r "${SRPM}" ];then /usr/bin/unlink "${SRPM}";fi
		#	create source rpm
		printf "%s\n" "    CREATE: [${SRPM}]"
		rpmbuild -bs "${SPEC}" > "${LOG}.log" 2>&1
		_file_readable "${SRPM}"
		printf "\n" >> "${LOG}.log" 2>&1
		/usr/bin/sync
	fi
	#	RPM
	if [ "${SPEC}" -nt "${RPM}" ];then
		# build rpm
		#	make sure a clean environment exists
		for d in BUILD BUILDROOT SOURCES TMP;do
			if [ -e "${d}" ];then
				rm -rf "${d}"
				mkdir "${d}"
			fi
		done
		START="$(/usr/bin/date +%s)"
		if [ -r "${RPM}" ];then
			/usr/bin/unlink "${RPM}"
		fi
		#	create binary rpm
		printf "%s\n" "    CREATE: [${RPM}]"
		/usr/bin/rpmbuild -bb  "${SPEC}" >> "${LOG}.log" 2>&1
		_file_readable "${RPM}"
		/usr/bin/sync
		#	create info
		/usr/bin/rpm -qli "${RPM}" > "${LOG}.info" 2>&1
		#	create provides
		work=""
		if [ -r "${LOG}.log" ];then
			work=$(/usr/bin/grep '^Provides:' "${LOG}.log")
			work=${work/Provides: /};	# remove 'Provides: '
			work=${work// = /=};		# chg ' = ' to '='
			work=${work// >= />=};		# chg ' >= ' to '>='
			/usr/bin/rm -f "${LOG}.provides"
			/usr/bin/touch "${LOG}.provides"
			for p in ${work};do
				if [ -n "${p}" ];then
				printf "%s\n" "${p}" >> "${LOG}.provides"
			fi
			done
		fi
		#	create requires
		work=""
		if [ -r "${LOG}.log" ];then
			work=$(/usr/bin/grep '^Requires:' "${LOG}.log") || true
			work=${work/Requires: /};	# remove 'Requires: '
			work=${work// = /=};		# chg ' = ' to '='
			work=${work// >= />=};		# chg ' >= ' to '>='
			/usr/bin/rm -f "${LOG}.requires"
			/usr/bin/touch "${LOG}.requires"
			for p in ${work};do
				if [ -n "${p}" ];then
				printf "%s\n" "${p}" >> "${LOG}.requires"
			fi
			done
		fi
		#	How long?
		DURATION=$(( $(/usr/bin/date +%s) - START ))
		DURATION=$( printf '%02d:%02d:%02d\n' $((DURATION/3600)) $((DURATION%3600/60)) $((DURATION%60)) )
		if [ -v DURATION ];then
			printf "%s\n" " BuildTime: [${DURATION}]"
			printf "%s\n" "${DURATION}" > "${LOG}.buildtime"
		fi
		#	Installation
		if [ ${INSTALLFLG} -eq 0 ];then
			/usr/bin/sudo -E /usr/bin/rpm -Uvh --nodeps --replacepkgs --oldpackage --noscripts "${RPM}" || false;
		fi
		/usr/bin/sync
	else
	#	Not building
		_file_readable "${SPEC}"
		_file_readable "${SRPM}"
		_file_readable "${RPM}"
	fi
done;
#	if [ "${SRPM}" -nt "${RPM}" ];then BUILD=0;fi
#	#	create depends
#	list="";line="";depends=""
#	if [ -r "${LOG}.requires" ];then
#		printf "%s\n" "   Package: [${NAME}]"
#		while IFS= read -r line; do
#			line=${line%%)>=*}
#			line=${line%%>=*}
#			line=${line%%3*}
#			depends="$(grep -l -R "${line}" LOGS/*.provides || true)"
#			depends="${depends#*/}"
#			depends="${depends%%/*}"
#			depends="${depends%%.provides}"
#			#	remove revision
#			depends=${depends%-*}
#			#	remove version
#			depends=${depends%-*}
#			if [ -z "${depends}" ];then depends="${line}:NOT_FOUND";fi
#			if [ "${depends}" == "${NAME}" ];then continue;fi
#			list+=" ${depends}"
#		done < "${LOG}.requires"
#	fi
#	#	sort and remove duplicate
#	list="$(echo "${list}" | tr " " "\n" | sort -u --version-sort | tr "\n" " ")"
#	#	remove leading and trailing whitspace
#	list="$(sed -e 's/^[ \t]*//' -e 's/\ *$//g'<<<"${list}")"
#	printf "%s\n" "${list}" > "${LOG}.depends"
#	End depemds
#	remove "empty directory" file if it has a zero file length
/usr/bin/find EMPTY -size 0 -type f -exec rm {} +
#	Status
printf "Processed: %s\n" "${COUNT}"
printf "%s\n" "${PRGNAME}: Run Complete"
