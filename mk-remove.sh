#!/usr/bin/dash
#-----------------------------------------------------------------------------
#	Title: mk-remove.sh
#	Author: scott-andrews@columbus.rr.com
#-----------------------------------------------------------------------------
#	Copyright 2020, 2021 Scott Andrews all rights reserved
#-----------------------------------------------------------------------------
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.

#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.

#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------------
set -o errexit	# exit if error...insurance ;)
set -o nounset	# exit if variable not initalized
#-----------------------------------------------------------------------------
#
#	Globals
#
RELEASE="2022-09-07"
USAGE="Usage: ${0##*/} [ -h | -v | -f <filespec> ]"
VERSTR="${0##*/}: version: ${RELEASE}"
FILESPEC="INVALID"
OPTIND=1
#
#	Generic Functions
#
#	Summary: Print to screen message
#	  Usage: _msg "message"
#	 Return: nothing
_msg(){
	printf "%s\n" "${1}"
	return
}
#	Summary: Print to screen message
#	  Usage: _line "message"
#	 Return: nothing
_line(){
	printf "%s" "${1}"
	return
}
#	Summary: Validate filespec exists and readable
#	  Usage: _file_exists "<path>/<filespec>"
#	 Return: status: 0=success, 1=failure
_file_readable(){
	local status=1
	printf "%s" "      FILE: [${1}]: "
	if [ -r "${1}" ];then
		_msg "READABLE"
		status=0
	else
		_msg " INVALID"
	fi
	return $(( status ))
}
#
#	Mainline
#
string=""
while getopts vhf: opt; do
	case "${opt}" in
		h)	_msg "${VERSTR}"
			_msg "${USAGE}"
			_msg "${0##*/} Cleanup packages/dependencies "
			printf "\t%s\n" "-v version"
			printf "\t%s\n" "-h help"
			printf "\t%s\n" "-f packages-<section>.text list"
			exit 1; ;;
		v)	_msg "${VERSTR}";exit 1; ;;
		f)	FILESPEC=${OPTARG}; ;;
		*)	_msg "Option: INVALID" ;;
	esac
done
shift $((OPTIND - 1))
_msg "${VERSTR}"
if [ "$(_file_readable "${FILESPEC}")" ];then
	while IFS= read -r i; do
		if [ "${i%%' '*}" = "#" ];then continue;fi
		if [ -n "${i}" ];then
			i=${i%:*}
			string="${i} ${string}"
		fi
	done < "${FILESPEC}"
	for i in ${string};do
		_line "   PACKAGE: [${i}]: "
		status=0
		rpm -qa "${i}"| grep -q "${i}"||status=1
		if [ 0 -eq ${status} ];then
			sudo rpm -e "${i}" > /dev/null 2>&1||status=1
			if [ 0 -eq ${status} ];then _msg "REMOVED"
			else _msg "FARTED"
			fi
		else
			_msg "NOT INSTALLED"
		fi

	done
fi
printf "     COUNT: %s\n" "$(/usr/bin/rpm -qa | wc -l)"
