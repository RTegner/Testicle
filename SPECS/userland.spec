Summary: Broadcomm user utilities
Name: userland
Version: 54fd97a
Release: 1
License: Other
Group: Core
URL: https://github.com/raspberrypi/userland
#	Source
Source0: http://www.example.org/Packages/userland/userland-54fd97a.tgz
Source1: http://www.example.org/Packages/userland/10-raspberrypi-firmware.rules
Source2: http://www.example.org/Packages/userland/raspberrypi-firmware.sh
Source3: http://www.example.org/Packages/userland/00-raspberrypi-firmware.conf
#	Build Requirements
BuildRequires: cmake
%description
Broadcomm user utilities

%prep
%setup -q -n %{name}-54fd97a
mkdir build

%build
cd build
_options=("-DCMAKE_BUILD_TYPE=Release" "..")
cmake "${_options[@]}"
%{make_build}

%install
cd build
%{make_install}
#	Install man pages to /usr/share/man/ not to /opt/vc/man/
mkdir -p %{buildroot}/usr/share/man/man{1,7}
mv %{buildroot}/opt/vc/man/man1/*.1 %{buildroot}/usr/share/man/man1
mv %{buildroot}/opt/vc/man/man7/*.7 %{buildroot}/usr/share/man/man7
rm -rf %{buildroot}/opt/vc/man
#	setup permissions on video group for /opt/vc/bin/vcgencmd
install -Dm0644 %{SOURCE1} %{buildroot}/usr/lib/udev/rules.d/10-raspberrypi-firmware.rules
#	setup PATH to hit /opt/vc/bin/
install -Dt %{buildroot}/etc/profile.d -m644 %{SOURCE2}
#	Remove executable flag on shared objects
find %{buildroot} -type f -name '*.so' -print0 | xargs -0 chmod -x
#	Create lib links
mkdir -p %{buildroot}/etc/ld.so.conf.d/
install -m644 %{SOURCE3} %{buildroot}/etc/ld.so.conf.d/00-raspberrypi-firmware.conf
touch %{buildroot}/opt/vc/include/interface/vcos/glibc/.hidden
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENCE

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 20221019-1
-	Initial build.	First version
