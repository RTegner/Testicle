Summary: Time zone data
Name: tzdata
Version: 2022g
Release: 1
License: public-domain
Group: Core
URL: http://www.iana.org/time-zones
#	Source
Source0: http://www.example.org/Packages/tzdata/tzdata2022g.tar.gz
Source1: http://www.example.org/Packages/tzdata/tzcode2022g.tar.gz
%description
Time zone and daylight saving time data

%prep
tar xf %{SOURCE0}
tar xf %{SOURCE1}
sed -i "s:sbin:bin:g" Makefile

%build
make

%install
#	install tzcode stuff
%{make_install}
#	install -D -m644 %{_sourcedir}/tmpfiles %{buildroot}/usr/lib/tmpfiles.d/%{name}.conf
#	install tzdata stuff
_timezones=('africa' 'antarctica' 'asia' 'australasia' 'europe' 'northamerica' 'southamerica' 'etcetera' 'backward' 'factory')
mkdir -pv %{buildroot}/usr/share/zoneinfo/{posix,right}
./zic -b fat -d %{buildroot}/usr/share/zoneinfo ${_timezones[@]}
./zic -b fat -d %{buildroot}/usr/share/zoneinfo/posix ${_timezones[@]}
./zic -b fat -d %{buildroot}/usr/share/zoneinfo/right -L leapseconds ${_timezones[@]}
#	This creates the posixrules file. We use New York because POSIX requires the daylight savings time rules to be in accordance with US rules.
./zic -b fat -d %{buildroot}/usr/share/zoneinfo -p America/New_York
#	zone.tab is depricated and will go soon
install -m644 -t %{buildroot}/usr/share/zoneinfo iso3166.tab leap-seconds.list zone1970.tab zone.tab SECURITY
#	cleanup
rm %{buildroot}/etc/localtime
rm -rf %{buildroot}/usr/share/man
#	set timezone
mkdir -vp %{buildroot}/etc
ln -vs /usr/share/zoneinfo/EST5EDT %{buildroot}/etc/localtime
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2022g-1
-	Initial build.	First version
