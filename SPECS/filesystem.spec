Summary: Default file system
Name: filesystem
Version: 2022.10.18
Release: 1
License: GPL
Group: Base
URL: http://www.example.org
#	Source
Source0: http://www.example.org/Packages/filesystem/10-eth0.network
Source1: http://www.example.org/Packages/filesystem/crypttab
Source2: http://www.example.org/Packages/filesystem/env-generator
Source3: http://www.example.org/Packages/filesystem/fstab
Source4: http://www.example.org/Packages/filesystem/group
Source5: http://www.example.org/Packages/filesystem/gshadow
Source6: http://www.example.org/Packages/filesystem/host.conf
Source7: http://www.example.org/Packages/filesystem/hosts
Source8: http://www.example.org/Packages/filesystem/issue
Source9: http://www.example.org/Packages/filesystem/ld.so.conf
Source10: http://www.example.org/Packages/filesystem/locale.sh
Source11: http://www.example.org/Packages/filesystem/netgroup
Source12: http://www.example.org/Packages/filesystem/os-release
Source13: http://www.example.org/Packages/filesystem/passwd
Source14: http://www.example.org/Packages/filesystem/profile
Source15: http://www.example.org/Packages/filesystem/securetty
Source16: http://www.example.org/Packages/filesystem/shadow
Source17: http://www.example.org/Packages/filesystem/shells
Source18: http://www.example.org/Packages/filesystem/sysctl
Source19: http://www.example.org/Packages/filesystem/sysusers
Source20: http://www.example.org/Packages/filesystem/tmpfiles
%description
Filesystem contains the basic directory layout

%prep
true

%build
true

%install
#	setup root filesystem
for d in boot dev etc home mnt usr var opt srv/http run; do
	install -d -m755 %{buildroot}/${d}
done
install -d -m555 %{buildroot}/proc
install -d -m555 %{buildroot}/sys
install -d -m0750 %{buildroot}/root
install -d -m1777 %{buildroot}/tmp

#	setup /etc and /usr/share/factory/etc
install -d %{buildroot}/etc/{ld.so.conf.d,skel,profile.d} %{buildroot}/usr/share/factory/etc

#	group
#	passwd
for f in group passwd fstab host.conf hosts ld.so.conf netgroup securetty shells profile; do
	install -m644 %{_sourcedir}/${f} %{buildroot}/etc/
	install -m644 %{_sourcedir}/${f} %{buildroot}/usr/share/factory/etc/
done
mkdir -vp %{buildroot}/etc/systemd/network
install -m644 %{_sourcedir}/10-eth0.network %{buildroot}/etc/systemd/network/10-eth0.network

ln -s ../proc/self/mounts %{buildroot}/etc/mtab
for f in gshadow shadow crypttab; do
	install -m600 %{_sourcedir}/${f} %{buildroot}/etc/
	install -m600 %{_sourcedir}/${f} %{buildroot}/usr/share/factory/etc/
done

install -m644 %{_sourcedir}/locale.sh %{buildroot}/etc/profile.d/locale.sh
install -Dm644 %{_sourcedir}/os-release %{buildroot}/usr/lib/os-release
#	setup /var
for d in cache local opt log/old lib/misc empty; do
	install -d -m755 %{buildroot}/var/${d}
done
install -d -m1777 %{buildroot}/var/{tmp,spool/mail}

#	setup /usr hierarchy
for d in bin include lib share/misc src; do
	install -d -m755 %{buildroot}/usr/${d}
done
for d in {1..8}; do
	install -d -m755 %{buildroot}/usr/share/man/man${d}
done

#	add lib symlinks
ln -s usr/lib %{buildroot}/lib
#	add bin symlinks
ln -s usr/bin %{buildroot}/bin
ln -s usr/bin %{buildroot}/sbin
ln -s bin %{buildroot}/usr/sbin

#	setup /usr/local hierarchy
for d in bin etc games include lib man sbin share src; do
	install -d -m755 %{buildroot}/usr/local/${d}
done
ln -s ../man %{buildroot}/usr/local/share/man

#	setup systemd-sysctl
install -D -m644 %{_sourcedir}/sysctl %{buildroot}/usr/lib/sysctl.d/10-gremlin.conf

#	setup systemd-sysusers
install -D -m644 %{_sourcedir}/sysusers %{buildroot}/usr/lib/sysusers.d/gremlin.conf

#	setup systemd-tmpfiles
install -D -m644 %{_sourcedir}/tmpfiles %{buildroot}/usr/lib/tmpfiles.d/gremlin.conf

#	setup systemd.environment-generator
install -D -m755 %{_sourcedir}/env-generator %{buildroot}/usr/lib/systemd/system-environment-generators/10-gremlin
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
	%defattr(-,root,root)
#	Files
%config(noreplace) /etc/crypttab
%config(noreplace) /etc/fstab
%config(noreplace) /etc/group
%config(noreplace) /etc/gshadow
%config(noreplace) /etc/host.conf
%config(noreplace) /etc/hosts
%config(noreplace) /etc/netgroup
%config(noreplace) /etc/passwd
%config(noreplace) /etc/profile
%config(noreplace) /etc/securetty
%config(noreplace) /etc/shadow
%config(noreplace) /etc/shells
%dir /root
%dir /var/cache
%dir /var/local
%dir /var/empty
%dir /var/opt
%dir /var/tmp
%dir /var/log/old
%dir /var/lib/misc
%dir /var/spool/mail
%dir /dev
%dir /sys
%dir /home
%dir /boot
%dir /etc/skel
%dir /etc/ld.so.conf.d
%dir /usr/share/misc
%dir /usr/share/man/man2
%dir /usr/share/man/man4
%dir /usr/share/man/man5
%dir /usr/share/man/man7
%dir /usr/share/man/man6
%dir /usr/share/man/man3
%dir /usr/share/man/man1
%dir /usr/share/man/man8
%dir /usr/include
%dir /usr/src
%dir /usr/bin
%dir /usr/local/games
%dir /usr/local/sbin
%dir /usr/local/include
%dir /usr/local/src
%dir /usr/local/bin
%dir /usr/local/etc
%dir /usr/local/lib
%dir /usr/local/man
%dir /opt
%dir /srv/http
%dir /tmp
%dir /run
%dir /proc
%dir /mnt

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2022.10.18-1
-	Initial build.	First version
