Summary: The GNU Bourne Again shell
Name: bash
#Version: 5.1.16
Version: 5.2.15
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/bash/
#	Source
#	Source0: http://www.example.org/Packages/bash/bash-5.1.16.tar.gz
Source0: http://www.example.org/Packages/bash/bash-5.2.15.tar.gz
Source1: http://www.example.org/Packages/bash/dot.bashrc
Source2: http://www.example.org/Packages/bash/dot.bash_profile
Source3: http://www.example.org/Packages/bash/dot.bash_logout
Source4: http://www.example.org/Packages/bash/system.bashrc
Source5: http://www.example.org/Packages/bash/system.bash_logout
Provides: /usr/bin/bash
Provides: /usr/bin/sh
Provides: /bin/sh
%description
The GNU Bourne Again shell

%prep
%setup -q -n %{name}-%{version}

%build
_bashconfig=(-DDEFAULT_PATH_VALUE=\'"/usr/local/sbin:/usr/local/bin:/usr/bin"\'
	-DSTANDARD_UTILS_PATH=\'"/usr/bin"\'
	-DSYS_BASHRC=\'"/etc/bash.bashrc"\'
	-DSYS_BASH_LOGOUT=\'"/etc/bash.bash_logout"\'
	-DNON_INTERACTIVE_LOGIN_SHELLS)
#export CFLAGS+="-O2 -g ${_bashconfig[@]}"
#_options=(--prefix=/usr
#	--without-bash-malloc
#	--with-installed-readline
#	--docdir=/usr/share/doc/%{name}

_options=(--prefix=/usr
    --with-curses
    --enable-readline
    --without-bash-malloc
    --with-installed-readline)
#	--with-curses
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
ln -vs bash %{buildroot}/usr/bin/sh
#	system-wide configuration files
install -Dm644 %{SOURCE4} %{buildroot}/etc/bash.bashrc
install -Dm644 %{SOURCE5} %{buildroot}/etc/bash.bash_logout
#	user configuration file skeletons
install -dm755 %{buildroot}/etc/skel/
install -m644 %{SOURCE1} %{buildroot}/etc/skel/.bashrc
install -m644 %{SOURCE2} %{buildroot}/etc/skel/.bash_profile
install -m644 %{SOURCE3} %{buildroot}/etc/skel/.bash_logout
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) /etc/bash.bashrc
%config(noreplace) /etc/bash.bash_logout
%config(noreplace) /etc/skel/.bashrc
%config(noreplace) /etc/skel/.bash_profile
%config(noreplace) /etc/skel/.bash_logout

%changelog
*	Tue Jan 24 2023 scott andrews <scott-andrews@columbus.rr.com> 5.2.15-1
-	Update to 5.2.15
*	Tue Jan 10 2023 scott andrews <scott-andrews@columbus.rr.com> 5.1.16-1
-	Initial build.	First version
