Summary: A fast and versatile file copying tool for remote and local files
Name: rsync
Version: 3.2.7
Release: 1
License: GPL3
Group: Core
URL: https://rsync.samba.org/
#	Source
Source0: http://www.example.org/Packages/rsync/rsync-3.2.7.tar.gz
#	Build Requirements
BuildRequires: git
#	BuildRequires: python-commonmark
#	BuildRequires: zstd
#	Add these
#	BuildRequires: xxhash
#	BuildRequires: lz4
#	depends=('acl' 'libacl.so' 'lz4' 'openssl' 'popt' 'xxhash' 'libxxhash.so' 'zlib' 'zstd')
#	optdepends=('python: for rrsync')
#	makedepends=('git' 'python-commonmark')
%description
A fast and versatile file copying tool for remote and local files

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--disable-debug
	--with-included-popt=no
	--with-included-zlib=no
#	mine
	--disable-xxhash
	--disable-lz4
	--disable-zstd)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.2.7-1
-	Initial build.	First version
