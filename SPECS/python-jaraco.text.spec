%define python_ver 3.11
Summary: Module for text manipulation
Name: python-jaraco.text
Version: 3.11.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/jaraco/jaraco.text
#	Source
Source0: http://www.example.org/Packages/python-jaraco.text/jaraco.text-3.11.0.tar.gz
#Requires: python
#Requires: python-jaraco.functools
#Requires: python-jaraco.context
#Requires: python-autocommand
#Requires: python-inflect
#Requires: python-more-itertools
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-setuptools-scm
#	depends=('python' 'python-jaraco.functools' 'python-jaraco.context' 'python-autocommand' 'python-inflect' 'python-more-itertools')
#	makedepends=('python-build' 'python-installer' 'python-setuptools-scm' 'python-wheel')
#	checkdepends=('python-pytest')

%description
Module for text manipulation

%prep
%setup -q -n jaraco.text-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir=%{buildroot} dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
"/usr/lib/python%{python_ver}/site-packages/jaraco/text/Lorem ipsum.txt"
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 3.11.0-1
-	Initial build.	First version
