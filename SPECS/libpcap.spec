Summary: A system-independent interface for user-level packet capture
Name: libpcap
Version: 1.10.3
Release: 1
License: BSD
Group: Networking
URL: https://www.tcpdump.org
#	Source
Source0: http://www.example.org/Packages/libpcap/libpcap-1.10.3.tar.gz
%description
A system-independent interface for user-level packet capture

%prep
%setup -q -n %{name}-%{version}
autoreconf -fiv

%build
_options=(--prefix=/usr
	--enable-ipv6
#	--enable-bluetooth
        --enable-usb
	--with-libnl)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Thu Jan 26 2023 scott andrews <scott-andrews@columbus.rr.com> 1.10.3-1
-	Update Version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.10.1-1
-	Initial build.	First version
