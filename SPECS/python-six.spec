Summary: Python 2 and 3 compatibility utilities
Name: python-six
Version: 1.16.0
Release: 1
License: MIT
Group: Python
URL: https://pypi.python.org/pypi/six/
#	Source
Source0: http://www.example.org/Packages/python-six/six-1.16.0.tar.gz
#	depends=('python')
#	makedepends=('python-setuptools')
#	checkdepends=('python-pytest' 'tk')
Requires: python
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-toml
BuildRequires: python-tomli
BuildRequires: python-wheel
BuildRequires: python-setuptools
%description


%prep
%setup -q -n six-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Jan  2 2023 scott andrews <scott-andrews@columbus.rr.com> %{version}-%{release}
-	ToolChain
