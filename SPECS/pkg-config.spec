Summary: A system for managing library compile/link flags
Name: pkg-config
Version: 0.29.2
Release: 1
License: GPLv2
Group: Core
URL: https://www.freedesktop.org/wiki/Software/pkg-config
#	Source
Source0: http://www.example.org/Packages/pkg-config/pkg-config-0.29.2.tar.gz
Provides: /usr/bin/pkg-config
%description
A system for managing library compile/link flags
 
%prep
%setup -q -n pkg-config-%{version}
 
%build
_options=(--prefix=/usr
	--build="${MACHTYPE}")
#	--with-internal-glib)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.29.2-1
-	Initial build.	First version
