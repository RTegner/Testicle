Summary: Backported and Experimental Type Hints for Python 3.7+
Name: python-typing_extensions
Version: 4.4.0
Release: 1
License: custom
Group: Python
URL: https://github.com/python/typing_extensions
#	Source
Source0: http://www.example.org/Packages/python-typing_extensions/python-typing_extensions-4.4.0.tar.gz
#Requires: python
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-flit
#	depends=(python)
#	makedepends=(git python-build python-flit-core python-installer)
#	checkdepends=(python-tests)
%description
Backported and Experimental Type Hints for Python 3.7+

%prep
%setup -q -n typing_extensions-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 4.4.0-1
-	Initial build.	First version
