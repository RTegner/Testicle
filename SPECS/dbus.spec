Summary: D-Bus is a message bus system.
Name: dbus
Version: 1.14.4
Release: 1
License: GPL custom)
Group: Core
URL: https://wiki.freedesktop.org/www/Software/dbus
#	Source
Source0: http://www.example.org/Packages/dbus/dbus-1.14.4.tar.xz
Source1: http://www.example.org/Packages/dbus/no-fatal-warnings.diff
Source2: http://www.example.org/Packages/dbus/tmpfiles
%description
Summary=D-Bus is a message bus system, a simple way for applications to talk to one another.

%prep
%setup -q -n %{name}-%{version}
#	Allow us to enable checks without them being fatal
patch -Np1 -i %{_sourcedir}/no-fatal-warnings.diff
NOCONFIGURE=1 ./autogen.sh

%build
_options=(
	--prefix=/usr
	--sysconfdir=/etc
	--localstatedir=/var
	--libexecdir=/usr/lib/dbus-1.0
	--runstatedir=/run
	--with-console-auth-dir=/run/console/
	--with-dbus-user=dbus
	--with-system-pid-file=/run/dbus/pid
	--with-system-socket=/run/dbus/system_bus_socket
	--with-systemdsystemunitdir=/usr/lib/systemd/system
	--enable-inotify
	#	--enable-libaudit
	--enable-systemd
	--enable-user-session
	#	--enable-xml-docs
	#	--enable-doxygen-docs
	#	--enable-ducktype-docs
	--disable-static
	--without-x
)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
install -D -m644 "%{_sourcedir}/tmpfiles" "%{buildroot}/usr/lib/tmpfiles.d/%{name}.conf"
mkdir -vp %{buildroot}/var/lib
ln -sfv /etc/machine-id %{buildroot}/var/lib/dbus
rmdir %{buildroot}/usr/share/dbus-1/session.d
rmdir %{buildroot}/usr/share/dbus-1/system.d
rmdir %{buildroot}/usr/share/dbus-1/services
rmdir %{buildroot}/usr/share/dbus-1/system-services
rm -r %{buildroot}/var/run
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.14.4-1
-	Initial build.	First version
