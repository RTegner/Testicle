%define __requires_exclude perl\\((WWW::Curl::Easy))
Summary: The Open Source toolkit for Secure Sockets Layer and Transport Layer Security
Name: openssl
Version: 3.0.7
Release: 1
License: GPL
Group: Core
URL: https://openssl.org
#	Source
Source0: http://www.example.org/Packages/openssl/openssl-3.0.7.tar.gz
%description
The Open Source toolkit for Secure Sockets Layer and Transport Layer Security

%prep
%setup -q -n %{name}-%{version}
sed '/my $CATOP/s/.\/demoCA/\/etc\/ssl/' -i apps/CA.pl.in

%build
_options=(--prefix=/usr
	--openssldir=/etc/ssl
	--libdir=lib
	shared
	enable-ktls
#	enable-ec_nistp_64_gcc_128
#	linux-aarch64
	linux-armv4
	"-Wa,--noexecstack")
./Configure "${_options[@]}"
%{make_build} depend
%{make_build}

%install
%{make_install} MANDIR=/usr/share/man MANSUFFIX=ssl install_sw install_ssldirs install_man_docs
touch %{buildroot}/etc/ssl/certs/.hidden
touch %{buildroot}/etc/ssl/private/.hidden
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE.txt

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.0.7-1
-	Initial build.	First version
