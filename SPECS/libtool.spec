Summary: A generic library support script
Name: libtool
Version: 2.4.7
Release: 1
License: GPLv2
Group: Core
URL: http://www.gnu.org/software/libtool
#	Source
Source0: http://www.example.org/Packages/libtool/libtool-2.4.7.tar.xz
Source1: http://www.example.org/Packages/libtool/no_hostname.patch
Source2: http://www.example.org/Packages/libtool/disable-lto-link-order2.patch
#	Build Requirements
BuildRequires: help2man
%description
A generic library support script
 
%prep
%setup -q -n %{name}-%{version}
patch -Np1 -i %{SOURCE1}
#	test 67 is broken with lto
#	this patch removes the -flto flag for this very test
#	adapt when -ffat-lto-objects is enabled by Arch
patch -Np1 -i %{SOURCE2}
./bootstrap --force
 
%build
_options=(--prefix=/usr lt_cv_sys_lib_dlsearch_path_spec="/usr/lib")
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.4.7-1
-	Initial build.	First version
