Summary: Jinja2 is a Python module that implements a simple pythonic template lanuage.
Name: python-jinja
Version: 3.1.2
Release: 1
License: Unknown
Group: Python
URL: Unknown
#	Source
Source0: http://www.example.org/Packages/python-jinja/Jinja2-3.1.2.tar.gz
%description
Jinja2 is a Python module that implements a simple pythonic template lanuage.

%prep
%setup -q -n Jinja2-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir=%{buildroot} dist/*.whl
#pkg=Jinja2
#pkg=${pkg##python-}
#python -m pip install --no-index --find-links dist --no-cache-dir --no-user --root %{buildroot} ${pkg}
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/Jinja2-%{version}.dist-info
%license LICENSE.rst

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 3.1.2-1
-	Initial build.	First version
