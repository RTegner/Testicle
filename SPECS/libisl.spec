Summary: Library for manipulating sets and relations of integer points bounded by linear constraints
Name: libisl
Version: 0.25
Release: 1
License: MIT
Group: Core
URL: http://freecode.com/projects/isl
#	Source
Source0: http://www.example.org/Packages/libisl/isl-0.25.tar.xz
%description
Library for manipulating sets and relations of integer points bounded by linear constraints

%prep
%setup -q -n isl-%{version}

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
install -dm755 %{buildroot}/usr/share/gdb/auto-load/usr/lib/
mv %{buildroot}/usr/lib/libisl.so.*-gdb.py %{buildroot}/usr/share/gdb/auto-load/usr/lib/

%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Mon Jan  2 2023 scott andrews <scott-andrews@columbus.rr.com> %{version}-%{release}
-	ToolChain
