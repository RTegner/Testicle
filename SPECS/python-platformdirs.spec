Summary: A small Python module for determining appropriate platform-specific dirs, e.g. a user data dir
Name: python-platformdirs
Version: 2.6.2
Release: 1
License: MIT
Group: Python
URL: "https://github.com/platformdirs/platformdirs"
Source0: http://www.example.org/Packages/python-platformdirs/platformdirs-2.6.2.tar.gz

%description
A small Python module for determining appropriate platform-specific dirs, e.g. a user data dir

%prep
%setup -q -n platformdirs-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps $PWD

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE
%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 2.6.2-1
-	Initial build.	First version
