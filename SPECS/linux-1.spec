Summary: Raspberry Pi 4 kernel
Name: linux
Version: 1.20230106
Release: 1
License: GPLv2
Group: Core
URL: https://github.com/raspberrypi/linux
#	Source
Source0: http://www.example.org/Packages/firmware/1.20230106.tar.gz
%description
Raspberry Pi 4 kernel

%prep
%setup -q -n firmware-%{version}

%build

%install
mkdir -vp %{buildroot}/boot/overlays
cp -v boot/*.dtb %{buildroot}/boot/
cp -v boot/*.img %{buildroot}/boot/
cp -vr boot/overlays %{buildroot}/boot/
mkdir -vp %{buildroot}/usr/lib/
cp -vr modules %{buildroot}/usr/lib/

#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Jan 23 2023 scott andrews <scott-andrews@columbus.rr.com> 1.20230106-1
-	Major update
