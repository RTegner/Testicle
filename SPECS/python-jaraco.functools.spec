Summary: Functools like those found in stdlib
Name: python-jaraco.functools
Version: 3.5.2
Release: 1
License: MIT
Group: Python
URL: https://github.com/jaraco/jaraco.functools
#	Source
Source0: http://www.example.org/Packages/python-jaraco-functools/jaraco.functools-3.5.2.tar.gz
#Requires: python
#Requires: python-more-itertools
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-setuptools-scm
#BuildRequires: python-more-itertools
#	depends=('python-more-itertools')
#	makedepends=('python-build' 'python-installer' 'python-setuptools-scm' 'python-wheel')
#	checkdepends=('python-pytest' 'python-jaraco.classes')

%description
Functools like those found in stdlib

%prep
%setup -q -n jaraco.functools-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Nov 26 2022 scott andrews <scott-andrews@columbus.rr.com> 3.5.2-1
-	Initial build.	First version
