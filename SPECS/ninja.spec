Summary: Ninja is a small build system with a focus on speed
Name: ninja
Version: 1.11.1
Release: 1
License: Apache
Group: Core
URL: https://ninja-build.org
#	Source
Source0: http://www.example.org/Packages/ninja/ninja-1.11.1.tar.gz
%description
Ninja is a small build system with a focus on speed

%prep
%setup -q -n %{name}-%{version}

%build
python3 configure.py --bootstrap

%install
install -vdm 755 %{buildroot}/usr/bin/
install -vm755 ninja %{buildroot}/usr/bin/
install -vDm644 misc/bash-completion %{buildroot}/usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion  %{buildroot}/usr/share/zsh/site-functions/_ninja
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 1.11.1-1
-	Initial build.	First version
