Summary: The Tcl scripting language
Name: tcl
Version: 8.6.13
Release: 1
License: Custom
Group: Core
URL: http://tcl.sourceforge.net/
#	Source
Source0: http://www.example.org/Packages/tcl/tcl8.6.13-src.tar.gz
%description
The Tcl scripting language

%prep
%setup -q -n %{name}%{version}

%build
cd unix
_options=(--prefix=/usr
	--mandir=/usr/share/man
	--enable-threads
#	--enable-64bit
)
./configure "${_options[@]}"
%{make_build}

%install
cd unix
pkgver=%{version}
srcdir=$(pwd)
_tclver=8.6
tdbcver=tdbc1.1.5
itclver=itcl4.2.3

/usr/bin/make INSTALL_ROOT="%{buildroot}" install install-private-headers
ln -sf tclsh${pkgver%.*} %{buildroot}/usr/bin/tclsh
ln -sf libtcl${pkgver%.*}.so %{buildroot}/usr/lib/libtcl.so
install -Dm644 tcl.m4 -t %{buildroot}/usr/share/aclocal
chmod 644 %{buildroot}/usr/lib/libtclstub8.6.a
#	remove buildroot traces
sed "s#${srcdir}/tcl${pkgver}/unix#/usr/lib#" -i %{buildroot}/usr/lib/tclConfig.sh
sed "s#${srcdir}/tcl${pkgver}#/usr/include#" -i %{buildroot}/usr/lib/tclConfig.sh
sed -e "s#'{/usr/lib} '#'/usr/lib/tcl${_tclver}'#" -i %{buildroot}/usr/lib/tclConfig.sh
sed "s#${srcdir}/tcl${pkgver}/unix/pkgs/${tdbcver}#/usr/lib/${tdbcver}#"  -i %{buildroot}/usr/lib/${tdbcver}/tdbcConfig.sh
sed "s#${srcdir}/tcl${pkgver}/pkgs/${tdbcver}/generic#/usr/include#"  -i %{buildroot}/usr/lib/${tdbcver}/tdbcConfig.sh
sed "s#${srcdir}/tcl${pkgver}/pkgs/${tdbcver}/library#/usr/lib/tcl${pkgver%.*}#"  -i %{buildroot}/usr/lib/${tdbcver}/tdbcConfig.sh
sed "s#${srcdir}/tcl${pkgver}/pkgs/${tdbcver}#/usr/include#"  -i %{buildroot}/usr/lib/${tdbcver}/tdbcConfig.sh
sed "s#${srcdir}/tcl${pkgver}/unix/pkgs/${itclver}#/usr/lib/${itclver}#" -i %{buildroot}/usr/lib/${itclver}/itclConfig.sh
sed "s#${srcdir}/tcl${pkgver}/pkgs/${itclver}/generic#/usr/include#" -i %{buildroot}/usr/lib/${itclver}/itclConfig.sh
sed "s#${srcdir}/tcl${pkgver}/pkgs/${itclver}#/usr/include#" -i %{buildroot}/usr/lib/${itclver}/itclConfig.sh
#	%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license license.terms

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 8.6.13-1
-	Initial build.	First version
