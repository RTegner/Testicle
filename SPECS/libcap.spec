Summary: POSIX 1003.1e capabilities
Name: libcap
Version: 2.66
Release: 1
License: GPLv2
Group: Core
URL: https://sites.google.com/site/fullycapable
#	Source
Source0: http://www.example.org/Packages/libcap/libcap-2.66.tar.xz
%description
POSIX 1003.1e capabilities
 
%prep
%setup -q -n %{name}-%{version}
sed  '/install -m.*STA/d' -i libcap/Makefile
 
%build
%{make_build} DYNAMIC=yes KERNEL_HEADERS=/usr/include lib=lib prefix=/usr sbindir=bin
 
%install
%{make_install} RAISE_SETFCAP=no lib=lib prefix=/usr sbindir=bin
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.66-1
-	Initial build.	First version
