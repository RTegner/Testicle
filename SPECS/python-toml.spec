Summary: A Python library for parsing and creating TOML
Name: python-toml
Version: 0.10.2
Release: 1
License: MIT
Group: Python
URL: https://github.com/uiri/tom
#	Source
Source0: http://www.example.org/Packages/python-toml/toml-0.10.2.tar.gz
Source1: http://www.example.org/Packages/python-toml/python-toml-0.10.1-install_type_hints.patch
#Requires: python
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#	depends=('python')
#	makedepends=('python-build' 'python-installer' 'python-setuptools'
#		'python-wheel')
#	checkdepends=('python-numpy' 'python-pytest')
%description
A Python library for parsing and creating TOML

%prep
%setup -q -n toml-%{version}
patch -Np1 -i %{SOURCE1}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.10.2-1
-	Initial build.	First version
