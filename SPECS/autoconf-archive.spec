Summary: A collection of freely re-usable Autoconf macros
Name: autoconf-archive
Version: 2022.09.03
Release: 1
License: GPL3
Group: Core
URL: https://www.gnu.org/software/autoconf-archive
#	Source
Source0: http://www.example.org/Packages/autoconf-archive/autoconf-archive-2022.09.03.tar.xz
%description
A collection of freely re-usable Autoconf macros
 
%prep
%setup -q -n %{name}-%{version}
 
%build
./configure --prefix=/usr
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2022.09.03-1
-	Initial build.	First version
