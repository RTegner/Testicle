Summary: General-purpose scalable concurrent malloc implementation
Name: jemalloc
Version: 5.3.0
Release: 1
License: BSD
Group: Library
URL: http://www.canonware.com/jemalloc
#	Source
Source0: http://www.example.org/Packages/jemalloc/jemalloc-5.3.0.tar.bz2
%description
General-purpose scalable concurrent malloc implementation
 
%prep
%setup -q -n %{name}-%{version}
 
%build

#	 FS#71745: GCC-built jemalloc causes telegram-desktop to crash a lot. The reason is still not clear.
#	export CC=clang
#	export CXX=clang++
_options=( "--enable-autogen" "--prefix=/usr" )
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
chmod 644 %{buildroot}/usr/lib/libjemalloc_pic.a
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
 
%changelog
*	Sun Jan 01 2023 scott andrews <scott-andrews@columbus.rr.com> 5.3.0-1
-	Initial build.	First version
