Summary: GNU database library
Name: gdbm
Version: 1.23
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/gdbm
#	Source
Source0: http://www.example.org/Packages/gdbm/gdbm-1.23.tar.gz
%description
GNU database library
 
%prep
%setup -q -n %{name}-%{version}
autoreconf -fiv
 
%build
_options=(--prefix=/usr --enable-libgdbm-compat)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.23-1
-	Initial build.	First version
