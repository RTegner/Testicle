Summary: The GNU Compiler Collection
Name: gcc
Version: 12.2.0
Release: 1
License: GPLv2
Group: Core
URL: https://gcc.gnu.org
#	Source
Source0: http://www.example.org/Packages/gcc/gcc-12.2.0.tar.xz
Source1: http://www.example.org/Packages/gcc/c89
Source2: http://www.example.org/Packages/gcc/c99
Source3: http://www.example.org/Packages/gcc/gcc-ada-repro.patch
%description
The GNU Compiler Collection

%prep
%setup -q -n %{name}-%{version}
#	Do not run fixincludes
sed -i 's@\./fixinc\.sh@-c true@' gcc/Makefile.in
#	install libraries into /lib not lib64
sed -i '/lp64=/s/lib64/lib/' gcc/config/aarch64/t-aarch64-linux

%build
mkdir build;cd build
_options=(--prefix=/usr
	--libdir=/usr/lib
	--libexecdir=/usr/lib
	--mandir=/usr/share/man
	--infodir=/usr/share/info
	--disable-multilib
	--disable-bootstrap
	--disable-libssp
	--disable-libstdcxx-pch
	--disable-werror
	--enable-languages=c,c++
	--enable-__cxa_atexit
	--enable-checking=release
	--enable-clocale=gnu
	--enable-default-pie
	--enable-default-ssp
	--enable-gnu-indirect-function
	--enable-gnu-unique-object
	--enable-libstdcxx-backtrace
	--enable-link-serialization=1
	--enable-linker-build-id
	--enable-lto
	--enable-plugin
	--enable-shared
	--enable-threads=posix
	--with-system-zlib
	--with-linker-hash-style=gnu)
../configure "${_options[@]}" %{CONFIG_ARM}
%{make_build}

%install
pushd build;%{make_install};popd
ln -svr /usr/bin/cpp %{buildroot}/usr/lib
mkdir -vp %{buildroot}/usr/lib/bfd-plugins
ln -sfv ../../libexec/gcc/$(gcc -dumpmachine)/%{version}/liblto_plugin.so %{buildroot}/usr/lib/bfd-plugins
install -d %{buildroot}/usr/share/gdb/auto-load/usr/lib
mv %{buildroot}/usr/lib/libstdc++.so.6.*-gdb.py %{buildroot}/usr/share/gdb/auto-load/usr/lib/
#	POSIX conformance launcher scripts for c89 and c99
install -Dm755 %{_sourcedir}/c89 %{buildroot}/usr/bin/c89
install -Dm755 %{_sourcedir}/c99 %{buildroot}/usr/bin/c99
ln -sv gcc %{buildroot}/usr/bin/cc
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 12.2.0-1
-	Initial build.	First version
