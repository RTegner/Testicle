Summary: Linux man pages
Name: man-pages
Version: 6.02
Release: 1
License: GPLv2
Group: Core
URL: http://www.kernel.org/doc/man-pages
#	Source
Source0: http://www.example.org/Packages/man-pages/man-pages-6.02.tar.xz
#Source1: http://www.example.org/Packages/man-pages/man-pages-posix-2017-a.tar.xz
%description
Linux man pages

%prep
%setup -q -n %{name}-%{version}
#	tar xf %%{SOURCE1}
sed -i "s|prefix := /usr/local|prefix := /usr|g" Makefile
#	included in shadow
rm man5/passwd.5
rm man3/getspnam.3
#	included in tzdata
rm	man5/tzfile.5 man8/{tzselect,zdump,zic}.8
#	included in libxcrypt
rm man3/crypt*.3

%build
true

%install
#	install man-pages
make DESTDIR=%{buildroot} prefix=/usr install
#	install posix pages
#	pushd %{name}-posix-2017
#	make DESTDIR=%{buildroot} install
#	popd
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%defattr(-,root,root)
#	%%license %{name}-posix-2017/POSIX-COPYRIGHT

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 6.02-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 6.01-1
-	Initial build.	First version
