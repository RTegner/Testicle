Summary: A high-quality data compression program
Name: bzip2
Version: 1.0.8
Release: 1
License: Other
Group: Core
URL: https://sourceware.org/bzip2/
#	Source
Source0: http://www.example.org/Packages/bzip2/bzip2-1.0.8.tar.gz
Source1: http://www.example.org/Packages/bzip2/bzip2.pc
%description
A high-quality data compression program

%prep
%setup -q -n %{name}-%{version}
cp %{_sourcedir}/bzip2.pc bzip2.pc
sed "s|@VERSION@|%{version}|" -i bzip2.pc

%build
make -f Makefile-libbz2_so CC="gcc"
make bzip2 bzip2recover CC="gcc"

%install
install -dm755 %{buildroot}/usr/{bin,lib,include,share/man/man1}
install -m755 bzip2-shared %{buildroot}/usr/bin/bzip2
install -m755 bzip2recover bzdiff bzgrep bzmore %{buildroot}/usr/bin
ln -sf bzip2 %{buildroot}/usr/bin/bunzip2
ln -sf bzip2 %{buildroot}/usr/bin/bzcat

cp -a libbz2.so* %{buildroot}/usr/lib
ln -s libbz2.so.%{version} %{buildroot}/usr/lib/libbz2.so
ln -s libbz2.so.%{version} %{buildroot}/usr/lib/libbz2.so.1 # For compatibility with some other distros

install -m644 bzlib.h %{buildroot}/usr/include/

install -m644 bzip2.1 %{buildroot}/usr/share/man/man1/
ln -sf bzip2.1 %{buildroot}/usr/share/man/man1/bunzip2.1
ln -sf bzip2.1 %{buildroot}/usr/share/man/man1/bzcat.1
ln -sf bzip2.1 %{buildroot}/usr/share/man/man1/bzip2recover.1

install -Dm644 bzip2.pc -t %{buildroot}/usr/lib/pkgconfig
%strip_libs
%strip_binaries
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.0.8-1
-	Initial build.	First version
