Summary: A garbage collector for C and C++
Name: gc
Version: 8.2.2
Release: 1
License: GPL
Group: Core
URL: https://www.hboehm.info/gc/
#	Source
Source0: http://www.example.org/Packages/gc/gc-8.2.2.tar.gz
%description
A garbage collector for C and C++

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr --enable-cplusplus --disable-static)
./configure "${_options[@]}"
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
%{make_build}

%install
%{make_install}
  sed 's|GC_MALLOC 1L|gc 3|g' doc/gc.man | install -Dm644 /dev/stdin "%{buildroot}/usr/share/man/man3/gc.3"
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Jan  2 2023 scott andrews <scott-andrews@columbus.rr.com> %{version}-%{release}
-	ToolChain
