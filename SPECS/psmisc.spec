Summary: Miscellaneous procfs tools
Name: psmisc
Version: 23.6
Release: 1
License: GPLv2
Group: Core
URL: http://psmisc.sourceforge.net
#	Source
Source0: http://www.example.org/Packages/psmisc/psmisc-23.6.tar.xz
%description
Miscellaneous procfs tools

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 23.6-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 23.5-1
-	Initial build.	First version
