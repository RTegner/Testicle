Summary: Access control list utilities, libraries and headers
Name: acl
Version: 2.3.1
Release: 1
License: GPLv2
Group: Core
URL: https://savannah.nongnu.org/projects/acl
#	Source
Source0: http://www.example.org/Packages/acl/acl-2.3.1.tar.xz
%description
Access control list utilities, libraries and headers
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
	--libdir=/usr/lib
	--libexecdir=/usr/lib
	--disable-static
	--docdir=/usr/share/doc/%{name})
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.3.1-1
-	Initial build.	First version
