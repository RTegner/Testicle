Summary: The internationalization tool collection
Name: intltool
Version: 0.51.0
Release: 1
License: GPLv2
Group: Core
URL: https://freedesktop.org/wiki/Software/intltool
#	Source
Source0: http://www.example.org/Packages/intltool/intltool-0.51.0.tar.gz
Source1: http://www.example.org/Packages/intltool/intltool-0.51.0-perl-5.26.patch
Source2: http://www.example.org/Packages/intltool/intltool-merge-Create-cache-file-atomically.patch
Source3: http://www.example.org/Packages/intltool/intltool_distcheck-fix.patch
Source4: http://www.example.org/Packages/intltool/intltool-fixrace.patch
%description
The internationalization tool collection

%prep
%setup -q -n %{name}-%{version}
patch -Np1 -i %{SOURCE1}
patch -Np1 -i %{SOURCE2}
patch -Np1 -i %{SOURCE3}
patch -Np1 -i %{SOURCE4}

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
install -vDm 644 doc/I18N-HOWTO %{buildroot}/usr/share/doc/%{name}/I18N-HOWTO
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.51.0-1
-	Initial build.	First version
