Summary: Portable implementation of which
Name: perl-file-which
Version: 1.27
Release: 1
License: GPL PerlArtistic
Group: Core
URL: https://metacpan.org/release/File-Which
#	Source
Source0: http://www.example.org/Packages/perl-file-which/File-Which-1.27.tar.gz
%description
Portable implementation of which
 
%prep
%setup -q -n File-Which-%{version}
 
%build
PERL_MM_USE_DEFAULT=1 perl Makefile.PL  INSTALLDIRS=vendor
%{make_build}
 
%install
%{make_install}
find %{buildroot} -name perllocal.pod -delete
find %{buildroot} -name .packlist -delete
rm -r %{buildroot}/usr/lib
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.27-1
-	Initial build.	First version
