Summary: A cross-platform open-source make system
Name: cmake
Version: 3.25.2
Release: 1
License: Other
Group: Core
URL: https://www.cmake.org/
#	Source
Source0: http://www.example.org/Packages/cmake/cmake-3.25.2.tar.gz
#	Build Requirements
BuildRequires: libuv
BuildRequires: curl
#	BuildRequires: libarchive
%description
A cross-platform open-source make system

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--mandir=/share/man
	--docdir=/share/doc/%{name}
	--datadir=/share/%{name}
	#	--sphinx-man
	#	--sphinx-html
	#	--system-libs
	#	--qt-gui
	--parallel=$(/usr/bin/getconf _NPROCESSORS_ONLN)
	#	--no-system-jsoncpp
	#	--no-system-librhash
	)
./bootstrap "${_options[@]}"
%{make_build}

%install
%{make_install}
rm -r %{buildroot}/usr/share/cmake/Help/generator
#	emacs -batch -f batch-byte-compile "${pkgdir}"/usr/share/emacs/site-lisp/cmake-mode.el
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license Copyright.txt

%changelog
*	Tue Jan 24 2024 scott andrews <scott-andrews@columbus.rr.com> 3.25.2-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.25.0-1
-	Initial build.	First version
