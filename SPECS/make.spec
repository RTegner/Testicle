Summary: GNU make utility to maintain groups of programs
Name: make
Version: 4.4
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/make
#	Source
Source0: http://www.example.org/Packages/make/make-4.4.tar.gz
BuildRequires: glibc
BuildRequires: guile
%description
GNU make utility to maintain groups of programs

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sun Jan 08 2023 scott andrews <scott-andrews@columbus.rr.com> 4.4-1
-	Initial build.	First version
