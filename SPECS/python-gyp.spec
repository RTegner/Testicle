Summary: "Generate Your Projects" Meta-Build system
Name: python-gyp
Version: 20220404.9ecf45e3
Release: 1
License: custom:BSD
Group: Core
URL: https://gyp.gsrc.io/
#	Source
Source0: http://www.example.org/Packages/python-gyp/gyp-20220404.9ecf45e3.tgz
Source1: http://www.example.org/Packages/python-gyp/0001-gyp-python38.patch
Source2: http://www.example.org/Packages/python-gyp/0002-gyp-fix-cmake.patch
Source3: http://www.example.org/Packages/python-gyp/0003-gyp-fips.patch
#	depends=(python-six ninja)
#	makedepends=(git python-setuptools)
Requires: python-six
Requires: ninja
Requires: python
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-toml
BuildRequires: python-tomli
BuildRequires: python-wheel
BuildRequires: python-setuptools
BuildRequires: python-setuptools
%description
"Generate Your Projects" Meta-Build system

%prep
%setup -q -n gyp
git status
git apply -3 %{SOURCE1}
git apply -3 %{SOURCE2}
git apply -3 %{SOURCE3}

%build
cd %{_builddir}/gyp
python -m build --wheel --no-isolation

%install
cd %{_builddir}/gyp
python -m installer --destdir="%{buildroot}" dist/*.whl
cd -
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Jan 14 2023 scott andrews <scott-andrews@columbus.rr.com> python-gyp-20220404.9ecf45e3-1
-	ToolChain
