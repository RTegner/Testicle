Summary: A built-package format for Python
Name: python-wheel
Version: 0.38.4
Release: 1
License: MIT
Group: Python
URL: https://pypi.python.org/pypi/wheel
#	Source
Source0: http://www.example.org/Packages/python-wheel/wheel-0.38.4.tar.gz

%description
A built-package format for Python

%prep
%setup -q -n wheel-%{version}
#	do not depend on python-coverage for tests
/usr/bin/sed -i 's/--cov=wheel//' setup.cfg
#	https://github.com/pypa/wheel/pull/365 but why?
/usr/bin/rm -r src/wheel/vendored
/usr/bin/sed -i 's/from .vendored.packaging import tags/from packaging import tags/' src/wheel/bdist_wheel.py
/usr/bin/sed -i 's/from wheel.vendored.packaging import tags/from packaging import tags/' tests/test_bdist_wheel.py

%build
#	python -m build --wheel --no-isolation
#PYTHONPATH=src
python -m pip wheel -w dist --no-build-isolation --no-deps $PWD

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/wheel-%{version}.dist-info

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.38.4-1
-	Initial build.	First version
