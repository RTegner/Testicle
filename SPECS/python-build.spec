Summary: A simple, correct PEP517 package builder.
Name: python-build
Version: 0.10.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/pypa/build
#	Source
Source0: http://www.example.org/Packages/python-build/0.10.0.tar.gz
#	Build Requirements
Requires: python
Requires: python-tomli
Requires: python-pep517
Requires: python-packaging
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-toml
BuildRequires: python-tomli
#BuildRequires: python-sphinx
#BuildRequires: python-sphinx-argparse-cli
#BuildRequires: python-sphinx-autodoc-typehints
#BuildRequires: python-sphinx-furo

#	depends=('python-tomli' 'python-pep517' 'python-packaging')
#	optdepends=('python-virtualenv: Use virtualenv for build isolation')
#	makedepends=('git' 'python-build' 'python-installer' 'python-setuptools' 'python-wheel'
#		'python-sphinx' 'python-sphinx-argparse-cli' 'python-sphinx-autodoc-typehints' 'python-sphinx-furo')
#	checkdepends=('python-pytest' 'python-pytest-mock' 'python-pytest-rerunfailures' 'python-filelock' 'python-toml' 'python-wheel')


%description
A simple, correct PEP517 package builder.

%prep
%setup -q -n build-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/build-%{version}.dist-info

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 0.10.0-1
-	Update version
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.9.0-1
-	Initial build.	First version
