Summary: A unit testing framework for C
Name: check
Version: 0.15.2
Release: 1
License: GPLv2
Group: Core
URL: https://libcheck.github.io/check
#	Source
Source0: http://www.example.org/Packages/check/check-0.15.2.tar.gz
%description
A unit testing framework for C
 
%prep
%setup -q -n %{name}-%{version}
autoreconf -fvi
 
%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install} docdir=/usr/share/doc/%{name}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.15.2-1
-	Initial build.	First version
