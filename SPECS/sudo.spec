Summary: Give certain users the ability to run some commands as root
Name: sudo
Version: 1.9.12p2
Release: 1
License: Other
Group: Core
URL: https://www.sudo.ws/sudo/
#	Source
Source0: http://www.example.org/Packages/sudo/sudo-1.9.12p2.tar.gz
Source1: http://www.example.org/Packages/sudo/tmpfiles
Source2: http://www.example.org/Packages/sudo/sudo_logsrvd.service
Source3: http://www.example.org/Packages/sudo/sudo.pam
%description
Give certain users the ability to run some commands as root

%prep
%setup -q -n %{name}-%{version}

%build
_options=(
    --prefix=/usr
    --sbindir=/usr/bin
    --libexecdir=/usr/lib
    --with-rundir=/run/sudo
    --with-vardir=/var/db/sudo
    --with-logfac=auth
    --enable-tmpfiles.d
    --without-pam
#    --with-sssd
#    --with-ldap
#    --with-ldap-conf-file=/etc/openldap/ldap.conf
    --with-env-editor
    --with-passprompt="[sudo] password for %p: "
    --with-all-insults)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install} install_uid=1000 install_gid=1000 sudoers_uid=1000
#	sudo_logsrvd service file (taken from sudo-logsrvd-1.9.0-1.el8.x86_64.rpm)
install -Dm644 -t "%{buildroot}/usr/lib/systemd/system" %{SOURCE2}
#	systemd files
install -D -m644 %{SOURCE1} %{buildroot}/usr/lib/tmpfiles.d/%{name}.conf
#	Remove sudoers.dist; not needed since pacman manages updates to sudoers
rm -f "%{buildroot}/etc/sudoers.dist"
#	Remove /run/sudo directory; we create it using systemd-tmpfiles
rmdir "%{buildroot}/run/sudo"
rmdir "%{buildroot}/run"
install -Dm644 %{SOURCE3} "%{buildroot}/etc/pam.d/sudo"
#	install user rpi sudo file
install -vdm 755 %{buildroot}/etc/sudoers.d
echo 'rpi ALL=(ALL) NOPASSWD: ALL' > %{buildroot}/etc/sudoers.d/rpi-no-passwd
touch %{buildroot}/var/db/sudo/lectured/.hidden
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%attr(4755,root,root) /usr/bin/sudo
%attr(660,root,root) %config(noreplace) /etc/sudoers
%config(noreplace) /etc/sudo.conf
%config(noreplace) /etc/pam.d/sudo
%config(noreplace) /etc/sudo_logsrvd.conf
%attr(440,root,root) /etc/sudoers.d/rpi-no-passwd
%license LICENSE.md
%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 1.9.12p2-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.9.12p1-1
-	Initial build.	First version
