Summary: Public Key Infrastructure is a method to validate the authenticity of an otherwise unknown entity across untrusted networks.
Name: make-ca
Version: 1.12
Release: 1
License: GPL3
Group: Core
URL: https://github.com/djlucas/make-ca
#	Source
Source0: http://www.example.org/Packages/make-ca/make-ca-1.12.tar.xz
Source1: http://www.example.org/Packages/make-ca/root.crt
Source2: http://www.example.org/Packages/make-ca/class3.crt
Source3: http://www.example.org/Packages/make-ca/certdata.txt
%description
Public Key Infrastructure  is a method to validate the authenticity of an otherwise unknown entity
across untrusted networks. PKI works by establishing a chain of trust, rather than trusting each individual host
or entity explicitly. In order for a certificate presented by a remote entity to be trusted, that certificate must present
a complete chain of certificates that can be validated using the root certificate of a Certificate Authority
that is trusted by the local machine

%prep
%setup -q -n %{name}-%{version}
sed 's:/usr/sbin:/usr/bin:' -i systemd/update-pki.service

%build
true

%install
%{make_install} SBINDIR=/usr/bin
install -vdm755  %{buildroot}/etc/ssl/local
#	Additional CA Certificates
openssl x509 -in %{_sourcedir}/root.crt -text -fingerprint -setalias "CAcert Class 1 root" \
	-addtrust serverAuth -addtrust emailProtection -addtrust codeSigning \
	> %{buildroot}/etc/ssl/local/CAcert_Class_1_root.pem
openssl x509 -in %{_sourcedir}/class3.crt -text -fingerprint -setalias "CAcert Class 3 root" \
	-addtrust serverAuth -addtrust emailProtection -addtrust codeSigning \
	> %{buildroot}/etc/ssl/local/CAcert_Class_3_root.pem
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE
%license LICENSE.GPLv3

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.12-1
-	Initial build.	First version
