Summary: Raspberry Pi 4 kernel
Name: linux
Version: 1.20230106
Release: 2
License: GPLv2
Group: Core
URL: https://github.com/raspberrypi/linux
#	Source
Source0: http://www.example.org/Packages/linux/1.20230106.tar.gz
%description
Raspberry Pi 4 kernel

%prep
%setup -q -n linux-%{version}

%build
make mrproper
#       Raspberry Pi 4
#	export KCFLAGS="-march=armv7-a+crc+simd -mtune=cortex-a53"
make bcm2711_defconfig
#	%{make_build} zImage modules dtbs
%{make_build}

%install
#	install kernel modules
make INSTALL_MOD_PATH=%{buildroot}/usr modules_install
rm -vf %{buildroot}/usr/lib/modules/*/build
rm -vf %{buildroot}/usr/lib/modules/*/source
#	broadcomm firmware overlays
install -vdm 755 %{buildroot}/boot/overlays
cp arch/arm/boot/dts/overlays/*.dtb* %{buildroot}/boot/overlays/
cp arch/arm/boot/dts/overlays/README %{buildroot}/boot/overlays/
#	broadcomm firmware
cp arch/arm/boot/dts/*.dtb %{buildroot}/boot/
unlink %{buildroot}/boot/bcm2835-rpi-a-plus.dtb
unlink %{buildroot}/boot/bcm2835-rpi-a.dtb
unlink %{buildroot}/boot/bcm2835-rpi-b-plus.dtb
unlink %{buildroot}/boot/bcm2835-rpi-b-rev2.dtb
unlink %{buildroot}/boot/bcm2835-rpi-b.dtb
unlink %{buildroot}/boot/bcm2835-rpi-cm1-io1.dtb
unlink %{buildroot}/boot/bcm2835-rpi-zero-w.dtb
unlink %{buildroot}/boot/bcm2835-rpi-zero.dtb
unlink %{buildroot}/boot/bcm2836-rpi-2-b.dtb
unlink %{buildroot}/boot/bcm2837-rpi-3-a-plus.dtb
unlink %{buildroot}/boot/bcm2837-rpi-3-b-plus.dtb
unlink %{buildroot}/boot/bcm2837-rpi-3-b.dtb
unlink %{buildroot}/boot/bcm2837-rpi-cm3-io3.dtb
#	Kernel
cp arch/arm/boot/zImage %{buildroot}/boot/kernel7l.img
cp -v System.map %{buildroot}/boot/System.map-%{version}
cp -v .config %{buildroot}/boot/config-%{version}
#	Documentation
#	install -vdm 755 %{buildroot}/usr/share/doc/%{name}-%{version}
#	cp -r Documentation/* %{buildroot}/usr/share/doc/%{name}-%{version}
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Jan 23 2023 scott andrews <scott-andrews@columbus.rr.com> 1.20230106-1
-	Major update
