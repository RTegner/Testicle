Summary: A tool for automating interactive applications
Name: expect
Version: 5.45.4
Release: 1
License: Custom
Group: Core
URL: https://www.nist.gov/el/msid/expect.cfm
#	Source
Source0: http://www.example.org/Packages/expect/expect5.45.4.tar.gz
Source1: http://www.example.org/Packages/expect/expect-5.45-format-security.patch
%description
A tool for automating interactive applications

%prep
%setup -q -n %{name}%{version}
patch -Np0 -i %{_sourcedir}/expect-5.45-format-security.patch

%build
_options=(--prefix=/usr
	--mandir=/usr/share/man
	--build=${MACHTYPE}
	--host=${MACHTYPE}
	--target=${MACHTYPE})
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license README

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 5.45.4-1
-	Initial build.	First version
