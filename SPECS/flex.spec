Summary: A tool for generating text-scanning programs
Name: flex
Version: 2.6.4
Release: 1
License: BSD
Group: Core
URL: https://github.com/westes/flex
#	Source
Source0: http://www.example.org/Packages/flex/flex-2.6.4.tar.gz
Source1: http://www.example.org/Packages/flex/flex-pie.patch
%description
A tool for generating text-scanning programs

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
ln -sv flex %{buildroot}/usr/bin/lex
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.6.4-1
-	Initial build.	First version
