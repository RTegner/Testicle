Summary: Setuptools extension for CalVer package versions
Name: python-calver
Version: 2022.06.26
Release: 1
License: Apache
Group: Python
URL: https://github.com/di/calver
#	Source
Source0: http://www.example.org/Packages/python-calver/calver-2022.6.26.tar.gz
Requires: python
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
#	depends=('python')
#	makedepends=('python-setuptools')
#	checkdepends=('python-pytest' 'python-pretend')
%description
Setuptools extension for CalVer package versions

%prep
%setup -q -n calver-2022.6.26
echo "Version: %{version}" > PKG-INFO

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 2022.06.26-1
-	Initial build.	First version
