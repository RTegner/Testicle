Summary: Multi-format archive and compression library
Name: libarchive
Version: 3.6.1
Release: 1
License: BSD
Group: Core
URL: https://libarchive.org/
#	Source
Source0: http://www.example.org/Packages/libarchive/libarchive-3.6.1.tar.xz
Source1: http://www.example.org/Packages/libarchive/0001-libarchive-Handle-a-calloc-returning-NULL-fixes-1754.patch
Source2: http://www.example.org/Packages/libarchive/0002-Validate-entry_bytes_remaining-in-pax_attribute.patch
Source3: http://www.example.org/Packages/libarchive/0003-libarchive-Do-not-include-sys-mount.h-when-linux-fs..patch
%description
Multi-format archive and compression library

%prep
%setup -q -n %{name}-%{version}
patch -Np1 < %{SOURCE1}
patch -Np1 < %{SOURCE2}
patch -Np1 < %{SOURCE3}

%build
_options=(--prefix=/usr
	--disable-static
	--without-xml2
	--without-nettle)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.6.1-1
-	Initial build.	First version
