Summary: Command line editor library providing generic line editing, history, and tokenization functions
Name: libedit
Version: 20210910_3.1
Release: 1
License: BSD
Group: Base
URL: https://thrysoee.dk/editline
#	Source
Source0: http://www.example.org/Packages/libedit/libedit-20221009-3.1.tar.gz
#	Build Requirements
BuildRequires: glibc
BuildRequires: ncurses
BuildRequires: pkg-config
%description
Command line editor library providing generic line editing, history, and tokenization functions

%prep
%setup -q -n libedit-20221009-3.1

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
make V=1

%install
%{make_install}
rm %{buildroot}/usr/share/man/man3/history.3 # conflicts with readline
install -Dm 644 %{buildroot}/usr/share/man/man3/editline.3 %{buildroot}/usr/share/man/man3/el.3
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 20210910_3.1-1
-	Initial build.	First version
