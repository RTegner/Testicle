Summary: A lil TOML parser
Name: python-tomli-w
Version: 1.0.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/hukkin/tomli-w
#	Source
Source0: http://www.example.org/Packages/python-tomli-w/1.0.0.tar.gz
#	Requirements
Requires: python
#	Build Requirements
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-flit
#	depends=(python)
#	makedepends=( python-build python-flit-core python-installer)
#	checkdepends=( python-pytest python-tomli)
%description
A lil TOML parser

%prep
%setup -q -n tomli-w-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/tomli_w-%{version}.dist-info
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 1.0.0-1
-	Initial build.	First version
