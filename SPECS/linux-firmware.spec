Summary: Firmware files for Linux
Name: linux-firmware
Version: 20230117
Release: 1
License: Other
Group: Core
URL: git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git
#	Source
Source0: http://www.example.org/Packages/linux-firmware/linux-firmware-20230117.tar.gz
%description
Firmware files for Linux

%prep
%setup -q -n %{name}-%{version}

%build
true

%install
make DESTDIR=%{buildroot} FIRMWAREDIR=/usr/lib/firmware install
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
"/usr/lib/firmware/brcm/brcmfmac43241b4-sdio.Intel Corp.-VALLEYVIEW C0 PLATFORM.txt"
"/usr/lib/firmware/brcm/brcmfmac43340-sdio.ASUSTeK COMPUTER INC.-TF103CE.txt"
"/usr/lib/firmware/brcm/brcmfmac43430a0-sdio.ONDA-V80 PLUS.txt"
"/usr/lib/firmware/brcm/brcmfmac43455-sdio.MINIX-NEO Z83-4.txt"
"/usr/lib/firmware/brcm/brcmfmac43455-sdio.Raspberry Pi Foundation-Raspberry Pi 4 Model B.txt"
"/usr/lib/firmware/brcm/brcmfmac43455-sdio.Raspberry Pi Foundation-Raspberry Pi Compute Module 4.txt"
"/usr/lib/firmware/brcm/brcmfmac4356-pcie.Xiaomi Inc-Mipad2.txt"

%changelog
*	Fri Jan 20 2023 scott andrews <scott-andrews@columbus.rr.com> 20230117-1
-	Initial build.	First version
