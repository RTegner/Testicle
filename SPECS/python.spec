%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%define python_ver 3.11
Summary: Next generation of the python high-level scripting language
Name: python
Version: 3.11.1
Release: 1
License: Other
Group: Development/Languages
URL: https://www.python.org
#	Source
Source0: http://www.example.org/Packages/python/Python-3.11.1.tar.xz
#	Requirements
Provides: /usr/bin/python3
BuildRequires: sqlite
BuildRequires: mpdecimal
%description
Next generation of the python high-level scripting language

%prep
%setup -q -n Python-%{version}
# 	FS#23997
sed -i -e "s|^#.* /usr/local/bin/python|#!/usr/bin/python|" Lib/cgi.py
#	Ensure that we are using the system copy of various libraries (expat, libffi, and libmpdec),
#	rather than copies shipped in the tarball
rm -r Modules/expat
rm -r Modules/_ctypes/{darwin,libffi}*
rm -r Modules/_decimal/libmpdec

%build
_options=(--prefix=/usr
	--enable-shared
	--with-computed-gotos
	--enable-optimizations
	--with-lto
	--enable-ipv6
	--with-system-expat
	--with-dbmliborder=gdbm:ndbm
	--with-system-ffi
	--with-system-libmpdec
	--enable-loadable-sqlite-extensions
	--without-ensurepip
#	--with-ensurepip
	--with-tzpath=/usr/share/zoneinfo)
./configure  "${_options[@]}"
%{make_build}

%install
%{make_install}
# Why are these not done by default...
/usr/bin/ln -s python3 %{buildroot}/usr/bin/python
/usr/bin/ln -s python3-config %{buildroot}/usr/bin/python-config
/usr/bin/ln -s idle3	%{buildroot}/usr/bin/idle
/usr/bin/ln -s pydoc3 %{buildroot}/usr/bin/pydoc
/usr/bin/ln -s python3.1 %{buildroot}%{_mandir}/man1/python.1
#	install -dm755 %{buildroot}/usr/share/doc/%{name}
#	/usr/bin/tar --strip-components=1 --no-same-owner --directory %{buildroot}/usr/share/doc/%{name} -xf  %{SOURCE1}
#	some useful "stuff" FS#46146
install -dm755 %{buildroot}/usr/lib/python%{python_ver}/Tools/{i18n,scripts}
install -m755 Tools/i18n/{msgfmt,pygettext}.py %{buildroot}/usr/lib/python%{python_ver}/Tools/i18n/
install -m755 Tools/scripts/{README,*py} %{buildroot}/usr/lib/python%{python_ver}/Tools/scripts/

  %strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
#	"/usr/lib/python%{python_ver}/site-packages/setuptools/command/launcher manifest.xml"
#	"/usr/lib/python%{python_ver}/site-packages/setuptools/script (dev).tmpl"

%license LICENSE

%changelog
*	Thu Jan 26 2023 scott andrews <scott-andrews@columbus.rr.com> 3.11.1-1
-	Update version
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 3.10.9-1
-	Initial build.	First version
