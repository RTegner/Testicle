Summary: A complete, highly portable implementation of the DNS protocol
Name: bind
Version: 9.18.12
Release: 1
License: MPL2
Group: Server
URL: https://www.isc.org/software/bind/
#	Source
Source0: http://www.example.org/Packages/bind/bind-9.18.12.tar.xz
Source1: http://www.example.org/Packages/bind/tmpfiles.conf
Source2: http://www.example.org/Packages/bind/sysusers.conf
Source3: http://www.example.org/Packages/bind/named.service
Source4: http://www.example.org/Packages/bind/bind.keys.v9_11
Source5: http://www.example.org/Packages/bind/mk-zone-file.sh
#	Build Requirements
BuildRequires: glibc
BuildRequires: jemalloc
BuildRequires: libcap
BuildRequires: libuv
BuildRequires: openssl
BuildRequires: zlib
%description
A complete, highly portable implementation of the DNS protocol

%prep
%setup -q -n %{name}-%{version}
###	(only if building from the git repository)
#	autoreconf -fiv

%build
_options=(--prefix=/usr
#	--sysconfdir=/etc
	--sysconfdir=/var/named
	--sbindir=/usr/bin
	--localstatedir=/var
	--enable-full-report
	--disable-doh
#	--disable-fixed-rrset
#	--disable-geoip
#	--disable-static
#	--disable-chroot
#	--with-libxml2=no
#	--with-json-c=no
#	--with-libxml2=no
#	--with-lmdb=no
#	--with-maxminddb=no
#	--with-libnghttp2=no
#	--with-readline=no
#	--with-libidn2=no
	--with-openssl)
#	--with-tuning=small)
#	--enable-fixed-rrset
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
install -D -m644 %{SOURCE1} %{buildroot}/usr/lib/tmpfiles.d/%{name}.conf
install -D -m644 %{SOURCE2} %{buildroot}/usr/lib/sysusers.d/%{name}.conf
install -D -m644 %{SOURCE3} %{buildroot}/usr/lib/systemd/system/named.service
#	mk-zone-file.sh and keys
install -d -m770 %{buildroot}/var/named
install -m640 %{SOURCE4} %{buildroot}/var/named/bind.keys
install -m640 %{SOURCE5} %{buildroot}/var/named
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) %attr(-,root,named) /var/named/bind.keys
%dir %attr(770,root,named) /var/named
%license LICENSE
%license COPYRIGHT

%changelog
*	Fri Feb 24 2023 scott andrews <scott-andrews@columbus.rr.com> 9.18.12-1
-	Update
*	Sun Jan 01 2023 scott andrews <scott-andrews@columbus.rr.com> 9.18.10-1
-	Initial build.	First version
