Summary: An URL retrieval utility and library
Name: curl
Version: 7.87.0
Release: 1
License: MIT
Group: Core
URL: https://curl.haxx.se
#	Source
Source0: http://www.example.org/Packages/curl/curl-7.87.0.tar.xz
Source1: http://www.example.org/Packages/curl/0001-typecheck_accept_expressions_for_option_info_parameters.patch
#	depends=('ca-certificates' 'brotli' 'libbrotlidec.so' 'krb5' 'libgssapi_krb5.so'
#		'libidn2' 'libidn2.so' 'libnghttp2' 'libpsl' 'libpsl.so' 'libssh2' 'libssh2.so'
#		'openssl' 'zlib' 'zstd' 'libzstd.so')
#makedepends=('patchelf')
%description
An URL retrieval utility and library

%prep
%setup -q -n %{name}-%{version}
patch -Np1 < %{SOURCE1}

%build
_options=(--prefix=/usr
	--mandir=/usr/share/man
	--disable-ldap
	--disable-ldaps
	--disable-manual
	--enable-ipv6
	--enable-threaded-resolver
	#	--with-gssapi
	#	--with-libssh2
	--with-openssl
	--with-random=/dev/urandom
	--with-ca-bundle=/etc/ssl/certs/ca-certificates.crt)
./configure "${_options[@]}"
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
%{make_build}

%install
%{make_install}
rm -rf docs/examples/.deps
find docs \( -name Makefile\* -o -name \*.1 -o -name \*.3 \) -exec rm {} \;
install -v -d -m755 %{buildroot}/usr/share/doc/%{name}
cp -v -R docs/* %{buildroot}/usr/share/doc/%{name}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 7.86.0-1
-	Initial build.	First version
