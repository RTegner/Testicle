Summary: Keytable files and keyboard utilities
Name: kbd
Version: 2.5.1
Release: 1
License: GPL
Group: Core
URL: http://ftp.altlinux.org/pub/people/legion/kbd
#	Source
Source0: http://www.example.org/Packages/kbd/kbd-2.5.1.tar.xz
Source1: http://www.example.org/Packages/kbd/fix-euro2.patch
#	Source2: http://www.example.org/Packages/kbd/vlock.pam
%description
Keytable files and keyboard utilities

%prep
%setup -q -n %{name}-%{version}
#	rename keymap files with the same names
#	this is needed because when only name of keymap is specified
#	loadkeys loads the first keymap it can find, which is bad (see FS#13837)
#	this should be removed when upstream adopts the change
mv data/keymaps/i386/qwertz/cz{,-qwertz}.map
mv data/keymaps/i386/olpc/es{,-olpc}.map
mv data/keymaps/i386/olpc/pt{,-olpc}.map
mv data/keymaps/i386/fgGIod/trf{,-fgGIod}.map
mv data/keymaps/i386/colemak/{en-latin9,colemak}.map
patch -Np1 -i %{SOURCE1}
autoreconf -if

%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--datadir=/usr/share/kbd
	--mandir=/usr/share/man
	--disable-vlock)
./configure "${_options[@]}"
%{make_build} KEYCODES_PROGS=yes RESIZECONS_PROGS=yes

%install
%{make_install} KEYCODES_PROGS=yes RESIZECONS_PROGS=yes
#	mkdir -vp %%{buildroot}/etc/pam.d
#	install -Dm644 %%{SOURCE2}/vlock.pam %%{buildroot}/etc/pam.d/vlock
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.5.1-1
-	Initial build.	First version
