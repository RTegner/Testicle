Summary: Hardware identification databases
Name: hwdata
Version: 0.365
Release: 1
License: GPL2
Group: Base
URL: https://github.com/vcrhonek/hwdata
#	Source
Source0: http://www.example.org/Packages/hwdata/hwdata-0.365.tar.gz
%description
Hardware identification databases
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
 
%install
%{make_install}
#	Do not package blacklist of kernel modules
rm -rf %{buildroot}/usr/lib/
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.365-1
-	Initial build.	First version
