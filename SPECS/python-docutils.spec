Summary: Set of tools for processing plaintext docs into formats such as HTML, XML, or LaTeX
Name: python-docutils
Version: 0.19
Release: 1
License: custom
Group: Python
URL: http://docutils.sourceforge.net
#	Source
Source0: http://www.example.org/Packages/python-docutils/docutils-0.19.tar.gz
#	Requirements
Requires: python
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
#	depends=('python')
#	makedepends=('python-build' 'python-installer' 'python-setuptools' 'python-wheel')

%description
Set of tools for processing plaintext docs into formats such as HTML, XML, or LaTeX

%prep
%setup -q -n docutils-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
# symlink without .py
for f in "%{buildroot}"/usr/bin/*.py; do
	ln -s "$(basename "$f")" "%{buildroot}/usr/bin/$(basename "$f" .py)"
done
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/docutils-%{version}.dist-info

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.19-1
-	Initial build.	First version
