Summary: Perl Module: Localization support
Name: perl-libintl
Version: 1.33
Release: 1
License: LGPL
Group: Core
URL: https://search.cpan.org/dist/libintl-perl
#	Source
Source0: http://www.example.org/Packages/perl-libintl/libintl-perl-1.33.tar.gz
%description
Perl Module: Localization support

%prep
%setup -q -n libintl-perl-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
make

%install
PERL_USE_UNSAFE_INC=1 make install DESTDIR=%{buildroot}
find %{buildroot} -name perllocal.pod -delete
find %{buildroot} -name .packlist -delete
rmdir %{buildroot}/usr/lib/perl5/5.36/core_perl
rmdir %{buildroot}/usr/lib/perl5/5.36/vendor_perl/auto/libintl-perl
#	%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 1.33-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.32-1
-	Initial build.	First version
