Summary: A utility to apply patch files to original sources
Name: patch
Version: 2.7.6
Release: 1
License: GPLv3
Group: Core
URL: https://www.gnu.org/software/patch
#	Source
Source0: http://www.example.org/Packages/patch/patch-2.7.6.tar.xz
Source1: http://www.example.org/Packages/patch/f290f48a621867084884bfff87f8093c15195e6a.patch
Source2: http://www.example.org/Packages/patch/b5a91a01e5d0897facdd0f49d64b76b0f02b43e1.patch
Source3: http://www.example.org/Packages/patch/123eaff0d5d1aebe128295959435b9ca5909c26d.patch
Source4: http://www.example.org/Packages/patch/3fcd042d26d70856e826a42b5f93dc4854d80bf0.patch
Source5: http://www.example.org/Packages/patch/19599883ffb6a450d2884f081f8ecf68edbed7ee.patch
Source6: http://www.example.org/Packages/patch/369dcccdfa6336e5a873d6d63705cfbe04c55727.patch
Source7: http://www.example.org/Packages/patch/9c986353e420ead6e706262bf204d6e03322c300.patch
%description
A utility to apply patch files to original sources

%prep
%setup -q -n %{name}-%{version}
#	CVE-2018-6951
patch -Np1 < %{SOURCE1}
patch -Np1 < %{SOURCE2}
#	CVE-2018-1000156
patch -Np1 < %{SOURCE3}
patch -Np1 < %{SOURCE4}
patch -Np1 < %{SOURCE5}
#	Fix memory leaks introduced in CVE-2018-1000165
patch -Np1 < %{SOURCE6}
#	CVE-2018-6952
patch -Np1 < %{SOURCE7}
autoreconf -fiv

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.7.6-1
-	Initial build.	First version
