Summary: Modern library for one-way hashing of passwords
Name: libxcrypt
Version: 4.4.33
Release: 1
License: GPL
Group: Utility
URL: https://github.com/besser82/libxcrypt
#	Source
Source0: http://www.example.org/Packages/libxcrypt/libxcrypt-4.4.33.tar.xz
%description
Modern library for one-way hashing of passwords
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
	--disable-static
	--enable-hashes=strong,glibc
	--enable-obsolete-api=no
	--disable-failure-tokens)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.4.33-1
-	Initial build.	First version
