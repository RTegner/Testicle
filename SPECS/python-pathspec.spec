Summary: Utility library for gitignore style pattern matching of file paths
Name: python-pathspec
Version: 0.11.0
Release: 1
License: MPL2
Group: Python Module
URL: https://github.com/cpburnz/python-pathspec
#	Source
Source0: http://www.example.org/Packages/python-pathspec/pathspec-0.11.0.tar.gz
%description
Utility library for gitignore style pattern matching of file paths

%prep
%setup -q -n pathspec-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Thu Feb 16 2023 scott andrews <scott-andrews@columbus.rr.com> -1
-	Initial build.	First version
