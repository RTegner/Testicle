Summary: Library for the arithmetic of complex numbers with arbitrarily high precision
Name: libmpc
Version: 1.3.1
Release: 1
License: LGPLv3
Group: Core
URL: http://www.multiprecision.org
#	Source
Source0: http://www.example.org/Packages/mpc/mpc-1.3.1.tar.gz
%description
Library for the arithmetic of complex numbers with arbitrarily high precision

%prep
%setup -q -n mpc-%{version}

%build
_options=( --prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.2.1-1
-	Initial build.	First version
