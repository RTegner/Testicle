Summary: GNU documentation system for on-line information and printed output
Name: texinfo
Version: 7.0.2
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/texinfo
#	Source
Source0: http://www.example.org/Packages/texinfo/texinfo-7.0.2.tar.gz
%description
GNU documentation system for on-line information and printed output

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
#	make TEXMF=%{buildroot}/usr/share/texmf install-tex
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 7.0.2-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 7.0.1-1
-	Initial build.	First version
