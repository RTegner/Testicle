Summary: A set of programs to assemble and manipulate binary and object files
Name: binutils
Version: 2.39
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/binutils
#	Source
Source0: http://www.example.org/Packages/binutils/binutils-2.39.tar.xz
Source1: http://www.example.org/Packages/binutils/gold-warn-unsupported.patch
#	Source2: http://www.example.org/Packages/binutils/fix-incorrect-undefined-symbol.patch
%description
A set of programs to assemble and manipulate binary and object files

%prep
%setup -q -n %{name}-%{version}
#	Turn off development mode (-Werror, gas run-time checks, date in sonames)
#	sed -i '/^development=/s/true/false/' bfd/development.sh
patch -Np1 -i %{SOURCE1}

%build
mkdir -v build;cd build
_options=(    --prefix=/usr
	--sysconfdir="%{buildroot}"/etc
	--with-lib-path=/usr/lib:/usr/local/lib
#	--with-bugurl=https://github.com/archlinuxarm/PKGBUILDs/issues
	--enable-default-execstack=no
	--enable-deterministic-archives
	--enable-gold
	--enable-install-libiberty
#	--enable-jansson
	--enable-ld=default
	--enable-new-dtags
	--enable-plugins
	--enable-relro
	--enable-shared
#	--enable-targets=bpf-unknown-none
	--enable-threads
	--disable-gdb
	--disable-gdbserver
	--disable-libdecnumber
	--disable-readline
	--disable-sim
	--disable-werror
#	--with-debuginfod
	--with-pic
	--with-system-zlib
	%{CONFIG_ARM}
)
../configure "${_options[@]}"
%{make_build} tooldir=/usr

%install
cd build;%{make_install} tooldir=/usr
#	install PIC version of libiberty
install -m644 libiberty/pic/libiberty.a %{buildroot}/usr/lib
#	Remove unwanted files
rm -f %{buildroot}/usr/share/man/man1/{dlltool,windres,windmc}*
#	No shared linking to these files outside binutils
rm -f %{buildroot}/usr/lib/lib{bfd,opcodes}.so
echo 'INPUT( /usr/lib/libbfd.a -liberty -lz -ldl )' > %{buildroot}/usr/lib/libbfd.so
echo 'INPUT( /usr/lib/libopcodes.a -lbfd )' > %{buildroot}/usr/lib/libopcodes.so
rm -fv %{buildroot}/usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes}.a
#	rm -r %{buildroot}/usr/lib64
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Tue Jan 17 2023 scott andrews <scott-andrews@columbus.rr.com> 2.40-1
-	Initial build.	First version
