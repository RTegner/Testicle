Summary: Perl XML parser library
Name: expat
Version: 2.5.0
Release: 1
License: Other
Group: Core
URL: https://libexpat.github.io
#	Source
Source0: http://www.example.org/Packages/expat/expat-2.5.0.tar.gz
#	Build Requirements
BuildRequires: cmake
%description
Perl XML parser library
 
%prep
%setup -q -n %{name}-%{version}
 
%build
cd %{_builddir}
_options=("-DCMAKE_INSTALL_PREFIX=/usr"
	"-DCMAKE_BUILD_TYPE=None"
	"-DCMAKE_INSTALL_LIBDIR=/usr/lib"
	"-W no-dev"
	"-B build"
	"-S %{name}-%{version}")
cmake "${_options[@]}"
%{make_build}-C build
 
%install
cd %{_builddir}
make VERBOSE=1 DESTDIR=%{buildroot} -C build install
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.5.0-1
-	Initial build.	First version
