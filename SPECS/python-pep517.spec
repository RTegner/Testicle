Summary: Wrappers to build Python packages using PEP 517 hooks
Name: python-pep517
Version: 0.13.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/takluyver/pep517
#	Source
Source0: http://www.example.org/Packages/python-pep517/pep517-0.13.0.tar.gz
Requires: python
Requires: python-tomli
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-toml
BuildRequires: python-tomli
BuildRequires: python-flit
#	depends=('python-tomli')1
#	makedepends=('python-build' 'python-flit-core' 'python-installer')
#	checkdepends=('python-mock' 'python-pytest' 'python-testpath' 'python-pip')

%description
Wrappers to build Python packages using PEP 517 hooks

%prep
%setup -q -n pep517-%{version}
#	Copied from openSUSE:
#	Remove what appears to be overly cautious flag
#	that causes tests to require internet, both here
#	and the test suites of any dependencies. Tracking at:
#	https://github.com/pypa/pep517/issues/101
sed "s/ '--ignore-installed',//" -i pep517/envbuild.py
sed '/--flake8/d' -i pytest.ini

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/pep517-%{version}.dist-info
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.13.0-1
-	Initial build.	First version
