Summary: Validation library and CLI tool for checking on pyproject.toml files using JSON Schema
Name: python-validate-pyproject
Version: 0.11
Release: 1
License: MPL
Group: Python
URL: https://github.com/abravalheri/validate-pyprojec
#	Source
Source0: http://www.example.org/Packages/python-validate-pyproject/v0.11.tar.gz

#	Requirements
#Requires: python
#Requires: python-packaging
#Requires: python-trove-classifiers
#Requires: python-tomli
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-setuptools-scm
#	depends=('python-packaging' 'python-trove-classifiers' 'python-tomli')
#	makedepends=('python-build' 'python-installer' 'python-wheel' 'python-setuptools-scm')
#	checkdepends=('python-pytest')

%description
Validation library and CLI tool for checking on pyproject.toml files using JSON Schema

%prep
%setup -q -n validate-pyproject-%{version}
#	Upstream author only supports VCS builds
echo "recursive-include src *.template *.json LICENSE LICENSE.*" > MANIFEST.in
sed -i '/--cov/d' setup.cfg
#	Devendor fastjsonschema
#	Disabled: functional differences due to changes in vendored copy
#	rm -r src/validate_pyproject/_vendor
#	sed -e 's/from validate_pyproject._vendor.fastjsonschema import/from fastjsonschema import/' #		-e 's/from validate_pyproject._vendor import fastjsonschema/import fastjsonschema/' #		-e 's/from .._vendor import fastjsonschema/import fastjsonschema/' #		-e 's/from ._vendor.fastjsonschema import/from fastjsonschema import/' #		-e 's/from ._vendor import fastjsonschema/import fastjsonschema/' #		-i src/validate_pyproject/*.py tests/*.py src/validate_pyproject/pre_compile/*.template src/validate_pyproject/pre_compile/*.py


%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 0.11-1
-	Update version
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.10.1-1
-	Initial build.	First version
