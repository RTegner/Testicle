Summary: The GNU general-purpose parser generator
Name: bison
Version: 3.8.2
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/bison
#	Source
Source0: http://www.example.org/Packages/bison/bison-3.8.2.tar.xz
%description
he GNU general-purpose parser generator
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr --datadir=/usr/share)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.8.2-1
-	Initial build.	First version
