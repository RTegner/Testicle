Summary: Framework for testing other programs
Name: dejagnu
Version: 1.6.3
Release: 1
License: GPL
Group: Core
URL: https://www.nist.gov/el/msid/expect.cfm
#	Source
Source0: http://www.example.org/Packages/dejagnu/dejagnu-1.6.3.tar.gz
%description
Framework for testing other programs
 
%prep
%setup -q -n %{name}-%{version}
 
%build
mkdir -v build;cd build
_options=(--prefix=/usr)
../configure "${_options[@]}"
 
%install
cd build;%{make_install};cd -
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.6.3-1
-	Initial build.	First version
