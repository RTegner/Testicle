Summary: The systemd package contains programs for controlling the startup, running, and shutdown of the system.
Name: systemd
Version: 252.2
Release: 1
License: GPL2 LGPL2.1
Group: Core
URL: https://www.github.com/systemd/systemd
#	Source
Source0: http://www.example.org/Packages/systemd-stable/v252.2.tar.gz
Source1: http://www.example.org/Packages/systemd-stable/0001-Use-Arch-Linux-device-access-groups.patch
Source2: http://www.example.org/Packages/systemd-stable/systemd-user.pam
Source3: http://www.example.org/Packages/systemd-stable/tmpfiles
%description
The systemd package contains programs for controlling the startup, running, and shutdown of the system.

%prep
%setup -q -n %{name}-stable-%{version}
# Replace cdrom/dialout/tape groups with optical/uucp/storage
patch -Np1 -i %{SOURCE1}

%build
_timeservers=( time.nist.gov
	0.pool.ntp.org
	1.pool.ntp.org)
_nameservers=(192.168.1.1)
_meson_options=(-Dmode=release
#	-Dgnu-efi=true
	-Dima=false
#	-Dlibidn2=true
#	-Dlz4=true
#	-Dman=true
#	We disable DNSSEC by default, it still causes trouble:
#	https://github.com/systemd/systemd/issues/10579
	-Ddbuspolicydir=/usr/share/dbus-1/system.d
	-Ddefault-dnssec=no
	-Ddefault-hierarchy=unified
	-Ddefault-kill-user-processes=false
	-Ddefault-locale=C
	-Dlocalegen-path=/usr/bin/locale-gen
	-Ddns-over-tls=openssl
	-Dfallback-hostname=rpi
	-Dnologin-path=/usr/bin/nologin
	-Dntp-servers="${_timeservers[*]}"
	-Ddns-servers="${_nameservers[*]}"
#	-Drpmmacrosdir=no
	-Dsysvinit-path=
	-Dsysvrcnd-path=
	-Duserdb=false
	-Dfirstboot=false
	-Dinstall-tests=false
	-Dman=false
	-Dpamconfdir=no)
cd %{_builddir}
arch-meson "%{_builddir}/%{name}-stable-%{version}" build "${_meson_options[@]}"
meson compile -C build

%install
cd %{_builddir}
DESTDIR=%{buildroot} meson install -C build
#	avoid a potential conflict with [core]/filesystem
rm %{buildroot}/usr/share/factory/etc/{issue,nsswitch.conf}
sed -e '/^C \/etc\/nsswitch\.conf/d' -e '/^C \/etc\/issue/d' -i %{buildroot}/usr/lib/tmpfiles.d/etc.conf
#	ship default policy to leave services disabled
#	echo \'disable *\' > %%{buildroot}/usr/lib/systemd/system-preset/99-default.preset
#
#	The group systemd-journal is allocated dynamically and may have varying
#	gid on different systems. Lets install with gid 0 (root), systemd-tmpfiles
# 	will fix the permissions for us. (see /usr/lib/tmpfiles.d/systemd.conf)
#	install -d -o root -g root -m 2755 %%{buildroot}/var/log/journal

#	match directory owner/group and mode from [extra]/polkit
#	install -d -o root -g 102 -m 0750 %%{buildroot}/usr/share/polkit-1/rules.d

#	add example bootctl configuration
#	install -D -m0644 arch.conf %%{buildroot}/usr/share/systemd/bootctl/arch.conf
#	install -D -m0644 loader.conf %%{buildroot}/usr/share/systemd/bootctl/loader.conf
#	install -D -m0644 splash-arch.bmp %%{buildroot}/usr/share/systemd/bootctl/splash-arch.bmp
#	overwrite the systemd-user PAM configuration with our own
#	install -D -m0644 %{SOURCE2} %%{buildroot}/etc/pam.d/systemd-user

#	Fixes
#	sed 's/mymachines//' -i %%{buildroot}/usr/share/factory/etc/nsswitch.conf
#	sed 's/myhostname//' -i %%{buildroot}/usr/share/factory/etc/nsswitch.conf
printf "%s\n" "LANG=C.UTF-8" > %{buildroot}/etc/locale.conf
printf "%s\n" "\
passwd:    compat systemd
group:     compat [SUCCESS=merge] systemd
shadow:    compat systemd
gshadow:   files  systemd

hosts:     resolve [!UNAVAIL=return] files dns
networks:  files

protocols: db files
services:  db files
ethers:    db files
rpc:       db files

netgroup:  nis" > %{buildroot}/etc/nsswitch.conf

install -D -m644 %{SOURCE3} %{buildroot}/usr/lib/tmpfiles.d/%{name}.conf
#	Empty directories
rmdir %{buildroot}/var/log/journal
rmdir %{buildroot}/var/lib/systemd
rmdir %{buildroot}/etc/udev/rules.d
rmdir %{buildroot}/etc/udev/hwdb.d
rmdir %{buildroot}/etc/systemd/network
rmdir %{buildroot}/etc/systemd/system
rmdir %{buildroot}/etc/systemd/user
rmdir %{buildroot}/etc/sysctl.d
rmdir %{buildroot}/etc/kernel/install.d
rmdir %{buildroot}/etc/binfmt.d
rmdir %{buildroot}/etc/tmpfiles.d
rmdir %{buildroot}/etc/modules-load.d
rmdir %{buildroot}/usr/lib/systemd/system-shutdown
rmdir %{buildroot}/usr/lib/systemd/system-sleep
rmdir %{buildroot}/usr/lib/binfmt.d
rmdir %{buildroot}/usr/lib/modules-load.d
rmdir %{buildroot}/etc/kernel
rmdir %{buildroot}/var/log
rmdir %{buildroot}/var/lib
rmdir %{buildroot}/var
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
#	%%config(noreplace) /etc/pam.d/systemd-user
#	%%config(noreplace) /etc/systemd/homed.conf
#	%%config(noreplace) /etc/systemd/journal-remote.conf
%config(noreplace) /etc/systemd/coredump.conf
%config(noreplace) /etc/systemd/journald.conf
%config(noreplace) /etc/systemd/journal-upload.conf
%config(noreplace) /etc/systemd/logind.conf
%config(noreplace) /etc/systemd/networkd.conf
%config(noreplace) /etc/systemd/oomd.conf
%config(noreplace) /etc/systemd/pstore.conf
%config(noreplace) /etc/systemd/resolved.conf
%config(noreplace) /etc/systemd/sleep.conf
%config(noreplace) /etc/systemd/system.conf
%config(noreplace) /etc/systemd/timesyncd.conf
%config(noreplace) /etc/systemd/user.conf
%config(noreplace) /etc/udev/udev.conf

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 252.2-1
-	Initial build.	First version
