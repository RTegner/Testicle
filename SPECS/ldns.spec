Summary: Fast DNS library supporting recent RFCs
Name: ldns
Version: 1.8.3
Release: 1
License: BSD
Group: Networking
URL: https://www.nlnetlabs.nl/projects/ldns
#	Source
Source0: http://www.example.org/Packages/ldns/ldns-1.8.3.tar.gz
Source1: http://www.example.org/Packages/ldns/trusted-key-20190629.key
#	Build Requirements
BuildRequires: bash
BuildRequires: glibc
BuildRequires: libpcap
BuildRequires: openssl
BuildRequires: pkg-config
%description
Fast DNS library supporting recent RFCs
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--localstatedir=/var
	--disable-rpath
	--with-drill
	--with-examples
	--with-trust-anchor=/etc/trusted-key.key)
./configure "${_options[@]}"
%{make_build}
 
%install
mkdir -vp %{buildroot}/usr/lib/pkgconfig
%{make_install}
#	Install trust key
mkdir %{buildroot}/etc
install -Dm644 %{SOURCE1} %{buildroot}/etc/trusted-key.key
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.8.3-1
-	Initial build.	First version
