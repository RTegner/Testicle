Summary: Miscellaneous system utilities for Linux
Name: util-linux
Version: 2.38.1
Release: 1
License: GPLv2
Group: Core
URL: http://freecode.com/projects/util-linux
#	Source
Source0: http://www.example.org/Packages/util-linux/util-linux-2.38.1.tar.xz
Source1: http://www.example.org/Packages/util-linux/60-rfkill.rules
Source2: http://www.example.org/Packages/util-linux/rfkill-unblock_.service
Source3: http://www.example.org/Packages/util-linux/rfkill-block_.service
Source4: http://www.example.org/Packages/util-linux/util-linux.sysusers
Source5: http://www.example.org/Packages/util-linux/pam-login
Source6: http://www.example.org/Packages/util-linux/pam-common
Source7: http://www.example.org/Packages/util-linux/pam-runuser
Source8: http://www.example.org/Packages/util-linux/pam-su
%description
Miscellaneous system utilities for Linux

%prep
%setup -q -n util-linux-%{version}
./autogen.sh
sed '/chgrp tty/d' -i Makefile.in		# wall
sed '/chmod g+s/d' -i Makefile.in		# mount umount
sed '/chown root:root/d' -i Makefile.in		# mount umount


%build
_options=(--prefix=/usr
	--libdir=/usr/lib
	--bindir=/usr/bin
	--sbindir=/usr/bin
	--localstatedir=/var
	--enable-usrdir-path
	--enable-fs-paths-default=/usr/bin:/usr/local/bin
	--enable-vipw
	--enable-newgrp
	--enable-chfn-chsh
	--enable-write
	--enable-mesg
	--with-python=3
	--disable-chfn-chsh	#	- shadow-4.11.1-1.aarch64, requires pam
	--disable-login		#	- shadow-4.11.1-1.aarch64, requires pam
	--disable-su		#	- shadow-4.11.1-1.aarch64, requires pam
	--disable-rpath
)

#	--enable-chfn-chsh	#	- shadow-4.11.1-1.aarch64, requires pam
#	--enable-login		#	- shadow-4.11.1-1.aarch64, requires pam
#	--enable-su		#	- shadow-4.11.1-1.aarch64, requires pam
#	--disable-chfn-chsh	#	- shadow-4.11.1-1.aarch64, requires pam
#	--disable-login		#	- shadow-4.11.1-1.aarch64, requires pam
#	--disable-su		#	- shadow-4.11.1-1.aarch64, requires pam
#	--disable-setpriv	#	WARNING: libcap-ng library not found; not building setpriv
#	--disable-runuser	#	requires pam
#	--disable-pylibmount
#	--disable-static)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install} usrsbin_execdir=/usr/bin
###	install rfkill
install -Dm0644 %{SOURCE1} %{buildroot}/usr/lib/udev/rules.d/60-rfkill.rules
install -Dm0644 %{SOURCE2} %{buildroot}/usr/lib/systemd/system/rfkill-unblock@.service
install -Dm0644 %{SOURCE3} %{buildroot}/usr/lib/systemd/system/rfkill-block@.service
###	Users
install -Dm0644 %{SOURCE4} %{buildroot}/usr/lib/sysusers.d/util-linux.conf
#	Farts
unlink %{buildroot}/usr/share/man/man8/vipw.8
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%attr(2644,root,tty) /usr/bin/wall
%attr(4755,root,root) /usr/bin/mount
%attr(4755,root,root) /usr/bin/umount

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.38.1-1
-	Initial build.	First version
