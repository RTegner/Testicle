%define python_ver 3.11
Summary: Easily download, build, install, upgrade, and uninstall Python packages
Name: python-setuptools
Version: 67.3.2
Release: 1
License: PSF
Group: Core
URL: https://pypi.org/project/setuptools
#	Source
Source0: http://www.example.org/Packages/python-setuptools/setuptools-67.3.2.tar.gz
#	http://www.example.org/Packages/python-setuptools/setuptools-65.6.3.tar.gz
Source1: http://www.example.org/Packages/python-setuptools/system-validate-pyproject.patch
#	http://www.example.org/Packages/python-setuptools/system-validate-pyproject.patch
Source2: http://www.example.org/Packages/python-setuptools/add-dependency.patch
#	http://www.example.org/Packages/python-setuptools/add-dependency.patch
#Requires: python
#Requires: python-appdirs
#Requires: python-jaraco.text
#Requires: python-more-itertools
#Requires: python-ordered-set
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#
#	depends=('python-appdirs' 'python-jaraco.text' 'python-more-itertools' 'python-ordered-set'
#		'python-packaging' 'python-pyparsing' 'python-tomli' 'python-validate-pyproject')
#	makedepends=('git' 'python-setuptools')
#	checkdepends=('python-jaraco.envs' 'python-jaraco.path' 'python-pip' 'python-pip-run'
#		'python-pytest-fixture-config' 'python-pytest-virtualenv' 'python-wheel'
#		'python-pytest-enabler' 'python-pytest-mypy' 'python-pytest-timeout' 'python-sphinx'
#		'python-build' 'python-ini2toml' 'python-tomli-w')
#	provides=('python-distribute')

%description
python-setuptools is a collection of enhancements to the
Python distutils that allow you to more easily build and
distribute Python packages, especially ones that have
dependencies on other packages.

%prep
%setup -q -n setuptools-%{version}
patch -p1 -i %{SOURCE1}
rm -r {pkg_resources,setuptools}/{extern,_vendor} setuptools/config/_validate_pyproject
#	Upstream devendoring logic is badly broken, see:
#	https://bugs.archlinux.org/task/58670
#	https://github.com/pypa/pip/issues/5429
#	https://github.com/pypa/setuptools/issues/1383
#	The simplest fix is to simply rewrite import paths to use the canonical
#	location in the first place
for _module in setuptools pkg_resources '' ; do
	find . -name \*.py -exec sed -i -e 's/from '$_module.extern' import/import/' -e 's/from '$_module.extern'\./from /' -e 's/import '$_module.extern'\./import /' -e "s/__import__('$_module.extern./__import__('/" -e 's/from \.\.extern\./from /' {} +
done
#	Add the devendored dependencies into metadata of setuptools
patch -p0 -i %{SOURCE2}
#	Fix tests invoking python-build
sed -e 's/"-m", "build", "--wheel"/"-m", "build", "--wheel", "--no-isolation"/' -e 's/"-m", "build", "--sdist"/"-m", "build", "--sdist", "--no-isolation"/' -i setuptools/tests/fixtures.py
#	Remove post-release tag since we are using stable tags
sed -e '/tag_build = .post/d' -e '/tag_date = 1/d' -i setup.cfg
#	Fix shebang
sed -i -e "s|^#\!.*/usr/bin/env python|#!/usr/bin/env python3|" setuptools/command/easy_install.py


%build
export SETUPTOOLS_INSTALL_WINDOWS_SPECIFIC_FILES=0
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}
#	python -m build --wheel --no-isolation

%install
export SETUPTOOLS_INSTALL_WINDOWS_SPECIFIC_FILES=0
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python%{python_ver}/site-packages/setuptools-%{version}.dist-info
"/usr/lib/python%{python_ver}/site-packages/setuptools/script (dev).tmpl"

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 65.6.3-1
-	Initial build.	First version
