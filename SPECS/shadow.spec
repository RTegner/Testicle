Summary: Password and account management tool suite with support for shadow files
Name: shadow
Version: 4.13
Release: 1
License: BSD
Group: Core
URL: https://github.com/shadow-maint/shadow
#	Source
Source0: http://www.example.org/Packages/shadow/shadow-4.13.tar.xz
Source1: http://www.example.org/Packages/shadow/chgpasswd
Source2: http://www.example.org/Packages/shadow/chpasswd
Source3: http://www.example.org/Packages/shadow/defaults.pam
Source4: http://www.example.org/Packages/shadow/login.defs
Source5: http://www.example.org/Packages/shadow/newusers
Source6: http://www.example.org/Packages/shadow/passwd
Source7: http://www.example.org/Packages/shadow/shadow.timer
Source8: http://www.example.org/Packages/shadow/shadow.service
Source9: http://www.example.org/Packages/shadow/useradd.defaults
%description
Password and account management tool suite with support for shadow files

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--bindir=/usr/bin
	--sbindir=/usr/bin
	--libdir=/usr/lib
	--mandir=/usr/share/man
	--sysconfdir=/etc
	--disable-account-tools-setuid
	--with-group-name-max-length=32
	--without-selinux
	#	--with-libpam
	#	--with-audit
	#	--without-su
)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install} exec_prefix=/usr
/usr/bin/make DESTDIR=%{buildroot} -C man install
#
###	useradd defaults - remove when using util-linux version
install -Dm600 %{_sourcedir}/useradd.defaults %{buildroot}/etc/default/useradd
###	login.defs - remove when using util-linux version
install -Dm644 %{_sourcedir}/login.defs %{buildroot}/etc/login.defs
#
###	move everything else to /usr/bin, because this is not handled by ./configure
mv %{buildroot}/usr/sbin/* %{buildroot}/usr/bin
###	Remove evil/broken tools
rm %{buildroot}/usr/bin/logoutd
#	Remove utilities provided by util-linux
#	keep newgrp, as sg is really an alias to it
rm %{buildroot}/usr/bin/{nologin,sg,vigr,vipw}
#  %{buildroot}/usr/bin/{login,chsh,chfn}
mv %{buildroot}/usr/bin/{newgrp,sg}
#	...and their man pages
find %{buildroot}/usr/share/man -name logoutd.8 -delete
find %{buildroot}/usr/share/man -name vigr.8 -delete
find %{buildroot}/usr/share/man -name nologin.8 -delete
find %{buildroot}/usr/share/man -name newgrp.1 -delete
#	find %{buildroot}/usr/share/man -name chsh.1 -delete
#	find %{buildroot}/usr/share/man -name chfn.1 -delete
#	find %{buildroot}/usr/share/man -name su.1 -delete
#	find %{buildroot}/usr/share/man -name login.1 -delete
#	remove empty directories
rmdir %{buildroot}/usr/sbin
rm -r %{buildroot}/usr/share/man/hu/man5
rm -r %{buildroot}/usr/share/man/ko/man5
rm -r %{buildroot}/usr/share/man/zh_TW/man5
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /etc/default
%config (noreplace) /etc/login.access
%config (noreplace) /etc/login.defs
%config (noreplace) /etc/default/useradd

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 4.13-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.12.3-1
-	Initial build.	First version
