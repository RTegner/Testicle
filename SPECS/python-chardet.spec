Summary: Python3 module for character encoding auto-detection
Name: python-chardet
Version: 5.1.0
Release: 1
License: LGPL
Group: Python
URL: https://github.com/chardet/chardet
#	Source
Source0: http://www.example.org/Packages/python-chardet/python-chardet-5.1.0.tar.gz
Requires: python
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
#	depends=('python')
#	makedepends=('python-build' 'python-installer' 'python-setuptools' 'python-wheel')
#	checkdepends=('python-pytest')
%description
Python3 module for character encoding auto-detection

%prep
%setup -q -n chardet-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/chardet-%{version}.dist-info

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 5.1.0-1
-	Initial build.	First version
