Summary: GNU C Library
Name: glibc
Version: 2.36
Release: 1
License: GPL
Group: Core
URL: http://www.gnu.org/software/libc
#	Source
Source0: http://www.example.org/Packages/glibc/glibc-2.36.tar.xz
Source1: http://www.example.org/Packages/glibc/locale-gen
Source2: http://www.example.org/Packages/glibc/locale.gen.txt
Source3: http://www.example.org/Packages/glibc/sdt.h
Source4: http://www.example.org/Packages/glibc/sdt-config.h
Source5: http://www.example.org/Packages/glibc/reenable_DT_HASH.patch
Source6: http://www.example.org/Packages/glibc/tmpfiles
Source7: http://gremlin.example.org/Packages/glibc/glibc-nscd-time_t.patch
#Source7: http://gremlin.example.org/Packages/glibc/glibc-nscd.patch
Provides: rtld(GNU_HASH)
%description
GNU C Library

%prep
%setup -q -n %{name}-%{version}
cp %{SOURCE3} include/
cp %{SOURCE4} include/
sed '/MAKEFLAGS :=/s/)r/) -r/' -i Makerules
#sed '/installed-modules/s/nscd//' -i Makeconfig
#	reenable_DT_HASH.patch
patch -Np1 -i %{SOURCE5}
#	fix nscd to use 64 bit time_t
patch -Np1 -i %{SOURCE7}
mkdir build

%build
cd build
_options=(--prefix=/usr
	--with-headers=/usr/include
#	--with-bugurl=https://bugs.archlinux.org/
	--enable-bind-now
#	--enable-cet
#	--enable-kernel=4.4
	--enable-kernel=5.10
#	--enable-multi-arch
	--enable-stack-protector=strong
#	--enable-systemtap
	--disable-crypt
	--disable-profile
	--disable-werror
#	mine
	--sysconfdir=/etc
	--libdir=/usr/lib
	--libexecdir=/usr/lib
	--disable-multi-arch
#	--disable-timezone-tools
	--enable-memory-tagging)
echo "slibdir=/usr/lib" >> configparms
echo "rtlddir=/usr/lib" >> configparms
echo "sbindir=/usr/bin" >> configparms
echo "rootsbindir=/usr/bin" >> configparms
../configure "${_options[@]}"
#	make
%{make_build}
#	pregenerate C.UTF-8 locale until it is built into glibc
#	(https://sourceware.org/glibc/wiki/Proposals/C.UTF-8, FS#74864)-
elf/ld.so --library-path "$PWD" locale/localedef -c -f ../localedata/charmaps/UTF-8 -i ../localedata/locales/C ./C.UTF-8/

%install
cd build
make install_root=%{buildroot} install
rm -f %{buildroot}/etc/ld.so.cache
#	Shipped in tzdata
rm -f %{buildroot}/usr/bin/{tzselect,zdump,zic}

install -dm755 %{buildroot}/usr/lib/{locale,systemd/system,tmpfiles.d}
install -m644 ../nscd/nscd.conf %{buildroot}/etc/nscd.conf
install -m644 ../nscd/nscd.service %{buildroot}/usr/lib/systemd/system
install -m644 ../nscd/nscd.tmpfiles %{buildroot}/usr/lib/tmpfiles.d/nscd.conf
install -m644 ../posix/gai.conf %{buildroot}/etc/gai.conf

#	LOCALES
install -m755 %{SOURCE1} %{buildroot}/usr/bin
install -m644 %{SOURCE2} %{buildroot}/etc/locale-gen.conf
sed -e '1,3d' -e 's|/| |g' -e 's|\\| |g' -e 's|^|#|g' ../localedata/SUPPORTED >> %{buildroot}/etc/locale-gen.conf
sed 's/^#en_US/en_US/' -i %{buildroot}/etc/locale-gen.conf
#	Add SUPPORTED file to pkg
sed -e '1,3d' -e 's|/| |g' -e 's| \\||g' ../localedata/SUPPORTED > %{buildroot}/usr/share/i18n/SUPPORTED

#	install C.UTF-8 so that it is always available
install -dm755 %{buildroot}/usr/lib/locale
cp -r ./C.UTF-8 -t %{buildroot}/usr/lib/locale
sed -i '/#C\.UTF-8 /d' -i %{buildroot}/etc/locale-gen.conf

#	Provide tracing probes to libstdc++ for exceptions, possibly for other
#	libraries too. Useful for gdb's catch command.
install -Dm644 %{SOURCE3} %{buildroot}/usr/include/sys/sdt.h
install -Dm644 %{SOURCE4} %{buildroot}/usr/include/sys/sdt-config.h
#	Add tempfiles
install -D -m644 %{SOURCE6} %{buildroot}/usr/lib/tmpfiles.d/%{name}.conf
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%attr(755,root,root) /usr/bin/locale-gen
%config(noreplace) /etc/locale-gen.conf
%config(noreplace) /etc/gai.conf
%config(noreplace) /etc/nscd.conf

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.36-1
-	Initial build.	First version
