Summary: Correctly generate plurals, singular nouns, ordinals, indefinite articles
Name: python-inflect
Version: 6.0.2
Release: 1
License: MIT
Group: Python
URL: https://github.com/jazzband/inflect
#	Source
Source0: http://www.example.org/Packages/python-inflect/inflect-6.0.2.tar.gz
Requires: python
Requires: python-pydantic
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-setuptools-scm
BuildRequires: python-toml
#	depends=(python python-pydantic)
#	makedepends=(python-build python-installer python-setuptools-scm python-toml python-wheel)
#	checkdepends=(python-pytest)
%description
Correctly generate plurals, singular nouns, ordinals, indefinite articles; convert numbers to words.

%prep
%setup -q -n inflect-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 6.0.2-1
-	Initial build.	First version
