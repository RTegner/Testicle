Summary: Package manager
Name: rpm
Version: 4.18.0
Release: 1
License: GPLv2
Group: Core
URL: http://rpm.org
#	Source
Source0: http://www.example.org/Packages/rpm/rpm-4.18.0.tar.bz2
Source1: http://www.example.org/Packages/rpm/rpm-no-man-pages.patch
#	Build Requirements
BuildRequires: libarchive
BuildRequires: libgcrypt
BuildRequires: lua
BuildRequires: popt
BuildRequires: sqlite
%description
Package manager

%prep
%setup -q -n %{name}-%{version}
#patch -Np0 -i %SOURCE1
sed 's:/bin/sh:/usr/bin/sh:' -i macros.in
#	sh autogen.sh --noconfigure

%build
_options=( --prefix=/usr
	--program-prefix=
	--sysconfdir=/etc
	--sharedstatedir=/var/lib
	--localstatedir=/var
	--with-crypto=libgcrypt
	--with-gnu-ld
	--with-archive
	--with-cap
	--with-acl
#	--with-lua
	--without-selinux
	--enable-zstd
	--enable-sqlite
	--enable-python
	--disable-openmp
	--disable-dependency-tracking
	--disable-silent-rules
	--disable-nls
	--disable-rpath)
./configure ${_options[@]}
#	sed '/^SUBDIRS =/ s/docs//' -i Makefile
%{make_build}

%install
%{make_install}
#	remove empty directories
rm -rf %{buildroot}/usr/share/man/{fr,ja,ko,pl,ru,sk}
rm -rf %{buildroot}/usr/lib/rpm/macros.d
rm -rf %{buildroot}/usr/lib/rpm/lua
rm -rf %{buildroot}/var
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.18.0-1
-	Initial build.	First version
