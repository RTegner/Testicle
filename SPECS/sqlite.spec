Summary: A C library that implements an SQL database engine
Name: sqlite
Version: 3.40.1
Release: 1
License: Public Domain
Group: Core
URL: https://www.sqlite.org
#	Source
#	Source0: http://www.example.org/Packages/sqlite/sqlite-autoconf-3400000.tar.gz
Source0: http://www.example.org/Packages/sqlite/sqlite-src-3400100.zip
Source1: http://www.example.org/Packages/sqlite/sqlite-lemon-system-template.patch
BuildRequires: tcl
BuildRequires: readline
BuildRequires: zlib
%description
A C library that implements an SQL database engine

%prep
%setup -q -n sqlite-src-3400100
#	patch taken from Fedora7
#	https://src.fedoraproject.org/rpms/sqlite/blob/master/f/sqlite.spec
patch -Np1 -i %{SOURCE1}

#autoreconf -vfi

#CFLAGS="-march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions \
#        -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security \
#        -fstack-clash-protection -fcf-protection"

%build
export CPPFLAGS="-DSQLITE_ENABLE_COLUMN_METADATA=1 \
	-DSQLITE_ENABLE_UNLOCK_NOTIFY \
	-DSQLITE_ENABLE_DBSTAT_VTAB=1 \
	-DSQLITE_ENABLE_FTS3_TOKENIZER=1 \
	-DSQLITE_ENABLE_FTS3_PARENTHESIS \
	-DSQLITE_SECURE_DELETE \
	-DSQLITE_ENABLE_STMTVTAB \
	-DSQLITE_MAX_VARIABLE_NUMBER=250000 \
	-DSQLITE_MAX_EXPR_DEPTH=10000 \
	-DSQLITE_ENABLE_MATH_FUNCTIONS"

_options=(--prefix=/usr
	--disable-static
	--enable-fts3
	--enable-fts4
	--enable-fts5
	--enable-rtree
	TCLLIBDIR=/usr/lib/sqlite%{version})
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
#	install -m755 showdb showjournal showstat4 showwal sqldiff %{buildroot}/usr/bin/
#	install manpage
install -m755 -d %{buildroot}/usr/share/man/man1
install -m644 sqlite3.1 %{buildroot}/usr/share/man/man1/
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 11 2023 scott andrews <scott-andrews@columbus.rr.com> 3.40.1-1
-	Initial build.	First version
