Summary: GNU utilities to locate files
Name: findutils
Version: 4.9.0
Release: 1
License: GPL3
Group: Core
URL: http://www.gnu.org/software/findutils
#	Source
Source0: http://www.example.org/Packages/findutils/findutils-4.9.0.tar.xz
%description
GNU utilities to locate files
 
%prep
%setup -q -n %{name}-%{version}
#	Don't build or install locate because we use mlocate,
#	which is a secure version of locate.
sed -e '/^SUBDIRS/s/locate//' -e 's/frcode locate updatedb//' -i Makefile.in
 
%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
#	do not build locate, but the docs want a file in there.
make -C locate dblocation.texi
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.9.0-1
-	Initial build.	First version
