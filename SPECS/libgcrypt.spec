Summary: General purpose cryptographic library based on the code from GnuPG
Name: libgcrypt
Version: 1.10.1
Release: 1
License: LGPL
Group: Core
URL: https://www.gnupg.org
#	Source
Source0: http://www.example.org/Packages/libgcrypt/libgcrypt-1.10.1.tar.bz2
#	Build Requirements
BuildRequires: libgpg-error
%description
General purpose cryptographic library based on the code from GnuPG
 
%prep
%setup -q -n %{name}-%{version}
#	tests fail due to systemd+libseccomp preventing memory syscalls when building in chroots
#	t-secmem: line 176: gcry_control (GCRYCTL_INIT_SECMEM, pool_size, 0) failed: General error
#	FAIL: t-secmem
#	t-sexp: line 1174: gcry_control (GCRYCTL_INIT_SECMEM, 16384, 0) failed: General error
#	FAIL: t-sexp
sed -i "s:t-secmem::" tests/Makefile.am
sed -i "s:t-sexp::" tests/Makefile.am
autoreconf -vfi
 
%build
_options=(--prefix=/usr
	-disable-static
	--disable-padlock-support)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.10.1-1
-	Initial build.	First version
