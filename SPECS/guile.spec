Summary: Portable, embeddable Scheme implementation written in C
Name: guile
Version: 3.0.8
Release: 1
License: GPL
Group: Core
URL: https://www.gnu.org/software/guile/
#	Source
Source0: http://www.example.org/Packages/guile/guile-3.0.8.tar.xz
BuildRequires: gmp
BuildRequires: ncurses
BuildRequires: libunistring
BuildRequires: gc
BuildRequires: libffi
BuildRequires: libxcrypt
%description
Portable, embeddable Scheme implementation written in C

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--disable-static
	--disable-error-on-warning)
./configure "${_options[@]}"
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
%{make_build}

%install
%{make_install}
#%strip_libs
#%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Jan  2 2023 scott andrews <scott-andrews@columbus.rr.com> %{version}-%{release}
-	ToolChain
