Summary: The libnsl package contains the public client interface for NIS(YP) and NIS+
Name: libnsl
Version: 2.0.0
Release: 1
License: LGPL-2.1
Group: Networking-Libraries
URL: https://github.com/thkukuk/libnsl
#	Source
Source0: http://www.example.org/Packages/libnsl/libnsl-2.0.0.tar.xz
%description
The libnsl package contains the public client interface for
NIS(YP) and NIS+. It replaces the NIS library that used to be in glibc.
 
%prep
%setup -q -n %{name}-%{version}
autoreconf -fiv
 
%build
_options=( --sysconfdir=/etc --disable-static )
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.0.0-1
-	Initial build.	First version
