Summary: Low level core library
Name: glib2
Version: 2.74.5
Release: 1
License: LGPL
Group: Core
URL: https://wiki.gnome.org/Projects/GLib
#	Source
Source0: http://www.example.org/Packages/glib/glib-2.74.5.tar.xz
%description
Low level core library

%prep
%setup -q -n glib-%{version}

%build
cd %{_builddir}
#	CFLAGS+=" -DG_DISABLE_CAST_CHECKS"
_options=( glib-%{version} build
	--default-library both
	-D glib_debug=disabled
	-D selinux=disabled
	-D sysprof=disabled
	-D man=false
	-D gtk_doc=false)
arch-meson "${_options[@]}"
meson compile -C build

%install
cd %{_builddir}
meson install -C build --destdir %{buildroot}
python -m compileall -d /usr/share/glib-2.0/codegen %{buildroot}/usr/share/glib-2.0/codegen
python -O -m compileall -d /usr/share/glib-2.0/codegen %{buildroot}/usr/share/glib-2.0/codegen
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Tue Jan 24 2024 scott andrews <scott-andrews@columbus.rr.com> 2.74.5-1
-	Update version
*	Thu Oct 27 2022 scott andrews <scott-andrews@columbus.rr.com> 2.74.1-1
-	Initial build.	First version
