Summary: A low-level library for calling build-backends in pyproject.toml-based project
Name: python-pyproject-hooks
Version: 1.0.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/pypa/pyproject-hooks
#	Source
Source0:http://www.example.org/Packages/pyproject-hooks/pyproject_hooks-1.0.0.tar.gz
%description
A low-level library for calling build-backends in pyproject.toml-based project

%prep
%setup -q -n pyproject_hooks-%{version}

%build
python -m build --wheel --no-isolation

%install
 python -m installer --destdir="%{buildroot}" dist/*.whl
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE
%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 1.0.0-1
-	ToolChain
