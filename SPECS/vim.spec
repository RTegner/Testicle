Summary: Vi Improved, a highly configurable, improved version of the vi text editor
Name: vim
Version: 9.0.1182
Release: 1
License: Charityware
Group: Core
URL: https://www.vim.org
#	Source
Source0: http://www.example.org/Packages/vim/v9.0.1182.tar.gz
Source1: http://www.example.org/Packages/vim/vimrc
Source2: http://www.example.org/Packages/vim/vimrc.skel
#	Source3: http://www.example.org/Packages/vim/vim.patch
#BuildRequires: glibc
#BuildRequires: libgcrypt
#BuildRequires: python
#BuildRequires: lua
#BuildRequires: gpm
#BuildRequires: gawk
#BuildRequires: tcl
#BuildRequires: zlib
#	BuildRequires: ruby
#	BuildRequires: libxt
#	BuildRequires: gtk3
#	BuildRequires: libcanberra
#makedepends=('glibc' 'libgcrypt' 'gpm' 'python' 'ruby' 'libxt' 'gtk3' 'lua'
#	'gawk' 'tcl' 'zlib' 'libcanberra')


%description
Vi Improved, a highly configurable, improved version of the vi text editor

%prep
%setup -q -n %{name}-%{version}
cd src
#	patch -Np0 -i %{SOURCE3}
autoconf

%build
_options=(--prefix=/usr
	--localstatedir=/var/lib/vim
	--with-features=huge
	--with-compiledby='Gremlin Linux' \
#	--enable-gpm
	--enable-acl
	--with-x=no
	--disable-gui
	--enable-multibyte
#	--enable-cscope
#	--enable-netbeans
	--enable-perlinterp=dynamic
	--enable-python3interp=dynamic
#	--enable-rubyinterp=dynamic
	--enable-luainterp=dynamic
	--enable-tclinterp=dynamic
	--disable-canberra)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
ln -vs vim %{buildroot}/usr/bin/vi
mkdir -vp %{buildroot}/etc/skel
install -t %{buildroot}/etc/ -m644 %{SOURCE1}
install -t %{buildroot}/etc/skel -m644 %{SOURCE2}
mv %{buildroot}/etc/skel/vimrc.skel  %{buildroot}/etc/skel/.vimrc
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) /etc/vimrc
%config(noreplace) /etc/skel/.vimrc

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 9.0.1182-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 9.0.0910-1
-	Initial build.	First version
