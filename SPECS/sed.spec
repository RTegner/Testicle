Summary: GNU stream editor
Name: sed
Version: 4.9
Release: 1
License: GPLv3
Group: Core
URL: https://www.gnu.org/software/sed
#	Source
Source0: http://www.example.org/Packages/sed/sed-4.9.tar.xz
BuildRequires: gettext
%description
GNU stream editor

%prep
%setup -q -n %{name}-%{version}
autoreconf -vfi

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.9-1
-	Initial build.	First version
