Summary: Library for manipulating Unicode strings and C strings
Name: libunistring
Version: 1.1
Release: 1
License: GPL
Group: Core
URL: https://www.gnu.org/software/libunistring
#	Source
Source0: http://www.example.org/Packages/libunistring/libunistring-1.1.tar.xz
%description
Library for manipulating Unicode strings and C strings

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Jan  2 2023 scott andrews <scott-andrews@columbus.rr.com> %{version}-%{release}
-	ToolChain
