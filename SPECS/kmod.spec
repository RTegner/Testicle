Summary: Linux kernel module management tools and library
Name: kmod
Version: 30
Release: 1
License: GPLv2.1
Group: Core
URL: https://git.kernel.org/?p=utils/kernel/kmod/kmod.git;a=summary
#	Source
Source0: http://www.example.org/Packages/kmod/kmod-30.tar.xz
Source1: http://www.example.org/Packages/kmod/0001-master.patch
Source2: http://www.example.org/Packages/kmod/depmod-search.conf
Source3: http://www.example.org/Packages/kmod/tmpfiles
%description
Linux kernel module management tools and library

%prep
%setup -q -n %{name}-%{version}
patch -Np1 < %{_sourcedir}/0001-master.patch

%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--with-openssl
	--with-xz
	--with-zstd
	--with-zlib)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
#	installed by tmpfiles
#	install -dm755 %{buildroot}/{etc,usr/lib}/{depmod,modprobe}.d
install -vdm 755 %{buildroot}/usr/bin
for tool in {ins,ls,rm,dep}mod mod{probe,info}; do
	ln -s kmod %{buildroot}/usr/bin/${tool}
done
#	install depmod.d file for search/ dir
install -Dm644 %{_sourcedir}/depmod-search.conf %{buildroot}/usr/lib/depmod.d/search.conf
#	setup tmpfiles
install -D -m644 %{_sourcedir}/tmpfiles %{buildroot}/usr/lib/tmpfiles.d/kmod.conf
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 30-1
-	Initial build.	First version
