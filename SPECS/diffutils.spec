Summary: Utility programs used for creating patch files
Name: diffutils
Version: 3.8
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/diffutils
#	Source
Source0: http://www.example.org/Packages/diffutils/diffutils-3.8.tar.xz
%description
Utility programs used for creating patch files
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.8-1
-	Initial build.	First version
