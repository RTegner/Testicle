Summary: Premier connectivity tool for remote login with the SSH protocol
Name: openssh
Version: 9.1p1
Release: 1
License: BSD
Group: Base
URL: https://www.openssh.com/portable.html
#	Source
Source0: http://www.example.org/Packages/openssh/openssh-9.1p1.tar.gz
Source1: http://www.example.org/Packages/openssh/openssh-9.0p1-sshd_config.patch
Source2: http://www.example.org/Packages/openssh/sshdgenkeys.service
Source3: http://www.example.org/Packages/openssh/sshd.service
Source4: http://www.example.org/Packages/openssh/sshd.conf
Source5: http://www.example.org/Packages/openssh/sshd.pam
#	Build Requirements
BuildRequires: bash
BuildRequires: glibc
BuildRequires: ldns
BuildRequires: libedit
BuildRequires: libxcrypt
BuildRequires: openssl
BuildRequires: zlib
%description
Premier connectivity tool for remote login with the SSH protocol

%prep
%setup -q -n %{name}-%{version}
patch -Np1 -i %{SOURCE1}

%build
_options=(
    --prefix=/usr
    --sbindir=/usr/bin
    --libexecdir=/usr/lib/ssh
    --sysconfdir=/etc/ssh
    --disable-strip
    --with-ldns
    --with-libedit
#    --with-security-key-builtin
    --with-ssl-engine
#    --with-pam
    --with-privsep-user=nobody
#    --with-kerberos5=/usr
    --with-xauth=/usr/bin/xauth
    --with-pid-dir=/run
    --with-default-path=/usr/local/sbin:/usr/local/bin:/usr/bin)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
ln -sf ssh.1.gz %{buildroot}/usr/share/man/man1/slogin.1.gz
install -Dm644 %{_sourcedir}/sshdgenkeys.service -t %{buildroot}/usr/lib/systemd/system/
install -Dm644 %{_sourcedir}/sshd.service -t %{buildroot}/usr/lib/systemd/system/
install -Dm644 %{_sourcedir}/sshd.conf -t %{buildroot}/usr/lib/tmpfiles.d/
#  install -Dm644 %{_sourcedir}/sshd.pam %{buildroot}/etc/pam.d/sshd

install -Dm755 contrib/findssl.sh -t %{buildroot}/usr/bin/
install -Dm755 contrib/ssh-copy-id -t %{buildroot}/usr/bin/
install -Dm644 contrib/ssh-copy-id.1 -t %{buildroot}/usr/share/man/man1/

rm -r %{buildroot}/var
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENCE

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 9.1p1-1
-	Initial build.	First version
