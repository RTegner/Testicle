Summary: Find your home and other directories on any platform
Name: perl-file-homedir
Version: 1.006
Release: 1
License: PerlArtistic
Group: Core
URL: https://www.cpan.org
#	Source
Source0: http://www.example.org/Packages/perl-file-homedir/File-HomeDir-1.006.tar.gz
%description
Find your home and other directories on any platform
 
%prep
%setup -q -n File-HomeDir-1.006
 
%build
perl Makefile.PL INSTALLDIRS=vendor
make
 
%install
%{make_install}
find %{buildroot} -name perllocal.pod -delete
find %{buildroot} -name .packlist -delete
rm -r %{buildroot}/usr/lib
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.006-1
-	Initial build.	First version
