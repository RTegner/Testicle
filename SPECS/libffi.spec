Summary: Portable foreign function interface library
Name: libffi
Version: 3.4.4
Release: 1
License: GPLv2
Group: Core
URL: https://sourceware.org/libffi
#	Source
Source0: http://www.example.org/Packages/libffi/libffi-3.4.4.tar.gz
%description
Portable foreign function interface library

%prep
%setup -q -n %{name}-%{version}
#	sh autogen.sh

%build
_options=(--prefix=/usr
	--disable-static
	--disable-multi-os-directory
	--disable-exec-static-tramp
	-enable-pax_emutramp)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.4.4-1
-	Initial build.	First version
