Summary: Clean single-source support for Python 3 and 2
Name: python-future
Version: 0.18.3
Release: 1
License: MIT
Group: Python
URL: https://python-future.org
#	Source
Source0: http://www.example.org/Packages/python-future/future-0.18.3.tar.gz
#Provides: futurize
#Provides: pasteurize
Requires: python
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-toml
BuildRequires: python-tomli
BuildRequires: python-wheel
BuildRequires: python-setuptools
#	depends=('python')
#	provides=('futurize' 'pasteurize')
#	checkdepends=('python-requests')
#	makedepends=('python-setuptools')
#	optdepends=('python-setuptools: futurize and pasteurize scripts')
%description
Clean single-source support for Python 3 and 2

%prep
%setup -q -n future-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Jan  2 2023 scott andrews <scott-andrews@columbus.rr.com> %{version}-%{release}
-	ToolChain
