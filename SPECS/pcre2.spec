Summary: A library that implements Perl 5-style regular expressions
Name: pcre2
Version: 10.41
Release: 1
License: BSD
Group: Core
URL: https://www.pcre.org/
#	Source
Source0: http://www.example.org/Packages/pcre2/pcre2-10.41.tar.gz
%description
A library that implements Perl 5-style regular expressions  2nd version

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
--enable-pcre2-16
--enable-pcre2-32
--enable-jit
--enable-pcre2grep-libz
--enable-pcre2grep-libbz2
--enable-pcre2test-libreadline)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENCE

%changelog
*	Tue Jan 10 2023 scott andrews <scott-andrews@columbus.rr.com> 10.42-1
-	Initial build.	First version
