%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\([^.]*\\.pl\\)
Summary: GNU troff text-formatting system
Name: groff
Version: 1.22.4
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/groff
#	Source
Source0: http://www.example.org/Packages/groff/groff-1.22.4.tar.gz
Source1: http://www.example.org/Packages/groff/display-utc-times.patch
Source2: http://www.example.org/Packages/groff/site.tmac
%description
GNU troff text-formatting system

%prep
%setup -q -n %{name}-%{version}
#	always use UTC times for display - using localtime is problematic for reproducible builds
#	fixes FS#69123 - patch taken from Debian
patch -Np1 -i %{_sourcedir}/display-utc-times.patch

%build
_options=(--prefix=/usr)
PAGE=letter ./configure "${_options[@]}"
make -j1

%install
%{make_install}
#	add compatibility symlinks
ln -s eqn %{buildroot}/usr/bin/geqn
ln -s tbl %{buildroot}/usr/bin/gtbl
ln -s soelim %{buildroot}/usr/bin/zsoelim
#	FS33760 - TERMCAP variables not followed
#	TODO: everyone is doing this - find out why upstream does not...
cat %{_sourcedir}/site.tmac >> %{buildroot}/usr/share/groff/site-tmac/man.local
cat %{_sourcedir}/site.tmac >> %{buildroot}/usr/share/groff/site-tmac/mdoc.local
rmdir %{buildroot}/usr/share/groff/site-font
rmdir %{buildroot}/usr/lib/groff/site-tmac
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.22.4-1
-	Initial build.	First version
