Summary: Powerful lightweight programming language designed for extending applications
Name: lua
Version: 5.4.4
Release: 1
License: MIT
Group: Core
URL: http://www.lua.org
#	Source
Source0: http://www.example.org/Packages/lua/lua-5.4.4.tar.gz
Source1: http://www.example.org/Packages/lua/liblua.so.patch
Source2: http://www.example.org/Packages/lua/lua.pc
BuildRequires: readline
#	depends=('readline')
%description
Powerful lightweight programming language designed for extending applications

%prep
%setup -q -n %{name}-%{version}
patch -p1 -i %{SOURCE1}
sed 's/%VER%/5.4/g;s/%REL%/%{version}/g' %{SOURCE2} > lua.pc

%build
make MYCFLAGS="-fPIC" linux-readline

%install
make TO_LIB="liblua.a liblua.so liblua.so.5.4 liblua.so.%{version}" \
	INSTALL_DATA="cp -d" \
	INSTALL_TOP=%{buildroot}/usr \
	INSTALL_MAN=%{buildroot}/usr/share/man/man1 \
	install
ln -sf /usr/bin/lua %{buildroot}/usr/bin/lua5.4
ln -sf /usr/bin/luac %{buildroot}/usr/bin/luac5.4
ln -sf /usr/lib/liblua.so.%{version} %{buildroot}/usr/lib/liblua5.4.so

install -Dm644 lua.pc %{buildroot}/usr/lib/pkgconfig/lua.pc
ln -sf lua.pc %{buildroot}/usr/lib/pkgconfig/lua54.pc
ln -sf lua.pc %{buildroot}/usr/lib/pkgconfig/lua5.4.pc
ln -sf lua.pc %{buildroot}/usr/lib/pkgconfig/lua-5.4.pc

install -d %{buildroot}/usr/share/doc/%{name}
install -m644 doc/*.{gif,png,css,html} %{buildroot}/usr/share/doc/%{name}
rm -vrf %{buildroot}/usr/share/%{name}
rm -vrf %{buildroot}/usr/lib/%{name}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 5.4.4-1
-	Initial build.	First version
