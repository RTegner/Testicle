Summary: Lightweight HTTP server and IMAP/POP3 proxy server
Name: nginx
Version: 1.22.1
Release: 1
License: custom
Group: Server
URL: https://nginx.org
#	Source
Source0: http://www.example.org/Packages/nginx/nginx-1.22.1.tar.gz
Source1: http://www.example.org/Packages/nginx/service
Source2: http://www.example.org/Packages/nginx/nginx.conf
Source3: http://www.example.org/Packages/nginx/git.example.org.conf
Source4: http://www.example.org/Packages/nginx/mail.example.org.conf
Source5: http://www.example.org/Packages/nginx/music.example.org.conf
Source6: http://www.example.org/Packages/nginx/video.example.org.conf
Source7: http://www.example.org/Packages/nginx/tmpfiles
%description
Lightweight HTTP server and IMAP/POP3 proxy server

%prep
%setup -q -n %{name}-%{version}

%build
_options=( --prefix=/etc/nginx
	--sbin-path=/usr/bin/nginx
	--modules-path=/usr/lib/nginx
	--conf-path=/etc/nginx/nginx.conf
	--error-log-path=stderr
	--http-log-path=/var/log/nginx/access.log
	--pid-path=/run/nginx.pid
	--lock-path=/run/lock/nginx.lock
	--http-client-body-temp-path=/var/lib/nginx/client-body
	--http-proxy-temp-path=/var/lib/nginx/proxy
	--http-fastcgi-temp-path=/var/lib/nginx/fastcgi
	--http-scgi-temp-path=/var/lib/nginx/scgi
	--http-uwsgi-temp-path=/var/lib/nginx/uwsgi
	--user=nobody
	--group=nobody )

_modules=( --with-threads
	--with-http_mp4_module
	--with-http_ssl_module)

#--with-http_ssl_module
#--with-pcre-jit
#	--with-file-aio
#	--with-cpp_test_module
#	--with-http_addition_module
#	--with-http_auth_request_module
#	--with-http_dav_module
#	--with-http_degradation_module
#	--with-http_flv_module
#	--with-http_geoip_module
#	--with-http_gunzip_module
#	--with-http_gzip_static_module
#	--with-http_image_filter_module
#	--with-http_perl_module
#	--with-http_random_index_module
#	--with-http_realip_module
#	--with-http_secure_link_module
#	--with-http_slice_module
#	--with-http_stub_status_module
#	--with-http_sub_module
#	--with-http_xslt_module
#	--with-http_v2_module
#	--with-mail
#	--with-mail_ssl_module
#	--with-stream
#	--with-stream_ssl_module )

./configure ${_options[@]} ${_modules[@]}
%{make_build}

%install
%{make_install}
sed 's|\<user\s\+\w\+;|user http;|g'  -i %{buildroot}/etc/nginx/nginx.conf
sed '44s|html|/usr/share/nginx/html|' -i %{buildroot}/etc/nginx/nginx.conf
sed '54s|html|/usr/share/nginx/html|' -i %{buildroot}/etc/nginx/nginx.conf
rm %{buildroot}/etc/nginx/*.default
install -dm755 %{buildroot}/usr/share/nginx
mv %{buildroot}/etc/nginx/html/ %{buildroot}/usr/share/nginx
install -Dm644 %{SOURCE1} %{buildroot}/usr/lib/systemd/system/nginx.service
install -D -m644 %{SOURCE7} %{buildroot}/usr/lib/tmpfiles.d/%{name}.conf
rmdir %{buildroot}/run
mkdir %{buildroot}/etc/nginx/conf.d
mkdir %{buildroot}/etc/nginx/sites-available
mkdir %{buildroot}/etc/nginx/sites-enabled
rm -r %{buildroot}/var
rm -r %{buildroot}/etc/nginx/conf.d
mv %{buildroot}/etc/nginx/nginx.conf %{buildroot}/etc/nginx/nginx.conf.default
cp %{SOURCE2} %{buildroot}/etc/nginx/
cp %{SOURCE3} %{buildroot}/etc/nginx/sites-available
cp %{SOURCE4} %{buildroot}/etc/nginx/sites-available
cp %{SOURCE5} %{buildroot}/etc/nginx/sites-available
cp %{SOURCE6} %{buildroot}/etc/nginx/sites-available
cd %{buildroot}/etc/nginx/sites-enabled
ln -vs ../sites-available/git.example.org.conf
ln -vs ../sites-available/music.example.org.conf
ln -vs ../sites-available/mail.example.org.conf
ln -vs ../sites-available/video.example.org.conf
cd -
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%post
[ -x /usr/bin/systemd-sysusers ] && /usr/bin/systemd-sysusers
[ -x /usr/bin/systemd-tmpfiles ] && /usr/bin/systemd-tmpfiles --create

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) /etc/nginx/fastcgi.conf
%config(noreplace) /etc/nginx/fastcgi_params
%config(noreplace) /etc/nginx/koi-utf
%config(noreplace) /etc/nginx/koi-win
%config(noreplace) /etc/nginx/mime.types
%config(noreplace) /etc/nginx/nginx.conf
%config(noreplace) /etc/nginx/scgi_params
%config(noreplace) /etc/nginx/uwsgi_params
%config(noreplace) /etc/nginx/win-utf
%config(noreplace) /etc/nginx/sites-available/git.example.org.conf
%config(noreplace) /etc/nginx/sites-available/mail.example.org.conf
%config(noreplace) /etc/nginx/sites-available/music.example.org.conf
%config(noreplace) /etc/nginx/sites-available/video.example.org.conf
%license LICENSE

%changelog
*	Sun Jan 01 2023 scott andrews <scott-andrews@columbus.rr.com> 1.22.1-1
-	Initial build.	First version
