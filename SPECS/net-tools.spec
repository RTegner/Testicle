Summary: Configuration tools for Linux networking
Name: net-tools
Version: 2.10
Release: 1
License: GPLv2
Group: Core
URL: http://net-tools.sourceforge.net
#	Source
Source0: http://www.example.org/Packages/net-tools/net-tools-2.10.tar.xz
%description
Configuration tools for Linux networking

%prep
%setup -q -n %{name}-%{version}

%build
export BINDIR="/usr/bin" SBINDIR="/usr/bin"
yes "" | make

%install
%{make_install} BINDIR="/usr/bin" SBINDIR="/usr/bin"
#	 the following is provided by yp-tools
rm %{buildroot}/usr/bin/{nis,yp}domainname
#	hostname is provided by inetutils
rm %{buildroot}/usr/bin/{hostname,dnsdomainname,domainname}
rm -r %{buildroot}/usr/share/man/man1
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.10-1
-	Initial build.	First version
