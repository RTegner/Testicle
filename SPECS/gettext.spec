Summary: GNU internationalization library
Name: gettext
Version: 0.21.1
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/gettext
#	Source
Source0: http://www.example.org/Packages/gettext/gettext-0.21.1.tar.xz
Source1: http://www.example.org/Packages/gettext/gettext-0.21-disable-libtextstyle.patch
%description
GNU internationalization library

%prep
%setup -q -n %{name}-%{version}
#	Do not build libtextstyle, as it depends on libcroco
#	which is now unmaintained and has known security bugs.
#	patch from Fedora
patch -p1 -i %{_sourcedir}/gettext-0.21-disable-libtextstyle.patch
autoreconf --force

%build
_options=(--prefix=/usr
	--enable-csharp
	--enable-nls
	--with-xz
	--without-included-gettext)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
#	chmod -v 0755 %{buildroot}/usr/lib/preloadable_libintl.so
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.21.1-1
-	Initial build.	First version
