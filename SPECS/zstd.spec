Summary: Zstandard is a real-time compression algorithm
Name: zstd
Version: 1.5.2
Release: 1
License: BSD GPL2
Group: Core
URL: https://facebook.github.io/zstd
#	Source
Source0: http://www.example.org/Packages/zstd/zstd-1.5.2.tar.gz
#	depends=(glibc gcc-libs zlib xz lz4)
#	makedepends=(cmake gtest ninja)
%description
Zstandard is a real-time compression algorithm, providing high compression ratios. It offers a very wide range of compression|speed trade-offs, while being backed by a very fast decoder.

%prep
%setup -q -n %{name}-%{version}
#	avoid error on tests without static libs, we use LD_LIBRARY_PATH
cp build/cmake/CMakeLists.txt{,.original}
sed '/build static library to build tests/d' -i build/cmake/CMakeLists.txt
sed 's/libzstd_static/libzstd_shared/g' -i build/cmake/tests/CMakeLists.txt

%build
_options=(-DCMAKE_BUILD_TYPE=None
    -DCMAKE_INSTALL_PREFIX=/usr
    -DCMAKE_INSTALL_LIBDIR=lib
    -DZSTD_BUILD_CONTRIB=ON
    -DZSTD_BUILD_STATIC=OFF
    -DZSTD_BUILD_TESTS=ON
    -DZSTD_PROGRAMS_LINK_SHARED=ON)
cmake -S build/cmake -B build -G Ninja "${_options[@]}"
cmake --build build

%install
DESTDIR=%{buildroot} cmake --install build
ln -sf /usr/bin/zstd %{buildroot}/usr/bin/zstdmt
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
%license LICENSE

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.5.2-1
-	Initial build.	First version
