Summary: Low-level library for installing a Python package from a wheel distribution
Name: python-installer
Version: 0.6.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/pypa/installer
#	Source
Source0: http://www.example.org/Packages/python-installer/0.6.0.tar.gz
%description
Low-level library for installing a Python package from a wheel distribution

%prep
%setup -q -n installer-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	remove windows entrypoint scripts executables
rm %{buildroot}/usr/lib/python*/site-packages/installer/_scripts/*.exe
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/installer-%{version}.dist-info

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.6.0-1
-	Initial build.	First version
