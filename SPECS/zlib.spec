Summary: Compression library implementing the deflate compression method found in gzip and PKZIP
Name: zlib
Version: 1.2.13
Release: 1
License: Other
Group: Core
URL: https://www.zlib.net
#	Source
Source0: http://www.example.org/Packages/zlib/zlib-1.2.13.tar.gz
%description
Compression library implementing the deflate compression method found in gzip and PKZIP
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.2.13-1
-	Initial build.	First version
