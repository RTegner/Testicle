Summary: High productivity build system
Name: meson
Version: 1.0.0
Release: 1
License: Apache
Group: Core
URL: https://mesonbuild.com
#	Source
Source0: http://www.example.org/Packages/meson/1.0.0.tar.gz
Source1: http://www.example.org/Packages/meson/0001-Skip-broken-tests.patch
Source2: http://www.example.org/Packages/meson/arch-meson
#	depends=(ninja python)
#	makedepends=( python-build python-installer python-wheel )
Requires: python
%description
High productivity build system

%prep
%setup -q -n %{name}-%{version}
patch -Np1 -i %{SOURCE1}

%build
#	python -m build --wheel --no-isolation
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Vim
install -d %{buildroot}/usr/share/vim/vimfiles
cp -rt %{buildroot}/usr/share/vim/vimfiles data/syntax-highlighting/vim/*/
#	Bash completions
install -Dt %{buildroot}/usr/share/bash-completion/completions -m644 data/shell-completions/bash/*
install -Dt %{buildroot}/usr/share/zsh/site-functions -m644 data/shell-completions/zsh/*
#	Arch packaging helper
install -D %{SOURCE2} -t %{buildroot}/usr/bin
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/meson-%{version}.dist-info

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 1.0.0-1
-	Initial build.	First version
