Summary: Bootloader and VL805 USB controller EEPROM update tool for RPi4
Name: rpi-eeprom
Version: 2022.12.07
Release: 1
License: custom
Group: Core
URL: https://github.com/raspberrypi/rpi-eeprom
#	Source
Source0: http://www.example.org/Packages/rpi-eeprom/v2022.12.07-138a1.tar.gz
%description
Bootloader and VL805 USB controller EEPROM update tool for RPi4

%prep
%setup -q -n %{name}-%{version}-138a1

%build
true

%install

install -pd %{buildroot}/usr/bin
install -pm755 rpi-eeprom-config %{buildroot}/usr/bin/rpi-eeprom-config
install -pm755 rpi-eeprom-digest %{buildroot}/usr/bin/rpi-eeprom-digest
install -pm755 rpi-eeprom-update %{buildroot}/usr/bin/rpi-eeprom-update
install -pDm644 rpi-eeprom-update-default %{buildroot}/etc/default/rpi-eeprom-update

# Arch ARM does not ship raspi-config, removes text line
sed -i "/to change the release/d" %{buildroot}/usr/bin/rpi-eeprom-update

install -pd %{buildroot}/usr/lib/firmware/raspberrypi/bootloader/backup
for target in beta critical stable; do
	cp -a firmware/$target %{buildroot}/usr/lib/firmware/raspberrypi/bootloader
	# remove old images
	rm -f %{buildroot}/usr/lib/firmware/raspberrypi/bootloader/$target/"pieeprom-202[0,1]*.bin"
done
ln -sf critical %{buildroot}/usr/lib/firmware/raspberrypi/bootloader/default
ln -sf stable %{buildroot}/usr/lib/firmware/raspberrypi/bootloader/latest
# firmware files should not be executable
find %{buildroot}/usr/lib/firmware/raspberrypi/bootloader -name '*.bin' -exec chmod 644 '{}' +
touch %{buildroot}/usr/lib/firmware/raspberrypi/bootloader/backup/.hidden
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Fri Jan 20 2023 scott andrews <scott-andrews@columbus.rr.com> 2022.12.0-1
-	Initial build.	First version
