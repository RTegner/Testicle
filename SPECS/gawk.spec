Summary: GNU version of awk
Name: gawk
Version: 5.2.1
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/gawk
#	Source
Source0: http://www.example.org/Packages/gawk/gawk-5.2.1.tar.xz
Provides: /usr/bin/gawk
%description
GNU version of awk
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=( --prefix=/usr
	--libexecdir=/usr/lib
	--sysconfdir=/etc
	--without-libsigsegv)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
chmod a+x %{buildroot}/etc/profile.d/gawk.csh
chmod a+x %{buildroot}/etc/profile.d/gawk.sh
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 5.2.1-1
-	Initial build.	First version
