Summary: XML C parser and toolkit
Name: libxml2
Version: 2.10.3
Release: 1
License: MIT
Group: Core
URL: http://www.xmlsoft.org
#	Source
Source0: http://www.example.org/Packages/libxml2/libxml2-v2.10.3.tar.gz
Source1: http://www.example.org/Packages/libxml2/libxml2-2.9.8-python3-unicode-errors.patch
Source2: http://www.example.org/Packages/libxml2/no-fuzz.diff
#	Build Requirements
#BuildRequires: bash
#BuildRequires: glibc
#BuildRequires: ncurses
#BuildRequires: pkg-config
#BuildRequires: readline
#BuildRequires: xz
#BuildRequires: zlib
%description
XML C parser and toolkit

%prep
%setup -q -n %{name}-v%{version}
#	Use xmlconf from conformance test suite
ln -s ../xmlconf
#	https://src.fedoraproject.org/rpms/libxml2/tree/rawhide
patch -Np1 -i %{SOURCE1}
#	Do not run fuzzing tests
patch -Np1 -i %{SOURCE2}
NOCONFIGURE=1 ./autogen.sh

%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--localstatedir=/var
	--with-threads
	--with-history
	--with-python=/usr/bin/python
	--disable-static
	--with-history)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license Copyright

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.10.3-1
-	Initial build.	First version
