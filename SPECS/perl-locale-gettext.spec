Summary: Permits access from Perl to the gettext() family of functions
Name: perl-locale-gettext
Version: 1.07
Release: 1
License: GPL
Group: Core
URL: https://search.cpan.org/dist/Locale-gettext
#	Source
Source0: http://www.example.org/Packages/perl-locale-gettext/Locale-gettext-1.07.tar.gz
%description
Permits access from Perl to the gettext() family of functions
 
%prep
%setup -q -n Locale-gettext-%{version}
 
%build
#	install module in vendor directories.
PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
%{make_build}
 
%install
%{make_install}
#	remove perllocal.pod and .packlist
find %{buildroot} -name perllocal.pod -delete
find %{buildroot} -name .packlist -delete
rmdir %{buildroot}/usr/lib/perl5/5.36/core_perl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.07-1
-	Initial build.	First version
