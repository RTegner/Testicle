Summary: Extended attribute support library for ACL support
Name: attr
Version: 2.5.1
Release: 1
License: GPLv2
Group: Core
URL: http://savannah.nongnu.org/projects/attr
#	Source
Source0: http://www.example.org/Packages/attr/attr-2.5.1.tar.gz
%description
Extended attribute support library for ACL support

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--disable-static
	--docdir=/usr/share/doc/%{name}
#	arch
	--libdir=/usr/lib
	--libexecdir=/usr/lib
)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.5.1-1
-	Initial build.	First version
