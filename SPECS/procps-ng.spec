Summary: Utilities for monitoring your system and its processes
Name: procps-ng
Version: 3.3.17
Release: 1
License: GPLv2
Group: Core
URL: https://sourceforge.net/projects/procps-ng
#	Source
Source0: http://www.example.org/Packages/procps-ng/procps-ng-3.3.17.tar.xz
%description
Utilities for monitoring your system and its processes
 
%prep
%setup -q -n procps-%{version}
sed 's:<ncursesw/:<:g' -i watch.c
 
%build
_options=(--prefix=/usr
	--exec-prefix=/
	--sysconfdir=/etc
	--libdir=/usr/lib
	--bindir=/usr/bin
	--sbindir=/usr/bin
	--enable-watch8bit
	--with-systemd
	--disable-modern-top
	--disable-kill)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
rmdir %{buildroot}/usr/share/man/pt_BR/man5
rmdir %{buildroot}/usr/share/man/pl/man5
rmdir %{buildroot}/usr/share/man/sv/man5
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.3.17-1
-	Initial build.	First version
