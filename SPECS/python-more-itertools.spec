Summary: More routines for operating on iterables, beyond itertools
Name: python-more-itertools
Version: 9.0.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/more-itertools/more-itertools
#	Source
Source0: http://www.example.org/Packages/python-more-itertools/more-itertools-9.0.0.tar.gz
#Requires: python
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-flit
#	depends=('python')
#	makedepends=('git' 'python-build' 'python-installer' 'python-wheel' 'python-flit-core')

%description
More routines for operating on iterables, beyond itertools

%prep
%setup -q -n more-itertools-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 9.0.0-1
-	Initial build.	First version
