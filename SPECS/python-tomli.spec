Summary: A lil TOML parser
Name: python-tomli
Version: 2.0.1
Release: 1
License: MIT
Group: Python
URL: https://github.com/hukkin/tomli
#	Source
Source0: http://www.example.org/Packages/python-tomli/python-tomli-2.0.1.tar.gz
#Requires: python
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-flit
#BuildRequires: python-toml
#	depends=('python')
#	makedepends=('python-build' 'python-installer' 'python-flit-core')

%description
A lil TOML parser

%prep
%setup -q -n tomli-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/tomli-%{version}.dist-info
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 2.0.1-1
-	Initial build.	First version
