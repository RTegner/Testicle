Summary: A modern project, package, and virtual env manager (backend)
Name: python-hatchling
Version: 1.12.2
Release: 1
License: MIT
Group: Python Module
URL: https://github.com/pypa/hatch
#	Source
Source0: http://www.example.org/Packages/python-hatchling/hatchling-1.12.2.tar.gz
%description
A modern project, package, and virtual env manager (backend)

%prep
%setup -q -n hatchling-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Thu Feb 16 2023 scott andrews <scott-andrews@columbus.rr.com> 1.12.2-1
-	Initial build.	First version
