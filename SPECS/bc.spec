Summary: An arbitrary precision calculator language
Name: bc
Version: 6.2.2
Release: 1
License: GPLv3
Group: Core
URL: https://www.gnu.org/software/bc/
#	Source
Source0: http://www.example.org/Packages/bc/bc-6.2.2.tar.xz
#	Build Requirements
#	BuildRequires: ed
#	BuildRequires: readline
%description
An arbitrary precision calculator language

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr -G -O3 -r)
CC=gcc ./configure "${_options[@]}"
make

%install
make DESTDIR=%{buildroot} install
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Tue Jam 17 2023 scott andrews <scott-andrews@columbus.rr.com> 6.2.2.1-1
-	Initial build.	First version
