Summary: /etc/protocols and /etc/services provided by IANA
Name: iana-etc
Version: 20221220
Release: 1
License: OSLv3.0
Group: Core
URL: https://www.iana.org/protocols
#	Source
Source0: http://www.example.org/Packages/iana-etc/iana-etc-20221220.tar.gz
Source1: http://www.example.org/Packages/iana-etc/LICENSE
%description
/etc/protocols and /etc/services provided by IANA

%prep
%setup -q -n %{name}-%{version}

%build
true

%install
install -d "%{buildroot}/etc"
cp %{SOURCE1} .
install -Dm644 service-names-port-numbers.xml %{buildroot}/usr/share/iana-etc/port-numbers.iana
install -Dm644 protocol-numbers.xml %{buildroot}/usr/share/iana-etc/protocol-numbers.iana
/usr/bin/gawk -F"[<>]" '
BEGIN { print "# Full data: /usr/share/iana-etc/protocol-numbers.iana\n" }
(/<record/) { v=n="" }
(/<value/) { v=$3 }
(/<name/ && $3!~/ /) { n=$3 }
(/<\/record/ && n && v!="") { printf "%%-12s %%3i %s\n", tolower(n),v,n }
'  protocol-numbers.xml > %{buildroot}/etc/protocols
/usr/bin/gawk -F"[<>]" '
BEGIN { print "# Full data: /usr/share/iana-etc/port-numbers.iana\n" }
(/<record/) { n=u=p=c="" }
(/<name/ && !/\(/) { n=$3 }
(/<number/) { u=$3 }
(/<protocol/) { p=$3 }
(/Unassigned/ || /Reserved/ || /historic/) { c=1 }
(/<\/record/ && n && u && p && !c) { printf "%%-15s %%5i/%s\n", n,u,p }
' service-names-port-numbers.xml > %{buildroot}/etc/services
#
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 20221107-1
-	Initial build.	First version
