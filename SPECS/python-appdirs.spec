Summary: A simple, correct PEP517 package builder.
Name: python-appdirs
Version: 1.4.4
Release: 1
License: MIT
Group: Python
URL: https://github.com/pypa/build
#	Source
Source0: http://www.example.org/Packages/python-appdirs/appdirs-1.4.4.tar.gz
Requires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#	depends=('python')
#	makedepends=('python-setuptools')

%description
A simple, correct PEP517 package builder.

%prep
%setup -q -n appdirs-%{version}

%build
#python -m build --wheel --no-isolation
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
pkg=%{name}
pkg=${pkg##python-}
python -m pip install --ignore-installed --no-index --find-links dist --no-cache-dir --no-user --root %{buildroot} ${pkg}

#	python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/appdirs-%{version}.dist-info

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 1.4.4-1
-	Initial build.	First version
