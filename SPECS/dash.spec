Summary: POSIX compliant shell that aims to be as small as possible
Name: dash
Version: 0.5.11.5
Release: 1
License: BSD
Group: Core
URL: http://gondor.apana.org.au/~herbert/dash/
#	Source
Source0: http://www.example.org/Packages/dash/dash-0.5.11.5.tar.gz
%description
POSIX compliant shell that aims to be as small as possible
 
%prep
%setup -q -n %{name}-%{version}
autoreconf -fiv
 
%build
_options=(--prefix=/usr
	--bindir=/usr/bin
	--mandir=/usr/share/man
	--exec-prefix=""
	#	--with-libedit
)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%post
grep -q '/usr/bin/dash' /etc/shells || printf %s\n '/usr/bin/dash' >> /etc/shells
 
%postun
[ -e /usr/bin/dash] || sed -i '/^\/bin\/dash/d' etc/shells
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.5.11.5-1
-	Initial build.	First version
