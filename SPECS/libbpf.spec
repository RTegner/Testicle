Name: libbpf
Version: 1.0.1
Release: 1
License: LGPL2.1
Group: Core
Summary: Library for loading eBPF programs and reading and manipulating eBPF objects from user-space
Source0: http://www.example.org/Packages/libbpf/v1.0.1.tar.gz
URL: https://github.com/libbpf/libbpf
%description
Library for loading eBPF programs and reading and manipulating eBPF objects from user-space

%prep
%setup -q -n %{name}-%{version}

%build
%{make_build} -C src

%install
%{make_install} -C src LIBSUBDIR=lib install install_headers
install -Dm 644 README.md -t %{buildroot}/usr/share/doc/%{name}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE*

%changelog
*	Thu Oct 06 2022 scott andrews <scott-andrews@columbus.rr.com> 1.0.1-1
-	Initial build.	First version
