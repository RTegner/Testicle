Summary: Data parsing and validation using Python type hints
Name: python-pydantic
Version: 1.10.4
Release: 1
License: MIT
Group: Python
URL: https://github.com/pydantic/pydantic
#	Source
Source0: http://www.example.org/Packages/python-pydantic/v1.10.4.tar.gz
#	Requirements
Requires: python
Requires: python-cython
Requires: python-typing_extensions
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
#	depends=(cython python python-typing-extensions)
#	optdepends=( 'python-dotenv: for .env file support' 'python-email-validator: for email validation' )
#	makedepends=(python-build python-installer python-setuptools python-wheel)
#	checkdepends=(python-hypothesis python-pytest python-pytest-mock)
%description
Data parsing and validation using Python type hints

%prep
%setup -q -n pydantic-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip libraries
%strip_libs
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 1.10.4-1
-	Initial build.	First version
