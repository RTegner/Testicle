Summary: The ASN.1 library used in GNUTLS
Name: libtasn1
Version: 4.19.0
Release: 1
License: LGPL
Group: Core
URL: https://www.gnu.org/software/libtasn1/
#	Source
Source0: http://www.example.org/Packages/libtasn/libtasn1-4.19.0.tar.gz
%description
The ASN.1 library used in GNUTLS
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
	--disable-static)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
make -C doc/reference DESTDIR=%{buildroot} install-data-local
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.19.0-1
-	Initial build.	First version
