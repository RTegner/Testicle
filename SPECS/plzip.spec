Summary: Multi-threaded lossless file compressor based on the LZMA algorithm
Name: plzip
Version: 1.10
Release: 1
License: GPLv2
Group: Core
URL: https://www.nongnu.org/lzip/plzip.html
#	Source
Source0: http://www.example.org/Packages/plzip/plzip-1.10.tar.lz
%description
Multi-threaded lossless file compressor based on the LZMA algorithm
 
%prep
%setup -q -n %{name}-%{version}
 
%build
./configure --prefix=/usr
make
make check
 
%install
make DESTDIR=%{buildroot} install{,-man}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.10-1
-	Initial build.	First version
