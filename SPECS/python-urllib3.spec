Summary: HTTP library with thread-safe connection pooling and file post support
Name: python-urllib3
Version: 1.26.12
Release: 1
License: MIT
Group: Python
URL: https://github.com/urllib3/urllib3
#	Source
Source0: http://www.example.org/Packages/python-urllib3/urllib3-1.26.12.tar.gz
Requires: python
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
#	makedepends=('python-setuptools' 'python-sphinx' 'python-ndg-httpsclient'
#		'python-pyasn1' 'python-pyopenssl'
#		'python-pysocks' 'python-mock'
#		'python-brotli' 'python-sphinx-furo')
#	checkdepends=('python-pytest-runner' 'python-tornado' 'python-nose' 'python-psutil' 'python-trustme'
#		'python-gcp-devrel-py-tools' 'python-flaky' 'python-dateutil')

%description
HTTP library with thread-safe connection pooling and file post support

%prep
%setup -q -n urllib3-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/urllib3-%{version}.dist-info
%license LICENSE.txt

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 1.26.12-1
-	Initial build.	First version
