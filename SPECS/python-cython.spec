Summary: C-Extensions for Python
Name: python-cython
Version: 0.29.33
Release: 1
License: APACHE
Group: Python
URL: https://cython.org
#	Source
Source0: http://www.example.org/Packages/python-cython/0.29.33.tar.gz
#	Requirements
Requires: python
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
#	makedepends=(python-setuptools)
%description
C-Extensions for Python

%prep
%setup -q -n cython-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
for f in cygdb cython cythonize; do
	mv %{buildroot}/usr/bin/"${f}" %{buildroot}/usr/bin/"${f}"3
	ln -s "${f}"3 %{buildroot}/usr/bin/"${f}"
done
#	Strip libraries
%strip_libs
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 0.29.33-1
-	Update version
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 0.29.32-1
-	Initial build.	First version
