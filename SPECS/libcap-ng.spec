Summary: A library for Linux that makes using posix capabilities easy
Name: libcap-ng
Version: 0.8.3
Release: 1
License: LGPL2.1
Group: Core
URL: https://people.redhat.com/sgrubb/libcap-ng
#	Source
Source0: http://www.example.org/Packages/libcap-ng/v0.8.3.tar.gz
#	depends=(glibc)
#	makedepends=(python swig)
%description
A library for Linux that makes using posix capabilities easy

%prep
%setup -q -n %{name}-%{version}
autoreconf -fiv

%build
_options=(--prefix=/usr
    --enable-static=no
    --without-python
    --with-python3 )
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Jan 14 2023 scott andrews <scott-andrews@columbus.rr.com> 0.8.3-1
-	Core
