Summary: File type identification utility
Name: file
Version: 5.44
Release: 1
License: Other
Group: Core
URL: https://www.darwinsys.com/file
#	Source
Source0: http://www.example.org/Packages/file/file-5.44.tar.gz
%description
File type identification utility

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--datadir=/usr/share/file
	--enable-fsect-man5
	--enable-libseccomp)
./configure "${_options[@]}"
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Tue Jan 24 2024 scott andrews <scott-andrews@columbus.rr.com> 5.44-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 5.43-1
-	Initial build.	First version
