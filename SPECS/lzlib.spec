Summary: A library providing in-memory LZMA compression and decompression functions
Name: lzlib
Version: 1.13
Release: 1
License: BSD
Group: Core
URL: http://www.nongnu.org/lzip/lzlib.html
#	Source
Source0: http://www.example.org/Packages/lzlib/lzlib-1.13.tar.gz
%description
A library providing in-memory LZMA compression and decompression functions

%prep
%setup -q -n %{name}-%{version}

%build
./configure --prefix=/usr --enable-shared
make
make check

%install
make DESTDIR=%{buildroot} install
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.13-1
-	Initial build.	First version
