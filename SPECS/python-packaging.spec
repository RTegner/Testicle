Summary: Core utilities for Python packages
Name: python-packaging
#Version: 21.0
Version: 23.0
#	Version: 23.0
Release: 1
License: Apache
Group: Python
URL: https://github.com/pypa/packaging
#	Source
Source0: http://www.example.org/Packages/python-packaging/packaging-23.0.tar.gz
#	Source0: http://www.example.org/Packages/python-packaging/python-packaging-21.3.tar.lz
#	Source0: http://www.example.org/Packages/python-packaging/packaging-23.0.tar.gz
#Source0: http://www.example.org/Packages/python-packaging/21.0.tar.gz
%description
Core utilities for Python packages

%prep
%setup -q -n packaging-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps $PWD

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/packaging-%{version}.dist-info

%changelog
*	Thu Jan 26 2023 scott andrews <scott-andrews@columbus.rr.com> 23.0-1
-	Update version
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 22.0-1
-	Update version
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 21.3-1
-	Initial build.	First version
