Summary: Multi-platform support library with a focus on asynchronous I/O
Name: libuv
Version: 1.44.2
Release: 1
License: Other
Group: Core
URL: https://github.com/libuv/libuv
#	Source
Source0: http://www.example.org/Packages/libuv/libuv-v1.44.2.tar.gz
%description
Multi-platform support library with a focus on asynchronous I/O

%prep
%setup -q -n %{name}-v%{version}    # Disable tests that fail on build.a.o (but pass locally)
sed -e '/udp_multicast_join/d' -i test/test-list.h
./autogen.sh

%build
_options=(--prefix=/usr --disable-static )
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.44.2-1
-	Initial build.	First version
