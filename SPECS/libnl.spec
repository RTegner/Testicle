Summary: Library for applications dealing with netlink sockets
Name: libnl
Version: 3.7.0
Release: 1
License: GPL
Group: Networking-Libraries
URL: https://github.com/thom311/libnl
#	Source
Source0: http://www.example.org/Packages/libnl/libnl-3.7.0.tar.gz
#	Build Requirements
BuildRequires: glibc
BuildRequires: pkg-config
%description
Library for applications dealing with netlink sockets
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
    --sysconfdir=/etc
    --sbindir=/usr/bin
    --disable-static)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.7.0-1
-	Initial build.	First version
