Summary: Transport Independent RPC library (SunRPC replacement)
Name: libtirpc
Version: 1.3.3
Release: 1
License: BSD
Group: Networking-Libraries
URL: http://git.linux-nfs.org/?p=steved/libtirpc.git;a=summary
#	Source
Source0: http://www.example.org/Packages/libtirpc/libtirpc-1.3.3.tar.bz2
%description
Transport Independent RPC library (SunRPC replacement)

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--disable-gssapi)
./configure "${_options[@]}"
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) /etc/netconfig
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.3.3-1
-	Initial build.	First version
