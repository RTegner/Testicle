Summary: Broadcomm firmware
Name: bootloader
Version: 1.20230106
Release: 1
License: Other
Group: Core
URL: None
#	Source
Source0: http://www.example.org/Packages/firmware/1.20230106.tar.gz
Source1: http://www.example.org/Packages/firmware/cmdline.txt
Source2: http://www.example.org/Packages/firmware/config.txt

%description
Broadcomm firmware

%prep
%setup -q -n firmware-%{version}

%build
true

%install
#install broadcomm firmware/boot loader
mkdir -vp %{buildroot}/boot
cp boot/{*.dat,*.bin,*.elf} %{buildroot}/boot/
install -Dm644 %{SOURCE1} -t %{buildroot}/boot/
install -Dm644 %{SOURCE2} -t %{buildroot}/boot/
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) /boot/cmdline.txt
%config(noreplace) /boot/config.txt
%license boot/LICENCE.broadcom

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.20230106-1
-	Initial build.	First version
