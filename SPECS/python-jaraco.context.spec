Summary: Context managers by jaraco
Name: python-jaraco.context
Version: 4.2.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/jaraco/jaraco.context
#	Source
Source0: http://www.example.org/Packages/python-jaraco-context/jaraco.context-4.2.0.tar.gz
#	Requirements
#Requires: python
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-setuptools-scm
#	depends=('python')
#	makedepends=('python-setuptools-scm' 'python-build' 'python-installer' 'python-wheel')
#	checkdepends=('python-pytest')

%description
Context managers by jaraco

%prep
%setup -q -n jaraco.context-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 4.2.0-1
-	Initial build.	First version
