Summary: Multiple-precision floating-point library
Name: mpfr
Version: 4.2.0
Release: 1
License: LGPLv3
Group: Core
URL: https://www.mpfr.org
#	Source
Source0: http://www.example.org/Packages/mpfr/mpfr-4.2.0.tar.xz
Source1: http://www.example.org/Packages/mpfr/patches.diff
%description
Multiple-precision floating-point library

%prep
%setup -q -n %{name}-%{version}
#	patch -p1 < %{_sourcedir}/patches.diff
#	autoreconf -fiv

%build
_options=(--prefix=/usr
	--enable-shared
	--enable-thread-safe)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.1.1.1-1
-	Initial build.	First version
