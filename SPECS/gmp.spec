Summary: A free library for arbitrary precision arithmetic
Name: gmp
Version: 6.2.1
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/gmp
#	Source
Source0: http://www.example.org/Packages/gmp/gmp-6.2.1.tar.xz
%description
A free library for arbitrary precision arithmetic
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
	--enable-cxx
	--build=${MACHTYPE}
	--enable-fat)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 6.2.1-1
-	Initial build.	First version
