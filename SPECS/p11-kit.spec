Summary: Loads and enumerates PKCS#11 modules
Name: p11-kit
Version: 0.24.1
Release: 1
License: BSD
Group: Core
URL: https://p11-glue.freedesktop.org
#	Source
Source0: http://www.example.org/Packages/p11-kit/p11-kit-0.24.1.tar.xz
#	Source1: sed.text
#	Build Requirements
BuildRequires: libffi
BuildRequires: libtasn1
BuildRequires: meson
BuildRequires: systemd
%description
Loads and enumerates PKCS#11 modules

%prep
%setup -q -n %{name}-%{version}
#	%_sed %{SOURCE1} -i trust/trust-extract-compat
sed '20,$ d' -i trust/trust-extract-compat
cat >> trust/trust-extract-compat << "EOF"
# Copy existing anchor modifications to /etc/ssl/local
/usr/libexec/make-ca/copy-trust-modifications

# Update trust stores
/usr/sbin/make-ca -rf
EOF
autoupdate
autoreconf -fiv

%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--libexecdir=/usr/lib
	--disable-rpath
	--enable-doc-html=no
	--with-trust-paths=/etc/pki/anchors)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
ln -sfv /usr/lib/p11-kit/trust-extract-compat %{buildroot}/usr/bin/update-ca-certificates
ln -sv ./pkcs11/p11-kit-trust.so %{buildroot}/usr/lib/libnssckbi.so
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.24.1-1
-	Initial build.	First version
