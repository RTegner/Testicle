Summary: A library to create a command-line program from a function
Name: python-autocommand
Version: 2.2.2
Release: 1
License: LGPL
Group: Python
URL: https://github.com/Lucretiel/autocommand
#	Source
Source0: http://www.example.org/Packages/python-autocommand/autocommand-2.2.2.tar.gz
Requires: python
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
#	depends=('python')
#	makedepends=('python-setuptools')
#	checkdepends=('python-pytest')
%description
A library to create a command-line program from a function

%prep
%setup -q -n autocommand-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 2.2.2-1
-	Initial build.	First version
