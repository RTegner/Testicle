Summary: Enhanced version of the Berkeley C shell
Name: tcsh
Version: 6.24.07
Release: 1
License: BSD
Group: Core
URL: https://www.tcsh.org/
#	Source
Source0: http://www.example.org/Packages/tcsh/tcsh-6.24.07.tar.gz
Source1: http://www.example.org/Packages/tcsh/csh.cshrc
Source2: http://www.example.org/Packages/tcsh/csh.login
Provides: /usr/bin/csh, /usr/bin/tcsh
%description
Enhanced version of the Berkeley C shell

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--bindir=/usr/bin)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install} mandir=/usr/share/man install install.man
#	shell configuration
install -vDm644 -t %{buildroot}/etc %{SOURCE1}
install -vDm644 -t %{buildroot}/etc %{SOURCE2}
mkdir -vp %{buildroot}/usr/bin
ln -s tcsh %{buildroot}/usr/bin/csh
ln -s tcsh.1 %{buildroot}/usr/share/man/man1/csh.1
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%post
printf "%s\n" "/bin/tcsh" >> /etc/shells
printf "%s\n" "/bin/csh" >> /etc/shells

%postun
if [ -r /etc/shells ];then
	/usr/bin/sed "/\/bin\/tcsh/d" -i /etc/shells
	/usr/bin/sed "/\/bin\/csh/d" -i /etc/shells
fi

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) /etc/csh.cshrc
%config(noreplace) /etc/csh.login
%license Copyright

%changelog
*	Thu Jan 12 2023 scott andrews <scott-andrews@columbus.rr.com> 6.24.07-1
-	Initial build.	First version
