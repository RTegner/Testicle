Summary: multi-threaded tar archiver lzip compressor
Name: tarlz
Version: 0.23
Release: 1
License: GPLv2
Group: Core
URL: https://www.nongnu.org/lzip/tarlz.html
#	Source
Source0: http://www.example.org/Packages/tarlz/tarlz-0.23.tar.lz
%description
Massively parallel (multi-threaded) combined implementation of the tar archiver and the lzip compressor
 
%prep
%setup -q -n %{name}-%{version}
 
%build
./configure --prefix=/usr
make
 
%install
make DESTDIR=%{buildroot} install{,-man}
%strip_libs
%strip_binaries
%rm_info_dir
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.23-1
-	Initial build.	First version
