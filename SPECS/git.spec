%define __requires_exclude perl\\((CGI|DBI|CGI::Carp|CGI::Util|SVN::Core|SVN::Delta|SVN::Ra|YAML::Any))
Summary: the fast distributed version control system
Name: git
Version: 2.39.1
Release: 1
License: GPLv2
Group: Core
URL: https://github.com/git/git
#	Source
Source0: http://www.example.org/Packages/git/git-2.39.1.tar.xz
Source1: http://www.example.org/Packages/git/git-daemon@.service
Source2: http://www.example.org/Packages/git/git-daemon.socket
Source3: http://www.example.org/Packages/git/git-sysusers.conf
#	Build Requirements
BuildRequires: python
#BuildRequires: libgnome-keyring
#BuildRequires: xmlto
#BuildRequires: asciidoc
#optdepends=('tk: gitk and git gui'
#	'perl-libwww: git svn'
#	'perl-term-readkey: git svn and interactive.singlekey setting'
#	'perl-io-socket-ssl: git send-email TLS support'
#	'perl-authen-sasl: git send-email TLS support'
#	'perl-mediawiki-api: git mediawiki support'
#	'perl-datetime-format-iso8601: git mediawiki support'
#	'perl-lwp-protocol-https: git mediawiki https support'
#	'perl-cgi: gitweb (web interface) support'
#	'python: git svn & git p4'
#	'subversion: git svn'
#	'org.freedesktop.secrets: keyring credential helper'
#	'libsecret: libsecret credential helper')
%description
the fast distributed version control system

%prep
%setup -q -n %{name}-%{version}

%build
_options=( prefix='/usr'
	gitexecdir='/usr/lib/git-core'
	perllibdir="$(/usr/bin/perl -MConfig -wle 'print $Config{installvendorlib}')"
	INSTALL_SYMLINKS=1
	MAN_BOLD_LITERAL=1
	NO_PERL_CPAN_FALLBACKS=1
	USE_LIBPCRE2=1)
/usr/bin/make -O -j4 V=1 VERBOSE=1 "${_options[@]}" all
###	FIXME	man


%install
_options=( prefix='/usr'
	gitexecdir='/usr/lib/git-core'
	perllibdir="$(/usr/bin/perl -MConfig -wle 'print $Config{installvendorlib}')"
	INSTALL_SYMLINKS=1
	MAN_BOLD_LITERAL=1
	NO_PERL_CPAN_FALLBACKS=1
	USE_LIBPCRE2=1)
make "${_options[@]}" DESTDIR=%{buildroot} install
###	FIXME	install-man
#	bash completion
mkdir -vp %{buildroot}/usr/share/bash-completion/completions
install -m 0644 ./contrib/completion/git-completion.bash %{buildroot}/usr/share/bash-completion/completions/git
#	fancy git prompt
#	mkdir -p %{buildroot}/usr/share/git
#	install -m 0644 ./contrib/completion/git-prompt.sh %{buildroot}/usr/share/git/git-prompt.sh
#	gnome credentials helper (deprecated, but we will keep it as long there is no extra cost)
#	https://gitlab.gnome.org/GNOME/libgnome-keyring/commit/6a5adea4aec93
#	install -m 0755 contrib/credential/gnome-keyring/git-credential-gnome-keyring %{buildroot}/usr/lib/git-core/git-credential-gnome-keyring
#	make -C contrib/credential/gnome-keyring clean
#	libsecret credentials helper
#	install -m 0755 contrib/credential/libsecret/git-credential-libsecret %{buildroot}/usr/lib/git-core/git-credential-libsecret
#	make -C contrib/credential/libsecret clean
#	subtree installation
#	make -C contrib/subtree  DESTDIR=%{buildroot} install install-man
#	mediawiki installation
#	make -C contrib/mw-to-git  DESTDIR=%{buildroot} install
#	the rest of the contrib stuff
#	find contrib/ -name '.gitignore' -delete
#	cp -a ./contrib/* %{buildroot}/usr/share/git/
#	git-daemon via systemd socket activation
#	install -D -m 0644 %{SOURCE1} %{buildroot}/usr/lib/systemd/system/git-daemon@.service
#	install -D -m 0644 %{SOURCE2} %{buildroot}/usr/lib/systemd/system/git-daemon.socket
#	sysusers file
install -D -m 0644 %{SOURCE3} %{buildroot}/usr/lib/sysusers.d/git.conf
touch %{buildroot}/usr/share/git-core/templates/branches/.hidden
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
%license LGPL-2.1

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 2.39.1-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.38.1-1
-	Initial build.	First version
