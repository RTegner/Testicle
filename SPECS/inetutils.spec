Summary: A collection of common network programs
Name: inetutils
Version: 2.3
Release: 1
License: GPLv3
Group: Core
URL: http://www.gnu.org/software/inetutils
#	Source
Source0: http://www.example.org/Packages/inetutils/inetutils-2.3.tar.xz
#	Build Requirements
BuildRequires: help2man
%description
A collection of common network programs
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(
	--prefix=/usr
	--libexec=/usr/bin
	--localstatedir=/var
	--sysconfdir=/etc
	--without-wrap
#	--with-pam
#	--enable-ftp
#	--enable-ftpd
	--enable-telnet
#	--enable-talk
#	--enable-talkd
#	--enable-rlogin
#	--enable-rlogind
#	--enable-rsh
#	--enable-rshd
#	--enable-rcp
	--enable-hostname
	--enable-dnsdomainname
	--disable-rexec
	--disable-rexecd
	--disable-tftp
	--disable-tftpd
#	--disable-ping
#	--disable-ping6
	--disable-logger
	--disable-syslogd
	--disable-inetd
#	--disable-whois
	--disable-uucpd
	--disable-ifconfig
#	--disable-traceroute
	--disable-servers)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
rmdir %{buildroot}/usr/share/man/man8
#	rmdir %{buildroot}/usr/libexec
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%attr(6755,root,root) /usr/bin/ping
%attr(6755,root,root) /usr/bin/ping6
%attr(6755,root,root) /usr/bin/traceroute
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.3-1
-	Initial build.	First version
