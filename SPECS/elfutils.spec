Summary: Utilities to handle ELF object files and DWARF debugging information
Name: elfutils
Version: 0.188
Release: 1
License: GPLv3
Group: Core
URL: https://sourceware.org/ftp/elfutils
#	Source
Source0: http://www.example.org/Packages/elfutils/elfutils-0.188.tar.bz2
#	Build Requirements
BuildRequires: bzip2
BuildRequires: curl
BuildRequires: gcc
BuildRequires: libarchive
BuildRequires: sqlite
BuildRequires: xz
BuildRequires: zlib
BuildRequires: zstd
%description
Utilities to handle ELF object files and DWARF debugging information
 
%prep
%setup -q -n elfutils-%{version}
# remove failing test due to missing glibc debug package
sed -e 's/run-backtrace-native.sh//g' -i tests/Makefile.am
autoreconf -fiv
 
%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--program-prefix=eu-
	--enable-deterministic-archives
	--disable-debuginfod
	--disable-libdebuginfod)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 0.188-1
-	Initial build.	First version
