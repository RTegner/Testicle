Summary: System V Release 4.0 curses emulation library
Name: ncurses
Version: 6.4
Release: 1
License: MIT
Group: Core
URL: http://www.gnu.org/software/ncurses
#	Source
Source0: http://www.example.org/Packages/ncurses/ncurses-6.4.tar.gz
Source1: http://www.example.org/Packages/ncurses/ncurses-6.3-libs.patch
Source2: http://www.example.org/Packages/ncurses/ncurses-6.3-pkgconfig.patch
%description
System V Release 4.0 curses emulation library

%prep
%setup -q -n %{name}-%{version}
#	do not link against test libraries
patch -Np1 -d ../%{name}-%{version} -i %{SOURCE1}
#	do not leak build-time LDFLAGS into the pkgconfig files:
#	https://bugs.archlinux.org/task/68523
patch -Np1 -d ../%{name}-%{version} -i %{SOURCE2}
#	NOTE: can not run autoreconf because the autotools setup is custom and ancient

%build
_options=(--prefix=/usr
	--enable-widec
	--enable-pc-files
	--mandir=/usr/share/man
	--with-cxx-binding
	--with-cxx-shared
	--with-manpage-format=normal
	--with-pkg-config-libdir=/usr/lib/pkgconfig
	--with-shared
	--with-versioned-syms
	--with-xterm-kbs=del
	--without-ada)
#	--without-debug
#	--without-normal)
./configure "${_options[@]}"
%{make_build}

%install

%{make_install}
#	fool packages looking to link to non-wide-character ncurses libraries
for lib in ncurses ncurses++ form panel menu; do
	printf "INPUT(-l%sw)\n" "${lib}" > %{buildroot}/usr/lib/lib${lib}.so
	ln -sv ${lib}w.pc %{buildroot}/usr/lib/pkgconfig/${lib}.pc
done
#	some packages look for -lcurses during build
printf "INPUT(-lncursesw)\n" > %{buildroot}/usr/lib/libcursesw.so
ln -sv libncurses.so %{buildroot}/usr/lib/libcurses.so
#	tic and ticinfo functionality is built in by default
#	make sure that anything linking against it links against libncursesw.so instead
for lib in tic tinfo; do
	printf "INPUT(libncursesw.so.%s)\n" "6" > %{buildroot}/usr/lib/lib${lib}.so
	ln -sv libncursesw.so.6 %{buildroot}/usr/lib/lib${lib}.so.6
	ln -sv ncursesw.pc %{buildroot}/usr/lib/pkgconfig/${lib}.pc
done
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 6.4-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 6.3-1
-	Initial build.	First version
