Summary: PHP scripting language for creating dynamic web sites
Name: php
Version: 8.2.0
Release: 1
License: PHP
Group: Web
URL: https://www.php.net/
#	Source
Source0: http://www.example.org/Packages/php/php-8.2.0.tar.gz
Source1: http://www.example.org/Packages/php/php-fpm.patch
Source2: http://www.example.org/Packages/php/php.ini.patch
Source3: http://www.example.org/Packages/php/php-fpm.tmpfiles
Provides: /usr/bin/php
%description
PHP is an HTML-embedded scripting language. PHP attempts to make it
easy for developers to write dynamically generated web pages. PHP also
offers built-in database integration for several commercial and
non-commercial database management systems, so writing a
database-enabled webpage with PHP is fairly simple. The most common
use of PHP coding is probably as a replacement for CGI scripts.

%prep
%setup -q -n %{name}-%{version}
patch -p0 -i %{SOURCE1}
patch -p0 -i %{SOURCE2}
autoconf
# Disable failing tests
rm tests/output/stream_isatty_*.phpt

%build
_options=( --prefix=/usr
	--sbindir=/usr/bin
	--sysconfdir=/etc/php
	--localstatedir=/var
	--mandir=/usr/share/man
	--config-cache)

_features=( --disable-rpath
	--disable-gcc-global-regs )

_sapi_modules=(--enable-cgi
	--enable-embed=shared
	--enable-fpm
	--with-fpm-acl
	--with-fpm-group=nobody
	--with-fpm-systemd
	--with-fpm-user=nobody )

_general=(--enable-libgcc
	--with-config-file-path=/etc/php
	--with-config-file-scan-dir=/etc/php/conf.d
	--with-layout=GNU )

_extensions=(--disable-mbregex
	--enable-bcmath=shared
	--enable-calendar=shared
	--enable-dba=shared
	--enable-ftp=shared
	--enable-mbstring
	--enable-sockets=shared
	--enable-sysvmsg=shared
	--enable-sysvsem=shared
	--enable-sysvshm=shared
	--with-bz2=shared
	--with-curl=shared
	--with-ffi=shared
	--with-gdbm=shared
	--with-gettext=shared
	--with-gmp=shared
	--with-iconv=shared
	--with-openssl
	--with-pdo-sqlite=shared
	--with-sqlite3=shared
	--with-zlib )

#	remove --disable-mbregex after oniguruma is built and installed
#	--enable-gd=shared
#	--with-webp=shared
#	--with-jpeg=shared
#	--with-xpm=shared
#	--with-freetype=shared )
EXTENSION_DIR=/usr/lib/php/modules
export EXTENSION_DIR
./configure "${_options[@]}" "${_features[@]}" "${_sapi_modules[@]}" "${_general[@]}" "${_extensions[@]}"
%{make_build}

%install
/usr/bin/make INSTALL_ROOT="%{buildroot}" install
install -D -m644 php.ini-production "%{buildroot}/etc/php/php.ini"
#	install -d -m755 "%{buildroot}/etc/php/conf.d/"
rmdir "%{buildroot}/usr/include/php/include"
install -D -m644 sapi/fpm/php-fpm.service "%{buildroot}/usr/lib/systemd/system/php-fpm.service"
install -D -m644 %{SOURCE3} "%{buildroot}/usr/lib/tmpfiles.d/php-fpm.conf"
rm -r %{buildroot}/var
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%config(noreplace) /etc/php/php.ini
%config(noreplace) /etc/php/php-fpm.conf
%config(noreplace) /etc/php/php-fpm.d/www.conf

%changelog
*	Sun Jan 01 2023 scott andrews <scott-andrews@columbus.rr.com> 8.2.0-1
-	Initial build.	First version
