Summary: The XML::Parser module is a Perl interface to James Clark XML parser, Expat
Name: perl-xml-parser
Version: 2.46
Release: 1
License: Non-GPL
Group: Core
URL: https://github.com/chorny/XML-Parser
#	Source
Source0: http://www.example.org/Packages/perl-xml-parser/XML-Parser-2.46.tar.gz
%description
The XML::Parser module is a Perl interface to James Clark XML parser, Expat
 
%prep
%setup -q -n XML-Parser-%{version}
 
%build
perl Makefile.PL INSTALLDIRS=vendor
%{make_build}
 
%install
%{make_install}
find %{buildroot} -name perllocal.pod -delete
find %{buildroot} -name .packlist -delete
rm -r %{buildroot}/usr/lib/perl5/5.36/core_perl
#	%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.46-1
-	Initial build.	First version
