Summary: MS-DOS filesystem utilities
Name: dosfstools
Version: 4.2
Release: 1
License: GPL2
Group: Core
URL: https://github.com/dosfstools/dosfstools
#	Source
Source0: http://www.example.org/Packages/dosfstools/dosfstools-4.2.tar.gz
%description
MS-DOS filesystem utilities
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
	--libexecdir=/usr/lib
	--sbindir=/usr/bin
	--mandir=/usr/share/man
	--docdir=/usr/share/doc/%{name}
	--enable-compat-symlinks)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 4.2-1
-	Initial build.	First version
