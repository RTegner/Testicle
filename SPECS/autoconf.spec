Summary: A GNU tool for automatically configuring source code
Name: autoconf
Version: 2.71
Release: 1
License: GPLv2
Group: Core
URL: http://www.gnu.org/software/autoconf
#	Source
Source0: http://www.example.org/Packages/autoconf/autoconf-2.71.tar.xz
%description
A GNU tool for automatically configuring source code
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.71-1
-	Initial build.	First version
