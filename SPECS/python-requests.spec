Summary: Python HTTP for Humans
Name: python-requests
Version: 2.28.1
Release: 1
License: Apache
Group: Python
URL: http://python-requests.org
#	Source
Source0: http://www.example.org/Packages/python-requests/v2.28.1.tar.gz
Source1: http://www.example.org/Packages/python-requests/certs.patch
Requires: python
Requires: python-urllib3
Requires: python-chardet
Requires: python-idna
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-chardet
BuildRequires: python-urllib3
BuildRequires: python-idna
#	depends=('python-urllib3' 'python-chardet' 'python-idna')
#	makedepends=('python-setuptools' 'python-chardet' 'python-urllib3' 'python-idna')
#	checkdepends=('python-pytest-httpbin' 'python-pytest-mock' 'python-pysocks' 'python-trustme')

%description
Python HTTP for Humans

%prep
%setup -q -n requests-%{version}
#	Stay with chardet for now: https://github.com/psf/requests/issues/5871
sed -e '/certifi/d' -e "s/,<.*'/'/" -e '/charset_normalizer/d' -i setup.py
patch -p1 -i %{_sourcedir}/certs.patch

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/requests-%{version}.dist-info

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 2.28.1-1
-	Initial build.	First version
