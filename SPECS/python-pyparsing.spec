Summary: General parsing module for Python
Name: python-pyparsing
Version: 3.0.9
Release: 1
License: MIT
Group: Python
URL: https://github.com/pyparsing/pyparsing
#	Source
Source0: http://www.example.org/Packages/python-pyparsing/pyparsing_3.0.9.tar.gz
Requires: python
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-flit
#	depends=('python')
#	makedepends=('python-build' 'python-installer' 'python-flit-core')
#	checkdepends=('python-jinja' 'python-railroad-diagrams')
#	optdepends=('python-railroad-diagrams: for generating Railroad Diagrams'
#		'python-jinja: for generating Railroad Diagrams')

%description
General parsing module for Python

%prep
%setup -q -n pyparsing-pyparsing_%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/pyparsing-%{version}.dist-info
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 3.0.9-1
-	Initial build.	First version
