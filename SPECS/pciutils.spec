Summary: PCI bus configuration space access library and tools
Name: pciutils
Version: 3.9.0
Release: 1
License: GPL2
Group: Base
URL: https://mj.ucw.cz/sw/pciutils
#	Source
Source0: http://www.example.org/Packages/pciutils/pciutils-3.9.0.tar.gz
%description
PCI bus configuration space access library and tools
 
%prep
%setup -q -n %{name}-%{version}
 
%build
make ZLIB=no SHARED=no PREFIX=/usr SHAREDIR=/usr/share/hwdata MANDIR=/usr/share/man SBINDIR=/usr/bin lib/libpci.a
cp lib/libpci.a %{_builddir}/
make clean
make ZLIB=no SHARED=yes PREFIX=/usr SBINDIR=/usr/bin SHAREDIR=/usr/share/hwdata MANDIR=/usr/share/man all
 
%install

make SHARED=yes PREFIX=/usr SBINDIR=/usr/bin SHAREDIR=/usr/share/hwdata MANDIR=/usr/share/man DESTDIR=%{buildroot} install install-lib
rm -rf %{buildroot}/usr/share/hwdata
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 3.9.0-1
-	Initial build.	First version
