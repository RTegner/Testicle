Summary: Conversion tool to create man files
Name: help2man
Version: 1.49.3
Release: 1
License: GPL
Group: Core
URL: https://www.gnu.org/software/help2man/
#	Source
Source0: http://www.example.org/Packages/help2man/help2man-1.49.3.tar.xz
%description
Conversion tool to create man files

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--mandir=/usr/share/man
	--infodir=/usr/share/info
	--libdir=/usr/lib)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Tue Jan 24 2024 scott andrews <scott-andrews@columbus.rr.com> 1.49.3-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.49.2-1
-	Initial build.	First version
