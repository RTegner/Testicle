Summary: Handles managing your python package versions in scm metadata
Name: python-setuptools-scm
Version: 7.1.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/pypa/setuptools_scm
#	Source
Source0: http://www.example.org/Packages/python-setuptools-scm/setuptools_scm-7.1.0.tar.gz
#Requires: python
#Requires: python-packaging
#Requires: python-setuptools
#Requires: python-tomli
#Requires: python-typing_extensions
#	Build Requirements
#BuildRequires: python
#BuildRequires: python-build
#BuildRequires: python-installer
#BuildRequires: python-pep517
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-flit
#BuildRequires: python-packaging
#BuildRequires: python-tomli
#	depends=('python-packaging' 'python-setuptools' 'python-tomli'
#		'python-typing_extensions')
#	makedepends=('python-build' 'python-installer' 'python-wheel')
#	checkdepends=('git' 'mercurial' 'python-pip' 'python-pytest'
#		'python-virtualenv')

%description
Handles managing your python package versions in scm metadata

%prep
%setup -q -n setuptools_scm-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 7.1.0-1
-	Initial build.	First version
