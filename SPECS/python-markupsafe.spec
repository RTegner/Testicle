Summary: MarkupSafe is a Python module that implements a XML/HTML/XHTML Markup safe string.
Name: python-markupsafe
Version: 2.1.1
Release: 1
License: Unknown
Group: Python
URL: Unknown
#	Source
Source0: http://www.example.org/Packages/python-markupsafe/python-markupsafe-2.1.1.tar.gz
%description
MarkupSafe is a Python module that implements a XML/HTML/XHTML Markup safe string.

%prep
%setup -q -n python-markupsafe-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir=%{buildroot} dist/*.whl
#	Strip libraries
%strip_libs
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/MarkupSafe-%{version}.dist-info
%license LICENSE.rst

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 2.1.1-1
-	Initial build.	First version
