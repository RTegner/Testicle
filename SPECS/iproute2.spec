Summary: IP Routing Utilities
Name: iproute2
Version: 6.1.0
Release: 1
License: GPLv2
Group: Core
URL: https://www.kernel.org/pub/linux/utils/net/iproute2
#	Source
Source0: http://www.example.org/Packages/iproute2/iproute2-6.1.0.tar.xz
Source1: http://www.example.org/Packages/iproute2/0001-make-iproute2-fhs-compliant.patch
%description
IP Routing Utilities

%prep
%setup -q -n %{name}-%{version}
#	set correct fhs structure
patch -Np1 -i %{SOURCE1}
#	do not treat warnings as errors
sed -i 's/-Werror//' Makefile

%build
./configure
%{make_build}

%install
%{make_install} SBINDIR=/usr/bin
#	libnetlink is not installed, install it FS#19385
install -Dm0644 include/libnetlink.h %{buildroot}/usr/include/libnetlink.h
install -Dm0644 lib/libnetlink.a %{buildroot}/usr/lib/libnetlink.a
touch %{buildroot}/var/lib/arpd/.hidden
touch %{buildroot}/usr/lib/tc/.hidden
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Tue Jan 24 2024 scott andrews <scott-andrews@columbus.rr.com> 6.1.0-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 6.0.0-1
-	Initial build.	First version
