Summary: RPi Foundation header and scripts for building modules for Linux kernel
Name: linux-api-headers
Version: 1.20230106
Release: 1
License: GPL
Group: Core
URL: http://www.kernel.org
#	Source
Source0: http://www.example.org/Packages/linux/1.20230106.tar.gz
%description
RPi Foundation header and scripts for building modules for Linux kernel

%prep
%setup -q -n linux-%{version}

%build
make mrproper

%install
make INSTALL_HDR_PATH=%{buildroot}/usr headers_install
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Fri Jan 20 2023 scott andrews <scott-andrews@columbus.rr.com> 1.20230106-1
-	Initial build.	First version
