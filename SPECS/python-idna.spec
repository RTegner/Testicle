Summary: Internationalized Domain Names in Applications (IDNA)
Name: python-idna
Version: 3.4
Release: 1
License: BSD
Group: Python
URL: https://github.com/kjd/idna
#	Source
Source0: http://www.example.org/Packages/python-idna/v3.4.tar.gz
#	Requirements
Requires: python
#	Build Requirements
BuildRequires: python
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-pep517
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-flit
#	depends=('python')
#	makedepends=('python-build' 'python-installer' 'python-flit-core')
#	checkdepends=('python-pytest')

%description
Internationalized Domain Names in Applications (IDNA)

%prep
%setup -q -n idna-%{version}

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/idna-%{version}.dist-info
%license LICENSE.md

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 3.4-1
-	Initial build.	First version
