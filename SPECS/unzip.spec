Summary: For extracting and viewing files in .zip archives
Name: unzip
Version: 6.0
Release: 1
License: Other
Group: Core
URL: https://www.info-zip.org/UnZip.html
#	Source
Source0: http://www.example.org/Packages/unzip/unzip60.tar.gz
Source1: http://www.example.org/Packages/unzip/unzip-6.0-exec-shield.patch
Source2: http://www.example.org/Packages/unzip/unzip-6.0-close.patch
Source3: http://www.example.org/Packages/unzip/unzip-6.0-attribs-overflow.patch
Source4: http://www.example.org/Packages/unzip/unzip-6.0-symlink.patch
Source5: http://www.example.org/Packages/unzip/unzip-6.0-format-secure.patch
Source6: http://www.example.org/Packages/unzip/unzip-6.0-valgrind.patch
Source7: http://www.example.org/Packages/unzip/unzip-6.0-x-option.patch
Source8: http://www.example.org/Packages/unzip/unzip-6.0-overflow.patch
Source9: http://www.example.org/Packages/unzip/unzip-6.0-cve-2014-8139.patch
Source10: http://www.example.org/Packages/unzip/unzip-6.0-cve-2014-8140.patch
Source11: http://www.example.org/Packages/unzip/unzip-6.0-cve-2014-8141.patch
Source12: http://www.example.org/Packages/unzip/unzip-6.0-overflow-long-fsize.patch
Source13: http://www.example.org/Packages/unzip/unzip-6.0-heap-overflow-infloop.patch
Source14: http://www.example.org/Packages/unzip/unzip-6.0-alt-iconv-utf8.patch
Source15: http://www.example.org/Packages/unzip/unzip-6.0-alt-iconv-utf8-print.patch
Source16: http://www.example.org/Packages/unzip/0001-Fix-CVE-2016-9844-rhbz-1404283.patch
Source17: http://www.example.org/Packages/unzip/unzip-6.0-timestamp.patch
Source18: http://www.example.org/Packages/unzip/unzip-6.0-cve-2018-1000035-heap-based-overflow.patch
Source19: http://www.example.org/Packages/unzip/unzip-6.0-cve-2018-18384.patch
Source20: http://www.example.org/Packages/unzip/unzip-6.0-COVSCAN-fix-unterminated-string.patch
Source21: http://www.example.org/Packages/unzip/unzip-zipbomb-part1.patch
Source22: http://www.example.org/Packages/unzip/unzip-zipbomb-part2.patch
Source23: http://www.example.org/Packages/unzip/unzip-zipbomb-part3.patch
Source24: http://www.example.org/Packages/unzip/unzip-zipbomb-manpage.patch
Source25: http://www.example.org/Packages/unzip/unzip-zipbomb-part4.patch
Source26: http://www.example.org/Packages/unzip/unzip-zipbomb-part5.patch
Source27: http://www.example.org/Packages/unzip/unzip-zipbomb-part6.patch
Source28: http://www.example.org/Packages/unzip/unzip-zipbomb-switch.patch
Source29: http://www.example.org/Packages/unzip/28-cve-2022-0529-and-cve-2022-0530.patch
Source30: http://www.example.org/Packages/unzip/unzip-6.0_CVE-2021-4217.patch
%description
For extracting and viewing files in .zip archives
 
%prep
%setup -q -n %{name}60
sed -i "/MANDIR =/s#)/#)/share/#" unix/Makefile
patch -p1 -i %{_sourcedir}/unzip-6.0-exec-shield.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-close.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-attribs-overflow.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-symlink.patch # FS#60433
patch -p1 -i %{_sourcedir}/unzip-6.0-format-secure.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-valgrind.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-x-option.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-overflow.patch # FS#44171
patch -p1 -i %{_sourcedir}/unzip-6.0-cve-2014-8139.patch # FS#43300
patch -p1 -i %{_sourcedir}/unzip-6.0-cve-2014-8140.patch # FS#43391
patch -p1 -i %{_sourcedir}/unzip-6.0-cve-2014-8141.patch # FS#43300
patch -p1 -i %{_sourcedir}/unzip-6.0-overflow-long-fsize.patch # FS#44171
patch -p1 -i %{_sourcedir}/unzip-6.0-heap-overflow-infloop.patch # FS#46955
patch -p1 -i %{_sourcedir}/unzip-6.0-alt-iconv-utf8.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-alt-iconv-utf8-print.patch
patch -p1 -i %{_sourcedir}/0001-Fix-CVE-2016-9844-rhbz-1404283.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-timestamp.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-cve-2018-1000035-heap-based-overflow.patch # FS#69739
patch -p1 -i %{_sourcedir}/unzip-6.0-cve-2018-18384.patch
patch -p1 -i %{_sourcedir}/unzip-6.0-COVSCAN-fix-unterminated-string.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-part1.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-part2.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-part3.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-manpage.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-part4.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-part5.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-part6.patch
patch -p1 -i %{_sourcedir}/unzip-zipbomb-switch.patch
patch -p1 -i %{_sourcedir}/unzip-6.0_CVE-2021-4217.patch
patch -p1 -F3 -i %{_sourcedir}/28-cve-2022-0529-and-cve-2022-0530.patch
 
%build
# DEFINES, make, and install args from Debian
DEFINES=(-DACORN_FTYPE_NFS
	-DWILD_STOP_AT_DIR
	-DLARGE_FILE_SUPPORT
	-DUNICODE_SUPPORT
	-DUNICODE_WCHAR
	-DUTF8_MAYBE_NATIVE
	-DNO_LCHMOD
	-DDATE_FORMAT=DF_YMD
	-DUSE_BZIP2
	-DNOMEMCPY
	-DNO_WORKING_ISPRINT)
make -f unix/Makefile prefix=/usr \
	D_USE_BZ2=-DUSE_BZIP2 L_BZ2=-lbz2 \
	CF="$CFLAGS $CPPFLAGS -I. ${DEFINES[*]}" \
	unzips
 
%install
make -f unix/Makefile prefix=%{buildroot}/usr install
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license LICENSE
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 6.0-1
-	Initial build.	First version
