Summary: A utility for reading man pages
Name: man-db
Version: 2.11.2
Release: 1
License: GPL
Group: Core
URL: https://www.nongnu.org/man-db
#	Source
Source0: http://www.example.org/Packages/man-db/man-db-2.11.2.tar.xz
Source1: http://www.example.org/Packages/man-db/convert-mans
%description
A utility for reading man pages

%prep
%setup -q -n man-db-%{version}

%build
_options=(--prefix=/usr
	--sbindir=/usr/bin
	--sysconfdir=/etc
	--libexecdir=/usr/lib
	--with-systemdsystemunitdir=/usr/lib/systemd/system
	--with-snapdir=/var/lib/snapd/snap
	--with-db=gdbm
	--disable-setuid
	--enable-cache-owner=root
	--enable-mandirs=GNU
	--with-sections="1 1p n l 8 3 3p 0 0p 2 5 4 9 6 7")
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
#	part of groff pkg
rm -f %{buildroot}/usr/bin/zsoelim
#	script from LFS to convert manpages, see
#	http://www.linuxfromscratch.org/lfs/view/6.4/chapter06/man-db.html
install -D -m755 %{SOURCE1} %{buildroot}/usr/bin/convert-mans
install -d -m755 %{buildroot}/usr/lib/systemd/system/timers.target.wants
ln -s ../man-db.timer %{buildroot}/usr/lib/systemd/system/timers.target.wants/man-db.timer
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 2.11.2-1
-	Update version
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 2.11.1-1
-	Initial build.	First version
