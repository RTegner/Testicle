Summary: The PyPA recommended tool for installing Python packages
Name: python-pip
Version: 23.0
Release: 1
License: MIT
Group: Core
URL: https://pip.pypa.io
#	Source
#Source0: http://www.example.org/Packages/python-pip/pip-22.3.1.tar.gz
Source0: http://www.example.org/Packages/python-pip/pip-23.0.tar.gz
%description
The PyPA recommended tool for installing Python packages

%prep
%setup -q -n pip-%{version}

%build
#	python -m build --wheel --no-isolation
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/pip-%{version}.dist-info
%license LICENSE.txt

%changelog
*	Thu Feb 16 2023 scott andrews <scott-andrews@columbus.rr.com> 23.0-1
-	Upodate version
*	Sun Nov 13 2022 scott andrews <scott-andrews@columbus.rr.com> 22.3-1
-	Initial build.	First version
