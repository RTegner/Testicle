Summary: Canonical source for classifiers on PyPI (pypi.org)
Name: python-trove-classifiers
Version: 2023.1.20
Release: 1
License: Apache
Group: Python
URL: https://github.com/pypa/trove-classifiers
#	Source
Source0: http://www.example.org/Packages/python-trove-classifiers/2023.1.20.tar.gz
#	Requirements
Requires: python
#	Build Requirements
BuildRequires: python-build
BuildRequires: python-installer
BuildRequires: python-setuptools
BuildRequires: python-wheel
BuildRequires: python-calver
%description
Canonical source for classifiers on PyPI (pypi.org)

%prep
%setup -q -n trove-classifiers-%{version}
echo "Version: %{version}" > PKG-INFO

%build
python -m build --wheel --no-isolation

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 2023.1.20-1
-	Update version
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 2022.12.22-1
-	Initial build.	First version
