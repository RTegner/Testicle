Summary: Simplified packaging of Python modules
Name: python-flit
Version: 3.8.0
Release: 1
License: BSD
Group: Python
URL: https://github.com/takluyver/flit
#	Source
Source0: http://www.example.org/Packages/python-flit/flit-3.8.0.tar.gz
%description
Simplified packaging of Python modules

%prep
%setup -q -n flit-%{version}
rm tests/test_sdist.py
cd flit_core
python build_dists.py

%build
#PYTHONPATH=flit_core python -m flit build --format wheel
export PYTHONPATH=flit_core
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
PIP_CONFIG_FILE=/dev/null pip3 install --isolated --root="%{buildroot}" --no-warn-script-location --ignore-installed --no-deps dist/*.whl
cd flit_core
PIP_CONFIG_FILE=/dev/null pip3 install --isolated --root="%{buildroot}" --no-warn-script-location --ignore-installed --no-deps dist/*.whl
#	Strip and remove la files
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%dir /usr/lib/python3.??/site-packages/flit-%{version}.dist-info
%dir /usr/lib/python3.??/site-packages/flit_core-%{version}.dist-info
%license LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 3.8.0-1
-	Initial build.	First version
