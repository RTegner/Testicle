Summary: A terminal based program for viewing text files
Name: less
Version: 608
Release: 1
License: Other
Group: Core
URL: http://www.greenwoodsoftware.com/less
#	Source
Source0: http://www.example.org/Packages/less/less-608.tar.gz
%description
A terminal based program for viewing text files
 
%prep
%setup -q -n %{name}-%{version}
 
%build
_options=(--prefix=/usr
	--sysconfdir=/etc
	--with-regex=pcre2)
sh configure "${_options[@]}"
%{make_build}
 
%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm
 
%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
 
%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 608-1
-	Initial build.	First version
