Summary: A MutableSet that remembers its order, so that every entry has an index
Name: python-ordered-set
Version: 4.1.0
Release: 1
License: MIT
Group: Python
URL: https://github.com/rspeer/ordered-set
#	Source
Source0: http://www.example.org/Packages/python-ordered-set/ordered-set-4.1.0.tar.gz
#	Requirements
Requires: python
#	Build Requirements
BuildRequires: python
#BuildRequires: python-installer
#BuildRequires: python-setuptools
#BuildRequires: python-wheel
#BuildRequires: python-flit
#	depends=('python')
#	makedepends=('python-build' 'python-flit-core' 'python-installer')
#	checkdepends=('python-pytest')

%description
A MutableSet that remembers its order, so that every entry has an index

%prep
%setup -q -n ordered-set-%{version}

%build
python -m pip wheel -w dist --no-build-isolation --no-deps ${PWD}

%install
python -m installer --destdir="%{buildroot}" dist/*.whl
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license MIT-LICENSE

%changelog
*	Sat Dec 31 2022 scott andrews <scott-andrews@columbus.rr.com> 4.1.0-1
-	Initial build.	First version
