Summary: GNU readline library
Name: readline
Version: 8.2.001
Release: 1
License: GPLv3
Group: Core
URL: https://tiswww.case.edu/php/chet/readline/rltop.html
#	Source
Source0: http://www.example.org/Packages/readline/readline-8.2.tar.gz
Source1: http://www.example.org/Packages/readline/readline82-001
Source2: http://www.example.org/Packages/readline/inputrc

%description
GNU readline library

%prep
%setup -q -n %{name}-8.2
patch -p0 -i%{SOURCE1}
#	remove RPATH from shared objects (FS#14366)
sed -i 's|-Wl,-rpath,$(libdir) ||g' support/shobj-conf

%build
_options=(--prefix=/usr)
./configure "${_options[@]}"
%{make_build} SHLIB_LIBS="-lncurses"

%install
%{make_install}
install -Dm644 %{SOURCE2} %{buildroot}/etc/inputrc
rmdir %{buildroot}/usr/bin
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Tue Jan 24 2023 scott andrews <scott-andrews@columbus.rr.com> 8.2.001-1
-	Update to 8.2.001
*	Tue Nov 29 2022 scott andrews <scott-andrews@columbus.rr.com> 8.2.1-1
-	Initial build.	First version
