Summary: Ext2/3/4 filesystem utilities
Name: e2fsprogs
Version: 1.46.5
Release: 1
License: GPL LGPL MIT
Group: Core
URL: http://e2fsprogs.sourceforge.net
#	Source
Source0: http://www.example.org/Packages/e2fsprogs/e2fsprogs-1.46.5.tar.gz
Source1: http://www.example.org/Packages/e2fsprogs/MIT-LICENSE
%description
Ext2/3/4 filesystem utilities

%prep
%setup -q -n %{name}-%{version}
#	Remove unnecessary init.d directory
sed '/init\.d/s|^|#|' -i misc/Makefile.in
cp %{_sourcedir}/MIT-LICENSE .

%build
_options=(--prefix=/usr
	--libdir=/usr/lib
	--sbindir=/usr/bin
	--enable-elf-shlibs
	--disable-libblkid
	--disable-libuuid
	--disable-uuidd
	--disable-fsck
	--with-root-prefix="")
./configure "${_options[@]}"
%{make_build}
#	regenerate locale files
find po/ -name *.gmo -delete
make -C po update-gmo

%install
%{make_install} install-libs
sed -i -e 's/^AWK=.*/AWK=awk/' %{buildroot}/usr/bin/compile_et
#	remove references to build directory
sed -i -e 's#^DIR=.*#DIR=/usr/share/ss#' %{buildroot}/usr/bin/mk_cmds
sed -i -e 's#^DIR=.*#DIR=/usr/share/et#' %{buildroot}/usr/bin/compile_et
#	remove static libraries with a shared counterpart
rm -f %{buildroot}/usr/lib/lib{com_err,e2p,ext2fs,ss}.a
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license MIT-LICENSE

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 1.46.5-1
-	Initial build.	First version
