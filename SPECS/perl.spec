%global __requires_exclude_from %{_docdir}
%global __provides_exclude perl\\((VMS|Win32|BSD::|DB\\)$)
%global __requires_exclude perl\\((VMS|BSD::|Win32|Tk|Mac::|unicore::Name|FCGI|Locale::Codes::.*(Code|Retired))
%define _baseversion 5.36
Summary: A highly capable, feature-rich programming language
Name: perl
Version: 5.36.0
Release: 1
License: GPLv1
Group: Core
URL: https://www.perl.org
#	Source
Source0: http://www.example.org/Packages/perl/perl-5.36.0.tar.xz
Source1: http://www.example.org/Packages/perl/perlbin.sh
Source2: http://www.example.org/Packages/perl/perlbin.csh
Source3: http://www.example.org/Packages/perl/perlbin.fish
Provides: /usr/bin/perl
Provides: perl >= 0:5.003000
Provides: perl >= 0:5.00405
Provides: perl >= 0:5.00503
Provides: perl >= 0:5.006001
Provides: perl >= 0:5.006002
Provides: perl >= 0:5.008001
Provides: perl >= 0:5.008003
Provides: perl >= 0:5.009001
Provides: perl >= 0:5.009005
Provides: perl >= 1:5
Provides: perl >= 1:5.0
Provides: perl >= 1:5.010
Provides: perl >= 1:5.010001
Provides: perl >= 1:5.035007
Provides: perl >= 1:5.6
Provides: perl >= 1:5.6.1
Provides: perl >= 1:5.12
Provides: perl >= 1:5.8.0
%description
A highly capable, feature-rich programming language

%prep
%setup -q -n %{name}-%{version}

%build
# reproducible builds overrides are only fully effective when loaded from file
#	cp %{_sourcedir}/config.over .
export TZ=UTC
_options=(-des
	-Dusethreads
	-Duseshrplib
	-Dprefix=/usr
	-Dvendorprefix=/usr
	-Dprivlib=/usr/share/perl5/core_perl
	-Darchlib=/usr/lib/perl5/%{_baseversion}/core_perl
	-Dsitelib=/usr/share/perl5/site_perl
	-Dsitearch=/usr/lib/perl5/%{_baseversion}/site_perl
	-Dvendorlib=/usr/share/perl5/vendor_perl
	-Dvendorarch=/usr/lib/perl5/%{_baseversion}/vendor_perl
	-Dscriptdir=/usr/bin/core_perl
	-Dsitescript=/usr/bin/site_perl
	-Dvendorscript=/usr/bin/vendor_per
	-Dinc_version_list=none
	-Dman1ext=1perl
	-Dman3ext=3perl)
sh Configure "${_options[@]}"
%{make_build}

%install
%{make_install}
### Perl Settings ###
# Change man page extensions for site and vendor module builds.
# Set no mail address since bug reports should go to the bug tracker
# and not some ones email.
sed -e '/^man1ext=/ s/1perl/1p/' -i %{buildroot}/usr/lib/perl5/%{_baseversion}/core_perl/Config_heavy.pl
sed -e '/^man3ext=/ s/3perl/3pm/' -i %{buildroot}/usr/lib/perl5/%{_baseversion}/core_perl/Config_heavy.pl
sed -e "/^cf_email=/ s/.*//" -i %{buildroot}/usr/lib/perl5/%{_baseversion}/core_perl/Config_heavy.pl
sed -e "/^perladmin=/ s/.*//" -i %{buildroot}/usr/lib/perl5/%{_baseversion}/core_perl/Config_heavy.pl
### CPAN Settings ###
# Set CPAN default config to use the site directories.
sed -e '/(makepl_arg =>/   s/""/"INSTALLDIRS=site"/' -i %{buildroot}/usr/share/perl5/core_perl/CPAN/FirstTime.pm
sed -e '/(mbuildpl_arg =>/ s/""/"installdirs=site"/' -i %{buildroot}/usr/share/perl5/core_perl/CPAN/FirstTime.pm
### Profile script to set paths to perl scripts.
install -D -m644 %{SOURCE1} %{buildroot}/etc/profile.d/perlbin.sh
# Profile script to set paths to perl scripts on csh. (FS#22441)
install -D -m644 %{SOURCE2} %{buildroot}/etc/profile.d/perlbin.csh
# Profile script to set paths to perl scripts on fish. (FS#51191)
install -D -m 755 %{SOURCE3} %{buildroot}/usr/share/fish/vendor_conf.d/perlbin.fish
# Add the dirs so new installs will already have them in PATH once they
# install their first perl program
install -d -m755 %{buildroot}/usr/bin/vendor_perl
install -d -m755 %{buildroot}/usr/bin/site_perl
#(cd %{buildroot}/usr/bin; mv perl${pkgver} perl)
#	rm "%{buildroot}/usr/bin/perl$pkgver"
#	install -D -m755 -t %{buildroot}/usr/share/libalpm/scripts %{_sourcedir}/detect-old-perl-modules.sh
#	install -D -m644 -t %{buildroot}/usr/share/libalpm/hooks %{_sourcedir}/detect-old-perl-modules.hook
find %{buildroot} -name perllocal.pod -delete
find %{buildroot} -name .packlist -delete
###	Empty directories
touch %{buildroot}/usr/share/perl5/site_perl/.hidden
touch %{buildroot}/usr/bin/vendor_perl/.hidden
touch %{buildroot}/usr/bin/site_perl/.hidden
touch %{buildroot}/usr/lib/perl5/5.36/site_perl/.hidden
###	Systemd tmpfiles
#	mkdir %{buildroot}/usr/lib/tmpfiles.d
#	install -m644 %{_sourcedir}/tmpfiles %{buildroot}/usr/lib/tmpfiles.d/perl.conf
#	Fix perms
chmod a+x %{buildroot}/etc/profile.d/perlbin.csh
chmod a+x %{buildroot}/etc/profile.d/perlbin.sh
###	%%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)

%changelog
*	Mon Dec 19 2022 scott andrews <scott-andrews@columbus.rr.com> 5.36.0-1
-	Initial build.	First version
