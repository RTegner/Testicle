Summary: Library and command line tools for XZ and LZMA compressed files
Name: xz
Version: 5.4.1
Release: 1
License: GPL
Group: Core
URL: https://tukaani.org/xz
#	Source
Source0: http://www.example.org/Packages/xz/xz-5.4.1.tar.gz
%description
Library and command line tools for XZ and LZMA compressed files

%prep
%setup -q -n %{name}-%{version}

%build
_options=(--prefix=/usr
	--disable-rpath
	--disable-rpath
	--enable-werror)
./configure "${_options[@]}"
%{make_build}

%install
%{make_install}
%strip_libs
%strip_binaries
%rm_info_dir
%rm_la_files
#	Create empty directory list
find %{buildroot} -type d -empty -print > "%{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}"
sed 's|^%{buildroot}||' -i %{_topdir}/EMPTY/%{NAME}-%{VERSION}-%{RELEASE}
#	Create filelist.rpm
find '%{buildroot}' -ls -not -type d -print > %{_builddir}/filelist.rpm
sed 's|^%{buildroot}||' -i %{_builddir}/filelist.rpm
sed '/ /d' -i %{_builddir}/filelist.rpm

%files -f %{_builddir}/filelist.rpm
%defattr(-,root,root)
%license COPYING

%changelog
*	Wed Jan 25 2023 scott andrews <scott-andrews@columbus.rr.com> 5.4.1-1
-	Update version
*	Sun Jan 08 2023 scott andrews <scott-andrews@columbus.rr.com> 5.4.0-1
-	Initial build.	First version
